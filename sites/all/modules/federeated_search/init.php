<?php

error_reporting(E_ALL);
ini_set('display_errors', true);

require __DIR__.'/vendor/autoload.php';

if (file_exists('config.php')) {
    require('config.php');
} else {
    require('config-local.php');
}



function debug($arr){
    print '<pre>';
    print_r($arr);
    print '</pre>';
}