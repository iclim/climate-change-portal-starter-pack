<?php

function getFacetFilterQueryString($value){
	$selections = explode(',', $value);
	$selections = array_filter($selections);
	$selections_string = '';

	foreach ($selections as $sel):
		$sel = '"'.$sel.'"';
	endforeach;

	if (count($selections) > 1):
		//muliple selections
		$selections_string = '('.implode(' OR ', $selections).')';
	else:
		//its just one selection
		$selections_string = '"'.array_pop($selections).'"';
	endif;


	return $selections_string;
}

function parseUrlFilters($value){

	$selections = explode(',', $value);
	$selections = array_filter($selections);	

	$return_array = array();
	foreach($selections as $v){
		$return_array[$v] = $v;
	}

	return $return_array;
}

function generateUrlFilterArray($arr){
	$urlArr = array();
	foreach($arr as $k => $v):
		$urlArr[$k] = implode(",", $v);
	endforeach;

	return $urlArr;

}



function displayPivotFacet($pivotFacet, $facet_name="Facet Name")
{
	?>
	<div class="panel panel-default">
	  	<div class="panel-heading">
	    	<h3 class="panel-title"><?php echo ucfirst($facet_name); ?></h3>
	  	</div>
	  	<div class="panel-body">
		  	<ul class="list-unstyled">
				<?php  		

				$it = $pivotFacet->getIterator();
			    foreach ($it as $key=>$val):
			    	displayPivot($val);
			    endforeach;

			    ?>
			   </ul>
    	</div>
    </div>


    <?php


    
}

function displayPivot($pivot){
	global $active_filters;
	
	//debug($active_filters);

	if ( isset($active_filters[$pivot->getField()]) ){
		if ( count( $active_filters[$pivot->getField()] ) ) {
			$visible_class = 'visible';

			$array_position = array_search($pivot->getValue(), $active_filters[$pivot->getField()]);

			if ($array_position):
				//Filter is selected
				$checked = 'checked';
				//debug($pivot->getField().' filter is seleced');
				$new_active_filters = $active_filters;
				unset($new_active_filters[$pivot->getField()][$array_position]);

				if (!count($new_active_filters[$pivot->getField()])):
					unset($new_active_filters[$pivot->getField()]);
				endif;

		    	$new_link = http_build_query(  generateUrlFilterArray($new_active_filters) );

		    	echo "<li class='".$visible_class."'><input ".$checked." type='checkbox' name='".$pivot->getValue()."' id='".$pivot->getValue()."' data-query='".$new_link."' class='change-filter' /> <label for='".$pivot->getValue()."'>".$pivot->getValue()."</label> <span class='badge'>" . $pivot->getCount() . "</span>";
			else:
				//debug($pivot->getField().' filter NOT seleced');
				//This filter is NOT selected
				$new_active_filters = $active_filters;
				$new_active_filters[$pivot->getField()][$pivot->getValue()] = $pivot->getValue();
		    	$new_link = http_build_query(  generateUrlFilterArray($new_active_filters) );
		    	if ($pivot->getCount() > 0):
		    		//ENABLED
		    		echo "<li class='".$visible_class."'><input type='checkbox' name='".$pivot->getValue()."' id='".$pivot->getValue()."' data-query='".$new_link."' class='change-filter' /> <label for='".$pivot->getValue()."'>".$pivot->getValue()."</label> <span class='badge'>" . $pivot->getCount() . "</span>";
		    	else:
		    		//DISABLED
		    		echo "<li class=disabled '".$visible_class."'><input disabled type='checkbox' name='".$pivot->getValue()."' id='".$pivot->getValue()."' data-query='".$new_link."' class='change-filter' /> <label for='".$pivot->getValue()."'>".$pivot->getValue()."</label> <span class='badge'>" . $pivot->getCount() . "</span>";
		    	endif;

			endif;
		} 
		
	}else{
		$visible_class = 'visible';
		//Filter not selected
		///debug($pivot->getField().' filter NOT seleced');
		//This filter is NOT selected
		$new_active_filters = $active_filters;
		$new_active_filters[$pivot->getField()][$pivot->getValue()] = $pivot->getValue();
    	$new_link = http_build_query(  generateUrlFilterArray($new_active_filters) );
    	if ($pivot->getCount() > 0):
    		//ENABLED
    		echo "<li class='".$visible_class."'><input type='checkbox' name='".$pivot->getValue()."' id='".$pivot->getValue()."' data-query='".$new_link."' class='change-filter' /> <label for='".$pivot->getValue()."'>".$pivot->getValue()."</label> <span class='badge'>" . $pivot->getCount() . "</span>";
    	else:
    		//DISABLED
    		echo "<li class=disabled '".$visible_class."'><input disabled type='checkbox' name='".$pivot->getValue()."' id='".$pivot->getValue()."' data-query='".$new_link."' class='change-filter' /> <label for='".$pivot->getValue()."'>".$pivot->getValue()."</label> <span class='badge'>" . $pivot->getCount() . "</span>";
    	endif;

	}

	


	//debug($pivot->getField());

	// $new_active_filters = $active_filters;
	// $new_active_filters[$pivot->getField()][$pivot->getValue()] = $pivot->getValue();
	// $new_link = http_build_query(  generateUrlFilterArray($new_active_filters) );

	// $visible_class = 'visible';
	// //$new_link = "#";
	// $checked = "";

	
   
	//echo "<li class='".$visible_class."'><input ".$checked." type='checkbox' name='".$pivot->getValue()."' id='".$pivot->getValue()."' data-query='".$new_link."' class='change-filter' /> <label for='".$pivot->getValue()."'>".$pivot->getValue()."</label> <span class='badge'>" . $pivot->getCount() . "</span>";

    
    echo '<ul class="facet">';
    foreach ($pivot->getPivot() as $nextPivot) {
        displayPivot($nextPivot);
    }
    echo '</ul>';

    echo '</li>';
    
}


function displayFacet($facet, $facet_field, $facet_param_name){
	global $active_filters;
?>
	<div class="panel panel-default">
	  	<div class="panel-heading">
	    	<h3 class="panel-title"><?php echo ucfirst($facet_param_name); ?></h3>
	  	</div>
	  	<div class="panel-body">

	  		<ul class="list-unstyled facet <?php echo $facet_param_name."-facet"; ?>">
		  	<?php
		  	$counter = 0;
			foreach ($facet as $value => $count):
				$counter++;
				$visible_class = 'visible';
				if ($counter > 10){
					$visible_class = 'hidden';

				}
				if ( isset($active_filters[$facet_param_name]) ):
					$array_position = array_search($value, $active_filters[$facet_param_name]);
					//debug($array_position);
					if ($array_position):
						//debug($value." Filter is selcted");
						//This filter is currently selected
						$new_active_filters = $active_filters;
						unset($new_active_filters[$facet_param_name][$array_position]);
										
						if (!count($new_active_filters[$facet_param_name])):
							unset($new_active_filters[$facet_param_name]);
						endif;

				    	$new_link = http_build_query(  generateUrlFilterArray($new_active_filters) );
						echo "<li class='".$visible_class."'><input checked type='checkbox' name='".$value."' id='".$value."' data-query='".$new_link."' class='change-filter' /> <label for='".$value."'>".$value."</label> <span class='badge'>" . $count . "</span></li>";
					else:
						//This filter is NOT selected
						$new_active_filters = $active_filters;
						$new_active_filters[$facet_param_name][$value] = $value;
				    	$new_link = http_build_query(  generateUrlFilterArray($new_active_filters) );
				    	if ($count > 0):
				    		echo "<li class='".$visible_class."'><input type='checkbox' name='".$value."' id='".$value."' data-query='".$new_link."' class='change-filter' /> <label for='".$value."'>".$value."</label> <span class='badge'>" . $count . "</span></li>";
				    	else:
				    		echo "<li class='disabled ".$visible_class."'><input disabled type='checkbox' name='".$value."' id='".$value."' data-query='".$new_link."' class='change-filter' /> <label for='".$value."'>".$value."</label> <span class='badge'>" . $count . "</span></li>";
				    	endif;
				    	
					endif;
				else:
					//This filter is NOT selected
					$new_active_filters = $active_filters;
					$new_active_filters[$facet_param_name][$value] = $value;
			    	$new_link = http_build_query(  generateUrlFilterArray($new_active_filters) );
			    	if ($count > 0):
			    		echo "<li class='".$visible_class."''><input type='checkbox' name='".$value."' id='".$value."' data-query='".$new_link."' class='change-filter' /> <label for='".$value."'>".$value."</label> <span class='badge'>" . $count . "</span></li>";
			    	else:
			    		echo "<li class='disabled ".$visible_class."'><input disabled type='checkbox' name='".$value."' id='".$value."' data-query='".$new_link."' class='change-filter' /> <label for='".$value."'>".$value."</label> <span class='badge'>" . $count . "</span></li>";
			    	endif;	
				endif;

				

				
				/*	
				if (isset($active_filters[$facet_param_name]) && $value == $active_filters[$facet_param_name]){
					
					echo "<li><input checked type='checkbox' name='".$value."' id='".$value."' data-query='".http_build_query(array_diff($active_filters, array($facet_param_name => $value) ))."' class='change-filter' /> <label for='".$value."'>".$value."</label> <span class='badge'>" . $count . "</span></li>";
				} elseif ($count == 0) {
					echo "<li class='disabled'><input disabled type='checkbox' name='".$value."' id='".$value."' data-query='".http_build_query(array_merge($active_filters, array($facet_param_name => $value) ))."' class='change-filter' /> <label for='".$value."'>".$value."</label> <span class='badge'>" . $count . "</span></li>";
				} else  {
					
					echo "<li><input type='checkbox' name='".$value."' id='".$value."' data-query='".http_build_query(array_merge($active_filters, array($facet_param_name => $value) ))."' class='change-filter' /> <label for='".$value."'>".$value."</label> <span class='badge'>" . $count . "</span></li>";
				}
				*/
			endforeach;
			if ($counter > 10){
				echo '<a href="javascript:showAll(\''.$facet_param_name.'\');">Show All</a>';

			}

		  	?>
		  	</ul>

	 	</div>
	</div>

<?php
}

function createFilterQueryMulti($fields=array(),$conditional='OR'){
	global $_GET, $query;
	$query_string = '';

	$populated_fields = array();
	foreach($fields as $field):
		if(isset($_GET[$field]) && !empty($_GET[$field])):
			$populated_fields[] = $field;
		endif;
	endforeach;

	$counter = 0;
	foreach($populated_fields as $field):

		$counter++;

		$populated_value = explode(",",$_GET[$field]);


		if (is_array($populated_value)):
			$counter2= 0;
			foreach($populated_value as $pop_val):
				$counter2++;
				$query_string .= $field.':"'.$pop_val.'"';	
				if( $counter2 < count($populated_value) ):
					//if there are more values left
					$query_string .= ' '.$conditional.' ';
				endif;
			endforeach;
		else:
			//single value
			$query_string .= $field.':"'.$_GET[$field].'"';
		endif;

		
		if( $counter < count($populated_fields) ):
			//if there are more fields left
			$query_string .= ' '.$conditional.' ';
		endif;
				
	endforeach;

	//debug($query_string);

	if ( count($populated_fields) ):
		$query->createFilterQuery($field)->setQuery( $query_string )->addTag($field);
	endif;
		
	

}