
-- SUMMARY --

This module manages connections to external semantic services (PoolParty server,
sOnr webMining server, SPARQL endpoints), offers APIs to work with them and
makes interconnection between these modules possible.

-- REQUIREMENTS --

- If you want to work with SPARQL-endpoints, the ARC2-library needs to be
installed: Download the ARC2-library (https://github.com/semsol/arc2/wiki) and
add ARC2.php and all the other files and folders to "sites/all/libraries/arc2".

-- INSTALLATION --

Install as usual:
see https://drupal.org/documentation/install/modules-themes/modules-7 for
further information.

-- USAGE --

- Activate the module.
- Configure the modules, that require this module.
