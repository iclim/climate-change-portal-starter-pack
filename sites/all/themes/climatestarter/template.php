<?php


/**
 * @file
 * template.php
 */
if (!function_exists('climatestarter_preprocess_page')) {
function climatestarter_preprocess_page(&$variables, $hook) {  // Add theme suggestion for all content types
	
	// if this is a panel page, add template suggestions
	  if($panel_page = page_manager_get_current_page()) {
	    // add a generic suggestion for all panel pages
	    $variables['theme_hook_suggestions'][] = 'page__panel';

	    // add the panel page machine name to the template suggestions
	    $variables['theme_hook_suggestions'][] = 'page__' . $panel_page['name'];

	    //add a body class for good measure
	    $body_classes[] = 'page-panel';
	  }

    if (isset($variables['node'])) {
        if ($variables['node']->type != '') {
            $variables['theme_hook_suggestions'][] = 'page__node__' . $variables['node']->type;
        }
    }
}
}

if (!function_exists('get_breadcrumb')) {
	function get_breadcrumb() {
		$breadcrumb = array();
	    $breadcrumb[] = l(t('Home'), '<front>');
	    $is_tax = false;	

	  if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))) {
	  	$is_tax = true;

	    $breadcrumb[] = l(t('Taxonomy'), '#');

	    $tid = arg(2);

	    if ($term = taxonomy_term_load($tid)) {
	      $uri = entity_uri('taxonomy_term', $term);
	      $breadcrumb[] = l($term->name, $uri['path'], $uri['options']);
	    }
	  }

	  // resume normal operation
	  if (!empty($breadcrumb)) {
	  // uncomment the next line to enable current page in the breadcrumb trail
	  	if (!$is_tax):
		    $title = drupal_get_title();
		    if (!empty($title)) {
		      $breadcrumb[] = $title;
		      unset($title);
		    }
	    endif;

	    return '<div class="breadcrumb">'. implode(' &gt; ', $breadcrumb) . '</div>';
	  }
	}
}

function climatestarter_facetapi_deactivate_widget($variables) {
	// $str = '<a class="btn btn-default btn-sm remove dropdown-toggle" href="?">
	// 	<span class="glyphicon glyphicon-remove"></span>
	// 	<span class="sr-only">Remove constraint</span>
	// </a>';
	// return $str;

  	return '(x)';
}

function climatestarter_facetapi_accessible_markup($variables) {
	// print '<pre>';
	// print_r($variables);
	// print '</pre>';
  $vars = array('@text' => $variables['text']);
  $text = ($variables['active']) ? t('Remove @text filter', $vars) : t('Apply @text filter', $vars);
  // Add spaces before and after the text, since other content may be displayed
  // inline with this and we don't want the words to run together. However, the
  // spaces must be inside the <span> so as not to disrupt the layout for
  // sighted users.
  return '<span class="foo element-invisible"> ' . $text . ' </span>';
 //  $str = '<span class="constraint-value btn btn-sm btn-default btn-disabled">
	// 	<span class="filterValue" title="'.$variables['text'].'">'.$variables['text'].'</span>
	// </span>';
	// return $str;
  
}

function climatestarter_facetapi_link_active($variables) {
// print '<pre>';
// 	print_r($variables);
// 	print '</pre>';
  // Sanitizes the link text if necessary.
  $sanitize = empty($variables['options']['html']);
  $link_text = ($sanitize) ? check_plain($variables['text']) : $variables['text'];

  // Theme function variables fro accessible markup.
  // @see http://drupal.org/node/1316580
  $accessible_vars = array(
    'text' => $variables['text'],
    'active' => TRUE,
  );

  // Builds link, passes through t() which gives us the ability to change the
  // position of the widget on a per-language basis.
  $replacements = array(
    '!facetapi_deactivate_widget' => theme('facetapi_deactivate_widget', $variables),
    '!facetapi_accessible_markup' => theme('facetapi_accessible_markup', $accessible_vars),
  );
  $variables['text'] = t('!facetapi_deactivate_widget !facetapi_accessible_markup', $replacements);
  $variables['options']['html'] = TRUE;

  return theme_link($variables) . $link_text;
}

function climatestarter_current_search_text(array $variables) {
  // Initializes output, sanitizes text if necessary.
  $sanitize = empty($variables['options']['html']);
  $output = ($sanitize) ? check_plain($variables['text']) : $variables['text'];

  // Adds wrapper markup and CSS classes.
  if ($variables['wrapper'] && $variables['element']) {
    $attributes = array('class' => $variables['class']);
    $element = check_plain($variables['element']);
    $output = '<' . $element . drupal_attributes($attributes) . '>' . $output . '</' . $element . '>';
  }
  // print '<pre>';
  // print $output;
  // print '</pre>';
  return $output;
}

function climatestarter_current_search_item_wrapper(array $variables) {
  $element = $variables['element'];
  $attributes = array(
    'class' => array(
      'current-search-item arve',
      drupal_html_class('foo1 current-search-item-' . $element['#current_search_id']),
      drupal_html_class('foo2 current-search-item-' . $element['#current_search_name']),
    ),
  );
  return '<div' . drupal_attributes($attributes) . '>' . $element['#children'] . '</div>';
}

if (!function_exists('get_term_name')) {
function get_term_name($tid) {
  return db_select('taxonomy_term_data', 't')
  ->fields('t', array('name'))
  ->condition('tid', $tid)
  ->execute()
  ->fetchField();
}
}
