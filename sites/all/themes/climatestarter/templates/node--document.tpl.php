
<!-- <pre> -->
<?php 


//print_r($node); 
//print drupal_get_path_alias('node/46');
//Prepare Document
 // We hide the comments and links now so that we can render them later.
hide($content['comments']);
hide($content['links']);
hide($content['field_tags']);

hide($content['field_topics']);
hide($content['field_focus_area']);
hide($content['field_related_links_entity']);
hide($content['field_related_projects_entity']);
hide($content['field_country']);


hide($content['field_verified_pccp']);
hide($content['field_related_corporations']);
hide($content['field_related_name_s_']);
hide($content['field_document_files']);
hide($content['field_cover_image']);

hide($content['field_joomla_id']);
hide($content['field_joomla_hits']);
hide($content['field_document_link']);



//var_dump($content['field_project_status'])


?>
<!-- </pre> -->

<div class="container">

  <article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
    
  <div class="col-md-8 content-container">

        <!-- Start Article Header -->
        <?php if ($title_prefix || $title_suffix || $display_submitted || !$page): ?>
        <header>
          <?php print render($title_prefix); ?>
          
          <?php if (!$page): ?>
            <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
          <?php endif; ?>
          <?php print render($title_suffix); ?>
       
          
        </header>
        <?php endif; ?>
        <!-- End Article Header -->

        

        <!-- Start Article Content -->


        <div class="content"<?php print $content_attributes; ?>>
          <!-- Status   -->

            <!-- Description   -->
            <?php if (($tags = render($content['field_the_description'])) ): ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Description</h3>
                </div>
                <div class="panel-body">
                    <?php 
                    $field = field_view_field('node', $node, 'field_the_description', array('label'=>'hidden'));
                    print render($field) 
                    ?>
                </div>
            </div>
            <?php endif; ?> 

            <!-- Project Record -->
            <table id="data-table" class="table table-striped table-hover ">
                <tbody>
                    <?php if(render($content['field_publication_year'])): ?>
                        <tr>
                            <td>Publication Year</td>
                            <td><?php print render($content['field_publication_year']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_publication_day_month'])): ?>
                        <tr>
                            <td>Publication Day & Month</td>
                            <td><?php print render($content['field_publication_day_month']); ?></td>
                        </tr>
                    <?php endif; ?>

                    

                    <?php if(render($content['field_author'])): ?>
                        <tr>
                            <td>Author(s)</td>
                            <td><?php print render($content['field_author']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_edition'])): ?>
                        <tr>
                            <td>Edition</td>
                            <td><?php print render($content['field_edition']); ?></td>
                        </tr>
                    <?php endif; ?>


                    <?php if(render($content['field_resource_type'])): ?>
                        <tr>
                            <td>Resource Type</td>
                            <td><?php print render($content['field_resource_type']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_rights_statement'])): ?>
                        <tr>
                            <td>Rights Statement</td>
                            <td><?php print render($content['field_rights_statement']); ?></td>
                        </tr>
                    <?php endif; ?>


                    <?php if(render($content['field_attribution_statement'])): ?>
                        <tr>
                            <td>Attribution Statement</td>
                            <td><?php print render($content['field_attribution_statement']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_bibliography_note'])): ?>
                        <tr>
                            <td>Bibliography Note</td>
                            <td><?php print render($content['field_bibliography_note']); ?></td>
                        </tr>
                    <?php endif; ?>


                  

                    

                    <?php if(render($content['field_cataloging_source'])): ?>
                        <tr>
                            <td>Cataloging source</td>
                            <td><?php print render($content['field_cataloging_source']); ?></td>
                        </tr>
                    <?php endif; ?>


                    <?php if(render($content['field_corporate_author'])): ?>
                        <tr>
                            <td>Corporate Author</td>
                            <td><?php print render($content['field_corporate_author']); ?></td>
                        </tr>
                    <?php endif; ?>


                    <?php if(render($content['field_event'])): ?>
                        <tr>
                            <td>Event</td>
                            <td><?php print render($content['field_event']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_general_note'])): ?>
                        <tr>
                            <td>General Note</td>
                            <td><?php print render($content['field_general_note']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_isbn'])): ?>
                        <tr>
                            <td>ISBN</td>
                            <td><?php print render($content['field_isbn']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_issn'])): ?>
                        <tr>
                            <td>ISSN</td>
                            <td><?php print render($content['field_issn']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_physical_description'])): ?>
                        <tr>
                            <td>Physical Description</td>
                            <td><?php print render($content['field_physical_description']); ?></td>
                        </tr>
                    <?php endif; ?>



                    <?php if(render($content['field_place_of_publication'])): ?>
                        <tr>
                            <td>Place of Publication</td>
                            <td><?php print render($content['field_place_of_publication']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_publisher'])): ?>
                        <tr>
                            <td>Publisher</td>
                            <td><?php print render($content['field_publisher']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_series'])): ?>
                        <tr>
                            <td>Series</td>
                            <td><?php print render($content['field_series']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_source_of_acquisition'])): ?>
                        <tr>
                            <td>Source of Acquisition</td>
                            <td><?php print render($content['field_source_of_acquisition']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_target_audience'])): ?>
                        <tr>
                            <td>Target Audience (Literacy)</td>
                            <td><?php print render($content['field_target_audience']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_variant_title'])): ?>
                        <tr>
                            <td>Variant Title</td>
                            <td><?php print render($content['field_variant_title']); ?></td>
                        </tr>
                    <?php endif; ?>

                     <?php if(render($content['field_educational_resource_type'])): ?>
                        <tr>
                            <td>Eucational Resource Type</td>
                            <td><?php print render($content['field_educational_resource_type']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_subject_strand'])): ?>
                        <tr>
                            <td>Subject Strand</td>
                            <td><?php print render($content['field_subject_strand']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_climate_change_topic'])): ?>
                        <tr>
                            <td>Climate Change Topic</td>
                            <td><?php print render($content['field_climate_change_topic']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_drm_topic'])): ?>
                        <tr>
                            <td>DRM Topic</td>
                            <td><?php print render($content['field_drm_topic']); ?></td>
                        </tr>
                    <?php endif; ?>

                     <?php if(render($content['field_tvet_training_area_focus'])): ?>
                        <tr>
                            <td>TVET Training Area/Focus</td>
                            <td><?php print render($content['field_tvet_training_area_focus']); ?></td>
                        </tr>
                    <?php endif; ?>

                     <?php if(render($content['field_tvet_training_area_focus'])): ?>
                        <tr>
                            <td>TVET Training Area/Focus</td>
                            <td><?php print render($content['field_tvet_training_area_focus']); ?></td>
                        </tr>
                    <?php endif; ?>
                    
                      <?php if(render($content['field_regional_focus'])): ?>
                        <tr>
                            <td>Regional Focus</td>
                            <td><?php print render($content['field_regional_focus']); ?></td>
                        </tr>
                    <?php endif; ?>

                       <?php if(render($content['field_utilised_in'])): ?>
                        <tr>
                            <td>Utilised In</td>
                            <td><?php print render($content['field_utilised_in']); ?></td>
                        </tr>
                    <?php endif; ?>



                </tbody>
            </table>

          <?php
           
            print render($content);
          ?>
        </div>

          
          
        <!-- End Article Content -->





        <!-- Start Article Footer -->
        <footer>

          <?php if ($display_submitted): ?>
            <div class="submitted">
              <?php print $user_picture; ?>
              <span class="glyphicon glyphicon-calendar"></span> <?php print $submitted; ?>
            </div>
          <?php endif; ?>
          </footer>
          <!-- End Article Footer -->

    </div>

    <!-- Start sidebar -->
    <div class="col-md-4 sidebar-container">
          <!-- Cover Image -->
          <?php if (($tags = render($content['field_cover_image'])) ): ?>
            <div class="panel panel-success">
              <!--   <div class="panel-heading">
                    <h3 class="panel-title">Cover Image</h3>
                </div> -->
                <div class="panel-body">
                    <?php print render($content['field_cover_image']); ?>
                </div>
            </div>
        <?php endif; ?> 

         <!-- Documents -->
        <?php if (($tags = render($content['field_document_files'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Documents</h3>
                </div>
                <div class="panel-body">
                    <?php print render($content['field_document_files']); ?>
                </div>
            </div>
        <?php endif; ?> 

        
        <!-- Related Countries -->
        <?php if (($tags = render($content['field_country'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Related Countries</h3>
                </div>
                <div class="panel-body">
                    <div class="field-items">
                    <?php 
                    // $field = field_view_field('node', $node, 'field_related_documents_entity', array('label'=>'hidden'));
                    // print render($field) 
                    if ( isset($content['field_country']['#items']) ):
                    $related = $content['field_country']['#items'];
                    if (is_array($related)):
                            foreach ($related as $entity) {
                                //var_dump($entity);
                                $title = $entity['entity']->title;
                                $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                            }
                        endif;
                    endif;
                    ?>
                    </div>
                </div>
            </div>
        <?php endif; ?> 

        <!-- Related Documents -->
        <?php if (($tags = render($content['field_related_documents_entity'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Related Documents</h3>
                </div>
                <div class="panel-body">
                    <div class="field-items">
                    <?php 
                    // $field = field_view_field('node', $node, 'field_related_documents_entity', array('label'=>'hidden'));
                    // print render($field) 
                    if ( isset($content['field_related_documents_entity']['#items']) ):
                    $related = $content['field_related_documents_entity']['#items'];
                    if (is_array($related)):
                            foreach ($related as $entity) {
                                //var_dump($entity);
                                $title = $entity['entity']->title;
                                $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                            }
                        endif;
                    endif;
                    ?>
                    </div>
                </div>
            </div>
        <?php endif; ?> 

        <!-- Related Projects -->
        <?php if (($tags = render($content['field_related_projects_entity'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Related Projects</h3>
                </div>
                <div class="panel-body">
                    <div class="field-items">
                    <?php 
                    // $field = field_view_field('node', $node, 'field_related_documents_entity', array('label'=>'hidden'));
                    // print render($field) 
                    if ( isset($content['field_related_projects_entity']['#items']) ):
                    $related = $content['field_related_projects_entity']['#items'];
                    if (is_array($related)):
                            foreach ($related as $entity) {
                                //var_dump($entity);
                                $title = $entity['entity']->title;
                                $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                            }
                        endif;
                    endif;
                    ?>
                    </div>
                </div>
            </div>
        <?php endif; ?> 


        <!-- Related Orgs -->
        <?php if (($tags = render($content['field_related_name_s_'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Related Names</h3>
                </div>
                <div class="panel-body">
                    <?php print render($content['field_related_name_s_']); ?>
                </div>
            </div>
        <?php endif; ?> 

         <!-- Related Names -->
        <?php if (($tags = render($content['field_related_corporations'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Related Corporations</h3>
                </div>
                <div class="panel-body">
                    <?php print render($content['field_related_corporations']); ?>
                </div>
            </div>
        <?php endif; ?> 
       

       
        <!-- Tags -->
        <?php if (($tags = render($content['field_tags'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Tags</h3>
                </div>
                <div class="panel-body">
                    <?php print render($content['field_tags']); ?>
                    <?php 
                    // $field = field_view_field('node', $node, 'field_tags', array('label'=>'hidden'));
                    // print render($field) 
                    ?>
                </div>
            </div>
        <?php endif; ?> 

        <!-- Topics -->
        <?php if (($tags = render($content['field_topics'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Topics</h3>
                </div>
                <div class="panel-body">
                    <?php print render($content['field_topics']); ?>
                </div>
            </div>
        <?php endif; ?> 
       

        <!-- Focus area -->
         <?php if (($tags = render($content['field_focus_area'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Focus Area</h3>
                </div>
                <div class="panel-body">
                    <?php print render($content['field_topics']); ?>
                </div>
            </div>
        <?php endif; ?> 



        
       

        
        
        


    </div>
    <!-- end sidebar -->

  </article>

</div> <!-- end main project container -->

