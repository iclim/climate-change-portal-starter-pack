
<!-- <pre> -->
<?php 


//print_r($node); 
//print drupal_get_path_alias('node/46');
//Prepare Document
 // We hide the comments and links now so that we can render them later.
hide($content['comments']);
hide($content['links']);
hide($content['field_tags']);

hide($content['field_topics']);
hide($content['field_focus_area']);
hide($content['field_related_links_entity']);
hide($content['field_related_projects_entity']);
hide($content['field_partner_category']);

hide($content['field_donor_type']);



hide($content['field_verified_pccp']);
hide($content['field_related_links_entity']);
hide($content['field_related_links']);
hide($content['field_related_corporations']);
hide($content['field_related_country']);
hide($content['field_image']);



//var_dump($content['field_project_status'])


?>
<!-- </pre> -->

<div class="container">

  <article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
    
  <div class="col-md-8 content-container">

        <!-- Start Article Header -->
        <?php if ($title_prefix || $title_suffix || $display_submitted || !$page): ?>
        <header>
          <?php print render($title_prefix); ?>
          
          <?php if (!$page): ?>
            <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
          <?php endif; ?>
          <?php print render($title_suffix); ?>
       
          
        </header>
        <?php endif; ?>
        <!-- End Article Header -->

        

        <!-- Start Article Content -->


        <div class="content"<?php print $content_attributes; ?>>
          <!-- Status   -->

            <!-- Description   -->
            <?php if (($tags = render($content['field_the_description'])) ): ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Description</h3>
                </div>
                <div class="panel-body">
                    <?php 
                    $field = field_view_field('node', $node, 'field_the_description', array('label'=>'hidden'));
                    print render($field) 
                    ?>
                </div>
            </div>
            <?php endif; ?> 

            <!-- Org Record -->
            <table id="data-table" class="table table-striped table-hover ">
                <tbody>
                    <?php if(render($content['field_acronym'])): ?>
                        <tr>
                            <td>Acronym</td>
                            <td><?php print render($content['field_acronym']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_organisation_type'])): ?>
                        <tr>
                            <td>Organisation Type</td>
                            <td><?php print render($content['field_organisation_type']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_donor_type'])): ?>
                        <tr>
                            <td>Organisation Type</td>
                            <td><?php print render($content['field_donor_type']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_partner_category'])): ?>
                        <tr>
                            <td>Partner Type</td>
                            <td><?php print render($content['field_partner_category']); ?></td>
                        </tr>
                    <?php endif; ?>

                    
                    

                    <?php if(render($content['field_end_date'])): ?>
                        <tr>
                            <td>End Date</td>
                            <td><?php print render($content['field_end_date']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_location_text_'])): ?>
                        <tr>
                            <td>Location</td>
                            <td><?php print render($content['field_location_text_']); ?></td>
                        </tr>
                    <?php endif; ?>


                    <?php if(render($content['field_venue'])): ?>
                        <tr>
                            <td>Venue</td>
                            <td><?php print render($content['field_venue']); ?></td>
                        </tr>
                    <?php endif; ?>

                   
                 


                </tbody>
            </table>


        <!-- Related Funds -->
        <?php if (($tags = render($content['field_related_funds'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Related Funds</h3>
                </div>
                <div class="panel-body">
                    <div class="field-items">
                    <?php 
                    // $field = field_view_field('node', $node, 'field_related_documents_entity', array('label'=>'hidden'));
                    // print render($field) 
                    if ( isset($content['field_related_funds']['#items']) ):
                    $related = $content['field_related_funds']['#items'];
                    if (is_array($related)):
                            foreach ($related as $entity) {
                                //var_dump($entity);
                                $title = $entity['entity']->title;
                                $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                            }
                        endif;
                    endif;
                    ?>
                    </div>
                </div>
            </div>
        <?php endif; ?> 


         <!-- Related Projects -->
        <?php if (($tags = render($content['field_related_projects_entity'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Related Projects</h3>
                </div>
                <div class="panel-body">
                    <div class="field-items">
                    <?php 
                    // $field = field_view_field('node', $node, 'field_related_documents_entity', array('label'=>'hidden'));
                    // print render($field) 
                    if ( isset($content['field_related_projects_entity']['#items']) ):
                    $related = $content['field_related_projects_entity']['#items'];
                    if (is_array($related)):
                            foreach ($related as $entity) {
                                //var_dump($entity);
                                $title = $entity['entity']->title;
                                $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                            }
                        endif;
                    endif;
                    ?>
                    </div>
                </div>
            </div>
        <?php endif; ?> 

        

          <?php
           
            print render($content);
          ?>
        </div>



          
          
        <!-- End Article Content -->





        <!-- Start Article Footer -->
        <footer>

          <?php if ($display_submitted): ?>
            <div class="submitted">
              <?php print $user_picture; ?>
              <span class="glyphicon glyphicon-calendar"></span> <?php print $submitted; ?>
            </div>
          <?php endif; ?>
          </footer>
          <!-- End Article Footer -->

    </div>

    <!-- Start sidebar -->
    <div class="col-md-4 sidebar-container">
        <!--  Image -->
          <?php if (($tags = render($content['field_image'])) ): ?>
            <!-- <div class="panel panel-success"> -->
              <!--   <div class="panel-heading">
                    <h3 class="panel-title">Cover Image</h3>
                </div> -->
                <!-- <div class="panel-body"> -->
                    <?php print render($content['field_image']); ?>
                <!-- </div> -->
            <!-- </div> -->
        <?php endif; ?> 


          <!-- Countries -->
          <?php if (($tags = render($content['field_related_country'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Related Countries</h3>
                </div>
                <div class="panel-body">
                    <?php print render($content['field_related_country']); ?>
                </div>
            </div>
        <?php endif; ?> 

         <!-- Links -->
        <?php if (($tags = render($content['field_related_links'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Related Links</h3>
                </div>
                <div class="panel-body">
                    <?php print render($content['field_related_links']); ?>
                </div>
            </div>
        <?php endif; ?> 

        <!-- Related Documents -->
        <?php if (($tags = render($content['field_related_documents_entity'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Related Documents</h3>
                </div>
                <div class="panel-body">
                    <div class="field-items">
                    <?php 
                    // $field = field_view_field('node', $node, 'field_related_documents_entity', array('label'=>'hidden'));
                    // print render($field) 
                    if ( isset($content['field_related_documents_entity']['#items']) ):
                    $related = $content['field_related_documents_entity']['#items'];
                    if (is_array($related)):
                            foreach ($related as $entity) {
                                //var_dump($entity);
                                $title = $entity['entity']->title;
                                $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                            }
                        endif;
                    endif;
                    ?>
                    </div>
                </div>
            </div>
        <?php endif; ?> 

       


       
       

       
        <!-- Tags -->
        <?php if (($tags = render($content['field_tags'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Tags</h3>
                </div>
                <div class="panel-body">
                    <?php 
                    $field = field_view_field('node', $node, 'field_tags', array('label'=>'hidden'));
                    print render($field) 
                    ?>
                </div>
            </div>
        <?php endif; ?> 

        <!-- Topics -->
        <?php if (($tags = render($content['field_topics'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Topics</h3>
                </div>
                <div class="panel-body">
                    <?php 
                    $field = field_view_field('node', $node, 'field_topics', array('label'=>'hidden'));
                    print render($field) 
                    ?>
                </div>
            </div>
        <?php endif; ?> 
       

        <!-- Focus area -->
         <?php if (($tags = render($content['field_focus_area'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Focus Area</h3>
                </div>
                <div class="panel-body">
                    <?php 
                    $field = field_view_field('node', $node, 'field_focus_area', array('label'=>'hidden'));
                    print render($field) 
                    ?>
                </div>
            </div>
        <?php endif; ?> 



        
       

        
        
        


    </div>
    <!-- end sidebar -->

  </article>

</div> <!-- end main project container -->

