
<!-- <pre> -->
<?php //print_r($content['field_elements']['#items']); ?>
<!-- </pre> -->
<?php 


//print_r($node); 
//print drupal_get_path_alias('node/46');
//Prepare Document
 // We hide the comments and links now so that we can render them later.
hide($content['comments']);
hide($content['links']);
hide($content['field_tags']);

hide($content['field_topics']);
hide($content['field_focus_area']);
hide($content['field_related_links_entity']);
hide($content['field_related_projects_entity']);
hide($content['field_related_documents_entity']);
hide($content['field_related_news']);
hide($content['field_related_links_entity']);
hide($content['field_related_events']);


hide($content['field_verified_pccp']);
hide($content['field_related_corporations']);
hide($content['field_related_name_s_']);
hide($content['field_document_files']);


hide($content['field_iso_code']);




//var_dump($content['field_project_status'])


?>

<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php if ($title_prefix || $title_suffix || $display_submitted || !$page): ?>
  <header>
    <?php print render($title_prefix); ?>
    
    <?php if (!$page): ?>
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a>
      </h2>

    <?php endif; ?>
    <?php print render($title_suffix); ?>

    
  </header>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>


    <div class="container subsite-header">

        <?php print render($content['field_banner']); ?>
        
    </div>
    


    <div class="container">

      <!-- Nav tabs -->
      <ul class="nav nav-pills nav-stacked col-md-3" role="tablist">

      <?php 
      $counter = 0;
      if (isset($content['field_elements'])):
        foreach($content['field_elements']['#items'] as $element): 
          $counter++;
          $active = '';
          if ($counter == 1){$active='active';}
          ?>
          <li role="presentation <?php echo $active; ?>" class="<?php echo $active; ?>"><a href="#<?php echo preg_replace('/\s/', '', strtolower($element['entity']->title)); ?>"  aria-controls="<?php echo preg_replace('/\s/', '', strtolower($element['entity']->title)); ?>"  role="tab" data-toggle="tab"><?php echo $element['entity']->title; ?></a></li>  
        <?php endforeach; ?>  
    <?php endif; ?>
<!-- 
        <li role="presentation active" class="active"><a href="#overview"     aria-controls="overview"      role="tab" data-toggle="tab">Overview</a></li>
        <li role="presentation"><a href="#priorities"  aria-controls="priorities"   role="tab" data-toggle="tab">National Priorities</a></li> -->
        
        

      </ul>


      <!-- Tab panes -->
      <div class="tab-content col-md-6">
        <?php 
            $counter = 0;
            if (isset($content['field_elements'])):
              foreach($content['field_elements']['#items'] as $element): 
              $counter++;
              $active = '';
              if ($counter == 1){$active='active';}
              $content_type =  $element['entity']->field_element_type['und'][0]['value'];

              if ($content_type == 'HTML'):?>
                  <div role="tabpanel <?php echo $active; ?>" class="tab-pane <?php echo $active; ?>" id="<?php echo preg_replace('/\s/', '', strtolower($element['entity']->title)); ?>">
                      <?php print print $element['entity']->field_contents['und'][0]['value']; ?>
                  </div>
              <?php elseif($content_type == 'Files'): ?>
                  <?php
                  $file_links =  $element['entity']->field_file_element['und'];
                  
                  if (count($file_links)):?>
                      <div role="tabpanel <?php echo $active; ?>" class="tab-pane <?php echo $active; ?>" id="<?php echo preg_replace('/\s/', '', strtolower($element['entity']->title)); ?>">
                      <ul>
                      <?php foreach($file_links as $flink):?>
                          <li><a href="<?php echo file_create_url($flink['uri']); ?>" target="_blank"><?php echo $flink['filename'] ?></a>
                      <?php endforeach;?>
                      </ul>
                      </div>
                  <?php endif; ?>

                  
                  
              <?php endif; ?>    
          <?php endforeach; ?> 
        <?php endif; ?>  
        
        
        
    
      </div>

    


    

    </div>

    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_tags']);
      //print render($content);
    ?>
  </div>
    
    <?php if (($tags = render($content['field_tags'])) || ($links = render($content['links']))): ?>
    <footer>
    <?php if ($display_submitted): ?>
      <div class="submitted">
        <?php print $user_picture; ?>
        <span class="glyphicon glyphicon-calendar"></span> <?php print $submitted; ?>
      </div>
    <?php endif; ?>
    <?php print render($content['field_tags']); ?>
    <?php print render($content['links']); ?>
    </footer>
    <?php endif; ?> 

  <?php print render($content['comments']); ?>

</article>