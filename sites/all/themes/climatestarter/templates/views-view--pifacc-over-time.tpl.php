<?php
function debugme($arr){
	print '<pre>';
	print_r($arr);
	print '</pre>';
	
}
function sprep_projects_graph(){
	$projects = [];
	$years = [];
	$project_years = [];
	$pifacc_themes_projects = [];
	$project_pifacc_themes = [];
	//get all projects and their start/end date
	$sql = "SELECT n.nid, n.title, YEAR(sd.field_start_date_value) as start_year,YEAR(enddate.field_end_date_value) as end_year
			FROM {node} n, {field_data_field_start_date} sd, {field_data_field_end_date} enddate
			WHERE n.type = 'product'
			AND n.nid = sd.entity_id
			AND n.nid = enddate.entity_id";

	// print $sql;
	// $result = $db_query($sql);
	$result = db_query("SELECT n.nid, n.title,YEAR(sd.field_start_date_value) as start,YEAR(enddate.field_end_date_value) as end_year FROM {node} n,{field_data_field_start_date} sd,{field_data_field_end_date} enddate WHERE n.type = 'product' AND n.nid = sd.entity_id AND n.nid = enddate.entity_id");
	if ($result) {
    	while ($row = $result->fetchAssoc()) {
    		$proj = [];
    		$proj['nid'] = $row['nid'];
    		$proj['title'] = $row['title'];
    		$proj['start_year'] = $row['start'];
    		$proj['end_year'] = $row['end_year'];
    		$projects[$row['nid']] = $proj;

    		//generate years
    		for ($i=$row['start']; $i <= $row['end_year']; $i++) { 
    			if (!is_array($years[$i])):
    				$years[$i] = [];
    			endif;
    			$years[$i][$proj['nid']] = $proj['nid'];

    			if (!is_array($project_years[$proj['nid']])):
    				$project_years[$proj['nid']] = [];
    			endif;
    			$project_years[$proj['nid']][] = $i;
    		}
    		
	      
	    }
	  }
	  ksort($years);
	  // debugme($project_years);

	$themes_to_show = [
		'699' => "Implementing tangible on-ground adaptation measures",
		'940' => "Education, Training and Awareness",
		'791' => "Governance and decision-making",
		'870' => "Improving Understanding of Climate Change",
		'747' => "Mitigation of global greenhouse gas emissions",
		'839' => "Partnerships and Cooperation"
	];

	//get all pifacc assignments for projects
	$result = db_query("SELECT field_pifacc_themes_tid as tid,entity_id as nid FROM {field_data_field_pifacc_themes} WHERE bundle = 'product'");
	if ($result) {
    	while ($row = $result->fetchAssoc()) {
 	
			if (!is_array($pifacc_themes_projects[$row['tid']])):
				$pifacc_themes_projects[$row['tid']] = [];
			endif;
			$pifacc_themes_projects[$row['tid']][$row['nid']] = $row['nid'];

			if (!is_array($project_pifacc_themes[$row['nid']])):
				$project_pifacc_themes[$row['nid']] = [];
			endif;
			$project_pifacc_themes[$row['nid']][$row['tid']] = $row['tid'];
    		
    		
	     
	    }
	}
	ksort($pifacc_themes_projects);
	// debugme($pifacc_themes_projects);

	$theme_projects_by_year = [];
	$themes_to_show_by_year = [];

	foreach($themes_to_show as $tid=>$name):
		
		$projects_for_theme = $pifacc_themes_projects[$tid];
		// debugme($projects_for_theme);
		foreach($years as $year=>$y):
			if ($year >= 2006):
				foreach ($projects_for_theme as $pid):
					if( in_array($year, $project_years[$pid] ) ):
						if (!is_array($themes_to_show_by_year[$tid][$year])):
							$themes_to_show_by_year[$tid][$year] = [];
						endif;
						$themes_to_show_by_year[$tid][$year][$pid] = $pid;	
					endif;
				endforeach;
			endif;
		endforeach;
		
			
		
	endforeach;


	// debugme($themes_to_show_by_year);
	// 
	print '<script>var series = [];</script>';
	$years_to_show = [];
	foreach($themes_to_show_by_year as $tid=>$theme):
		foreach( $theme as $year=>$projects ):
			$years_to_show[$year] = $year;
		endforeach;
	endforeach;

	// debugme($themes_to_show_by_year);
	foreach($themes_to_show_by_year as $tid=>$theme):
		
		

		foreach( $years_to_show as $year ):
			
			if (is_array($themes_to_show_by_year[$tid][$year])):
				$year_counts[$year] = count($themes_to_show_by_year[$tid][$year]);
			else:
				$year_counts[$year] = 0;
			endif;
		endforeach;

		print '<script>';
		print 'var theme = {};';
		print 'theme["name"] = "'.$themes_to_show[$tid].'";';		
		print 'theme["data"] = ['.implode(',', $year_counts).'];';		
		print 'series.push(theme);';
		print '</script>';
		// debugme($year_counts);
	endforeach;

	print '<script>var years_to_show = ['.implode(',', $years_to_show).'];</script>';

}



sprep_projects_graph();

?>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>


<script type="text/javascript">
	window.onload=(function(oldLoad){
	  return function(){
	    oldLoad && oldLoad();
	    createChart();
	  }
	})(window.onload);


	function createChart(){
	    jQuery('#container').highcharts({
	        title: {
	            text: 'PIFACC Theme Projects Per Year',
	            x: -20 //center
	        },
	        subtitle: {
	            text: 'Source: SPREP',
	            x: -20
	        },
	        // xAxis: {
	        //     categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
	        //         'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
	        // },
	        xAxis: {
	            categories: years_to_show
	        },
	        yAxis: {
	            title: {
	                text: 'Number of projects'
	            },
	            plotLines: [{
	                value: 0,
	                width: 1,
	                color: '#808080'
	            }]
	        },
	        tooltip: {
	            valueSuffix: ' Projects'
	        },
	        legend: {
	            layout: 'vertical',
	            align: 'right',
	            verticalAlign: 'middle',
	            borderWidth: 0
	        },
	        series: series
	        // series: [{
	        //     name: 'Tokyo',
	        //     data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
	        // }, {
	        //     name: 'New York',
	        //     data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
	        // }, {
	        //     name: 'Berlin',
	        //     data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
	        // }, {
	        //     name: 'London',
	        //     data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
	        // }]
	    });
	}
</script>