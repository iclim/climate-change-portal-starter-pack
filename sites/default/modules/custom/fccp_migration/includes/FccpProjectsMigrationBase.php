<?php


abstract class FccpProjectsMigrationBase extends Migration {

  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->source = new MigrateSourceCSV(DRUPAL_ROOT . '/../migrate_data/projects.csv', $this->csvcolumns(), array('header_rows' => 1), $this->fields());
  }

  /**
   * Default implementation of prepareKey. This method is called from the source
   * plugin immediately after retrieving the raw data from the source - by
   * default, it simply assigns the key values based on the field names passed
   * to MigrateSQLMap(). Override this if you need to generate your own key
   * (e.g., the source doesn't have a natural unique key). Be sure to also
   * set any values you generate in $row.
   *
   * @param array $source_key
   * @param object $row
   *
   * @return array
   */
  public function prepareKey($source_key, $row) {
    self::prepareRow($row);
    $key = parent::prepareKey($source_key, $row);
    return $key;
  }

  /**
   * Overrides DrupalNode7Migration::prepareRow().
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
//    $row->title = '';
//    $title_contact = explode("Contact:", $row->project_title_contact);
//    $row->title = trim($title_contact[0]);
//
    
    $row->contact_email = '';

    if (isset($row->Contact)) {
      $contact_parts = explode("<", trim($row->Contact));
      if (isset($contact_parts[1])) {
        
        $contact_name = explode(" ", trim($contact_parts[0]));
        $row->contact_fn = array_shift($contact_name);
        $row->contact_ln = implode(" ", $contact_name);
        $row->contact_email = trim(trim($contact_parts[1], '>'));
      }
    }
//    $row->organisation = $row->org2;
//    if (empty($row->organisation) && !empty($row->org1)) {
//      $row->organisation = $row->org1;
//    }
//    // Make sure the mapping for field_short_title always has a value.
//    if (empty($row->field_project_short_name)) {
//      $row->field_project_short_name = $row->title;
//    }
//
//    // Gather location data from Location module and format for Get Locations.
//    if (!empty($row->field_project_sites)) {
//      $row->locations = $this->loadLocations($row->field_project_sites);
//    }
//
//    // Aggregate values from multiple organisation reference fields.
//    $row->organisations = $row->contacts = array();
//    $organisation_fields = array(
//      'field_project_dev_partner',
//      'field_project_implement_agency',
//      'field_project_lead_organiation'
//    );
//    foreach ($organisation_fields as $field_name) {
//      if (!empty($row->{$field_name})) {
//        $row->organisations = array_merge($row->organisations, $row->{$field_name});
//      }
//    }
//    $row->organisations = $this->uniquify($row->organisations);
//
//    // Aggregate values from multiple contact reference fields.
//    $contact_fields = array(
//      'field_contact',
//      'field_lead_contacts',
//      'field_partner_s_contact_s_',
//      'field_project_agencys_contacts',
//      'field_donors_contacts'
//    );
//    foreach ($contact_fields as $field_name) {
//      if (!empty($row->{$field_name})) {
//        $row->contacts = array_merge($row->contacts, $row->{$field_name});
//      }
//    }
//    $row->contacts = $this->uniquify($row->contacts);

    return TRUE;
  }

  public function fields() {
    return array(
      //'title' => 'Title stripped out of the project title contact column',
      'contact_fn' => 'contact first name',
      'contact_fn' => 'contact last name',
      'contact_email' => 'The contact email stripped out of the project title contact column',
      //'organisation' => 'Consolidated organisation title from org1 or org2',
      //'partner_organisations' => 'exploded',
    );
  }

  protected function csvcolumns() {
    return array();
    return array(
      //array('Lead org', 'Lead organisation'),
      array('organisation', 'Organisation'),
      array('va_tools', 'V&A Tools used'),
      array('title', 'Project Title'),
//      array('project_title_contact', 'Project Title Contact'),
//      array('start_date', 'Start Date'),
//      array('end_date', 'End Date'),
//      array('status', 'Status'),
//      array('duration', 'Duration'),
//      array('vulnerable_sites', 'Vulnerable sites'),
//      array('project_sites', 'Project sites'),
//      array('sector', 'Sector'),
//      array('vulnerabilities_hazards', 'Vulnerabilities Hazards'),
//      array('themes', 'Themes'),
//      array('objectived', 'Objectives'),
//      array('activities', 'Activities'),
//      array('outputs', 'Outputs'),
//      array('funding', 'Total funding (in FJD)'),
//      array('donors', 'Donors'),
//      array('links', 'Other links')
    );
  }

}