<?php

class FccpContactsMigration extends FccpProjectsMigrationBase {

  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->map = new MigrateSQLMap($this->machineName, array(
      'Contact' => array('type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'description' => 'Project title contact',
      ),
      ), MigrateDestinationNode::getKeySchema()
    );
    $this->destination = new MigrateDestinationNode('contact');

    $this->addFieldMapping('title', 'Contact');
    $this->addFieldMapping('field_email', 'contact_email');
    $this->addFieldMapping('field_first_name', 'contact_fn');
    $this->addFieldMapping('field_last_name', 'contact_ln');
    $this->addFieldMapping('status')->defaultValue(NODE_PUBLISHED);
  }


  /**
   * Overrides DrupalNode7Migration::prepareRow().
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    if (empty($row->Contact)) {
      return FALSE;
    }
  }

}
