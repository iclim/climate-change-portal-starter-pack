<?php

class FccpDonorsMigration extends Migration {

  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->source = new MigrateSourceCSV(DRUPAL_ROOT . '/../migrate_data/donors.csv', $this->csvcolumns(), array('header_rows' => 1), $this->fields());

    $this->map = new MigrateSQLMap($this->machineName, array(
      'title' => array('type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'description' => 'Donor Title',
      ),
      ), MigrateDestinationNode::getKeySchema()
    );
    $this->destination = new MigrateDestinationNode('donor');

    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('status')->defaultValue(NODE_PUBLISHED);
  }

  protected function csvcolumns() {
    return array(
      array('title', 'title'),
    );
  }

  public function fields() {
    return array(
    );
  }

  /**
   * Default implementation of prepareKey. This method is called from the source
   * plugin immediately after retrieving the raw data from the source - by
   * default, it simply assigns the key values based on the field names passed
   * to MigrateSQLMap(). Override this if you need to generate your own key
   * (e.g., the source doesn't have a natural unique key). Be sure to also
   * set any values you generate in $row.
   *
   * @param array $source_key
   * @param object $row
   *
   * @return array
   */
  public function prepareKey($source_key, $row) {
    self::prepareRow($row);
    $key = parent::prepareKey($source_key, $row);
    return $key;
  }

  /**
   * Overrides DrupalNode7Migration::prepareRow().
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    $row->title = self::mapDonorName($row->title);

    $exists = db_query('SELECT count(*) FROM {node} where title = !title', array('!title' => $row->title))->fetchField();
    if ($exists) {
      error_log('EXISTS!');
      return FALSE;
    }
  }

  public static function mapDonorName($name) {

    $map = array(
      'Australian Agency for International Development (AusAID)' => array(
        'AusAID',
        'Aus AID',
      ),
      'Global Environment Facility (GEF) Small Grants Program (SGP)' => array(
        'GEF-SGP',
        'GEF- Small Grants',
        'GEF small grant',
        'GEF SGP',
      ),
      'Global Environment Facility (GEF)' => array(
        'GEF',
      )
    );

    foreach ($map as $want => $dont_want) {
      foreach ($dont_want as $v) {
        if (strtolower($name) == strtolower($v)) {
          return $want;
        }
      }
    }

    return $name;
  }

  public static function explodeName($name) {
    $donors = array();
    switch ($name) {
      case "Global Environment Facility (GEF) and Australian Aid (AusAid)":
        $donors = array(
          'Global Environment Facility (GEF)',
          'Australian Agency for International Development (AusAID)'
        );
        break;
      case 'Partners of Live & learn (EU, Canada fund)':
        $donors = array($name);
        break;
    }

    if (!empty($donors)) {
      return array_combine($donors, $donors);
    }


    if (strstr($name, ";")) {
      $sep = ";";
    } else {
      $sep = ",";
    }


    foreach (explode($sep, $name) as $v) {
    
      if (!empty($v)) {
        $v = trim(FccpDonorsMigration::mapDonorName($v));

        $donors[$v] = $v;
      }
    }

    return $donors;
  }

}
