<?php

class FccpOrganisationsMigration extends FccpProjectsMigrationBase {
  public static $migrationArguments = array(
    'dependencies' => array(
      'FccpContactsMigration',
    ),
  );

  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->map = new MigrateSQLMap($this->machineName, array(
      'org2' => array('type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'description' => 'Organisation Name',
      ),
      ), MigrateDestinationNode::getKeySchema()
    );
    $this->destination = new MigrateDestinationNode('organisation');

    $this->addFieldMapping('title', 'organisation');
    $this->addFieldMapping('status')->defaultValue(NODE_PUBLISHED);
  }

  public static function mapDonorName($name) {

    $map = array(
      
    );

    foreach ($map as $want => $dont_want) {
      foreach ($dont_want as $v) {
        if (strtolower($name) == strtolower($v)) {
          return $want;
        }
      }
    }

    return $name;
  }

  public static function explodeName($name) {
    $orgs = array();
    switch ($name) {
      
    }

    if (!empty($orgs)) {
      return array_combine($orgs, $orgs);
    }


    if (strstr($name, ";")) {
      $sep = ";";
    } else {
      $sep = ",";
    }


    foreach (explode($sep, $name) as $v) {

      if (!empty($v)) {
        $v = trim(self::mapDonorName($v));

        $orgs[$v] = $v;
      }
    }

    return $orgs;
  }

}
