<?php


class FccpProjectsMigration extends FccpProjectsMigrationBase {

  public static $migrationArguments = array(
    'dependencies' => array(
      //'FccpOrganisationsMigration',
    ),
  );

  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->map = new MigrateSQLMap($this->machineName, array(
      'Project Title' => array('type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'description' => 'Project Title',
      ),
      ), MigrateDestinationNode::getKeySchema()
    );
    
    $this->destination = new MigrateDestinationNode('product');

    $this->addFieldMapping('status')->defaultValue(NODE_PUBLISHED);

    $this->addFieldMapping('field_project_name', 'Project Title');
//    $this->addFieldMapping('field_short_title', 'field_project_short_name');
//    $this->addFieldMapping('field_start_date', 'start_date');
//    $this->addFieldMapping('field_end_date', 'end_date');
//    $this->addFieldMapping('field_duration', 'duration');
    //$this->addFieldMapping('field_project_total_funding', 'funding');
//
    
//    $this->addFieldMapping('field_actions', 'field_project_major_actions')->callbacks(array($this, 'multiValueToSingle'));;
    $this->addFieldMapping('field_objectives', 'Objectives');
    $this->addFieldMapping('field_activities', 'Activities');
    $this->addFieldMapping('field_outputs', 'Outputs');
//    $this->addFieldMapping('field_project_outcomes', 'field_project_outcomes')->callbacks(array($this, 'multiValueToSingle'));;
//
//    $this->addFieldMapping('field_the_description', 'body');
//    $this->addFieldMapping('field_the_description:format', 'body:format');
//    $this->addFieldMapping('field_the_description:summary', 'body:summary');
//
//    $this->addFieldMapping('field_project_comments', 'field_project_comments');
//    $this->addFieldMapping('field_project_comments:format', 'field_project_comments:format');
//
//    // Reference fields
//
//    $this->addFieldMapping('field_project_type', 'field_project_type')->sourceMigration('ProjectTypes');
//    $this->addFieldMapping('field_project_type:source_type')->defaultValue('tid');
//
//    $this->addFieldMapping('field_project_status', 'field_project_status')->sourceMigration('ProjectStatuses');
//    $this->addFieldMapping('field_project_status:source_type')->defaultValue('tid');
//
//    $this->addFieldMapping('field_project_scope', 'field_project_scope')->sourceMigration('ProjectScopes');
//    $this->addFieldMapping('field_project_scope:source_type')->defaultValue('tid');
//
//    $this->addFieldMapping('field_topics', 'field_project_theme')->sourceMigration('Topics')->callbacks(array($this, 'addParentTopics'));
//    $this->addFieldMapping('field_topics:source_type')->defaultValue('tid');
//
//    $this->addFieldMapping('field_pifacc_themes', 'field_project_pifacc')->sourceMigration('Pifacc');
//    $this->addFieldMapping('field_pifacc_themes:source_type')->defaultValue('tid');
//
//    $this->addFieldMapping('field_tags', 'field_project_glossary_terms')->sourceMigration('ClimateChangeTerminology');
//    $this->addFieldMapping('field_tags:source_type')->defaultValue('tid');
//
//    $this->addFieldMapping('field_hfa_priorities', 'field_project_hfa_priorities')->sourceMigration('HfaPriorities');
//    $this->addFieldMapping('field_hfa_priorities:source_type')->defaultValue('tid');
//
//    $this->addFieldMapping('field_rfa_priorities', 'field_project_rfa_priorities')->sourceMigration('RfaPriorities');
//    $this->addFieldMapping('field_rfa_priorities:source_type')->defaultValue('tid');
//
//    $this->addFieldMapping('field_national_objective', 'field_project_national_obj')->sourceMigration('NationalFrameworks');
//    $this->addFieldMapping('field_national_objective:source_type')->defaultValue('tid');
//
//    $this->addFieldMapping('field_implementing_countries', 'field_country')->sourceMigration('Countries');
//
//    $this->addFieldMapping('field_implementing_agency', 'field_project_implement_agency')->sourceMigration('Organisations');
//
//    $this->addFieldMapping('field_donor_enity', 'field_project_donors')->sourceMigration('Donors');
//
//    $this->addFieldMapping('field_project_sites_3', 'locations');
//
//    // For these fields, multiple source fields get aggregated in prepareRow().
    //$this->addFieldMapping('field_organisation_entity', 'Organisation')->sourceMigration('FccpOrganisationsMigration');

    $this->addFieldMapping('field_organisation_entity', 'Organisation')->separator(',');
    $this->addFieldMapping('field_organisation_entity:create_entity')->defaultValue(TRUE);
    $this->addFieldMapping('field_organisation_entity:source_type')->defaultValue('name');
    $this->addFieldMapping('field_organisation_entity:create_entity_bundle')->defaultValue('organisation');
    $this->addFieldMapping('field_organisation_entity:create_entity_status')->defaultValue(NODE_PUBLISHED);

    $this->addFieldMapping('field_project_contact', 'Contact')->sourceMigration('FccpContactsMigration');
  }

  /**
   * Overrides DrupalNode7Migration::prepareRow().
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    //Replace 1. 2. etc with \n1. \n2. etc to split things on to new lines nicely
    $pattern = '/(\d+\.\s)/';
    foreach (array('Objectives', 'Activities', 'Outputs') as $f) {
      $row->{$f} = preg_replace($pattern, "\n$1", $row->{$f});
    }


//    $row->Objectives = str_replace(";", "\n", $row->Objectives);
//    $row->Activities = str_replace(";", "\n", $row->Activities);
//    $row->Outputs = str_replace(";", "\n", $row->Outputs);

    return TRUE;


    if (empty($row->start_date)) {
      return FALSE;
    }

    if (empty($row->end_date)) {
      return FALSE;
    }

    $dates = array();
    foreach (array('start_date', 'end_date') as $date_col) {

      $date_str = trim($row->{$date_col});

      $date = $format = $matches = null;
      if (preg_match('/(\d{2}\/\d{1,2}\/\d{1,2})/', $date_str, $matches)) {
        $date_str = $matches[1];
        $format = 'y/m/d';
      } else if (preg_match('/(\d{1,2}\/\d{1,2}\/\d{2})/', $date_str, $matches)) {
        $date_str = $matches[1];
        $format = 'm/d/y';
      } else if (preg_match('/(\d{4}\/\d{2})/', $date_str, $matches)) {
        $format = 'Y/m';
        $date_str = $matches[1];
      } else if (preg_match('/(\d{4})/', $date_str, $matches)) {
        $format = 'Y';
        $date_str = $matches[1];
      } else if (preg_match('/(.{3}-\d{2})/', $date_str, $matches)) {
        $format = 'M-d';
        $date_str = $matches[1];
      }

      if ($format) {
        $date = DateTime::createFromFormat($format, $date_str);
      }

      if (empty($date)) {
        error_log("bad $date_col: skipping project [$date_str] [$format] " . $row->title);
        return FALSE;
      } else {
        $dates[$date_col] = $date->format(DATE_FORMAT_ISO);
      }
    }

    

    if (strtotime($dates['end_date']) < strtotime($dates['start_date'])) {
      print_r($row);
      print_r($dates);
      return FALSE;
    } else {
      $row->start_date = $dates['start_date'];
      $row->end_date = $dates['end_date'];
    }
  }

}