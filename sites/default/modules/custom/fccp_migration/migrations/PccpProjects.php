<?php

class PccpProjects extends Migration {

  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->source = new MigrateSourceCSV(DRUPAL_ROOT . '/../migrate_data/projects_from_pccp.csv', $this->csvcolumns(), array('header_rows' => 1), $this->fields());

    $this->map = new MigrateSQLMap($this->machineName, array(
      'title' => array('type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'description' => 'Project Name',
      ),
      ), MigrateDestinationNode::getKeySchema()
    );

    $this->destination = new MigrateDestinationNode('product');

    $this->addFieldMapping('status')->defaultValue(NODE_PUBLISHED);

    $this->addFieldMapping('field_project_name', 'title');
    $this->addFieldMapping('field_project_status', 'status');
    $this->addFieldMapping('field_the_description', 'description');
    $this->addFieldMapping('field_project_total_funding', 'total_funding');
    $this->addFieldMapping('field_start_date', 'start_date');
    $this->addFieldMapping('field_end_date', 'end_date');
    $this->addFieldMapping('field_duration', 'duration');
    $this->addFieldMapping('field_short_title', 'short_title');
    $this->addFieldMapping('field_project_type', 'project_type')->separator(',');
    $this->addFieldMapping('field_amount_donor_currency_', 'amount_donor_currency');
    $this->addFieldMapping('field_project_scope', 'project_scope');

    $this->addFieldMapping('field_implementing_agency', 'implementing_agency')->separator(',');
    $this->addFieldMapping('field_implementing_agency:create_entity')->defaultValue(TRUE);
    $this->addFieldMapping('field_implementing_agency:source_type')->defaultValue('name');
    $this->addFieldMapping('field_implementing_agency:create_entity_bundle')->defaultValue('organisation');
    $this->addFieldMapping('field_implementing_agency:create_entity_status')->defaultValue(NODE_PUBLISHED);

    //$this->addFieldMapping('field_duration', 'duration');
  }

  public function prepareKey($source_key, $row) {
    self::prepareRow($row);
    $key = parent::prepareKey($source_key, $row);
    return $key;
  }

  /**
   * Overrides DrupalNode7Migration::prepareRow().
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    $format = 'l, F j, Y';
    foreach (array('start_date', 'end_date') as $date_fname) {
      if ($row->{$date_fname}) {
        $date_ob = DateTime::createFromFormat($format, $row->{$date_fname});
        if ($date_ob) {
          $row->{$date_fname} = $date_ob->format(DATE_FORMAT_ISO);
        }
      }
    }

    return TRUE;
  }

  public function fields() {
    return array(
    );
  }

  protected function csvcolumns() {
    return array(
      array('title', ''),
      array('status', 'Status'),
      array('description', 'Description'),
      array('total_funding', 'Total Funding'),
      array('project_status', 'Project Status'),
      array('start_date', 'Start Date'),
      array('end_date', 'End Date'),
      array('duration', 'Duration'),
      array('short_title', 'Short Title'),
      array('project_type', 'Project Type'),
      array('amount_donor_currency', 'Amount (donor currency)'),
      array('project_scope', 'Project Scope'),
      array('implementing_agency', 'Implementing Agency'),
    );
  }

}
