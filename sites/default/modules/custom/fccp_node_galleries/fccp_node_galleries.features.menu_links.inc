<?php
/**
 * @file
 * fccp_node_galleries.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function fccp_node_galleries_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: management_node-gallery:admin/config/content/node-gallery.
  $menu_links['management_node-gallery:admin/config/content/node-gallery'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/config/content/node-gallery',
    'router_path' => 'admin/config/content/node-gallery',
    'link_title' => 'Node Gallery',
    'options' => array(
      'attributes' => array(
        'title' => 'Create and manage your Node Gallery relationships.',
      ),
      'identifier' => 'management_node-gallery:admin/config/content/node-gallery',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'management_content-authoring:admin/config/content',
  );
  // Exported menu link: navigation_gallery-item:node/add/node-gallery-item.
  $menu_links['navigation_gallery-item:node/add/node-gallery-item'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/node-gallery-item',
    'router_path' => 'node/add/node-gallery-item',
    'link_title' => 'Gallery Item',
    'options' => array(
      'attributes' => array(
        'title' => 'A gallery node item.',
      ),
      'identifier' => 'navigation_gallery-item:node/add/node-gallery-item',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'navigation_add-content:node/add',
  );
  // Exported menu link: navigation_gallery:node/add/node-gallery-gallery.
  $menu_links['navigation_gallery:node/add/node-gallery-gallery'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/node-gallery-gallery',
    'router_path' => 'node/add/node-gallery-gallery',
    'link_title' => 'Gallery',
    'options' => array(
      'attributes' => array(
        'title' => 'A gallery of nodes.',
      ),
      'identifier' => 'navigation_gallery:node/add/node-gallery-gallery',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'navigation_add-content:node/add',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Gallery');
  t('Gallery Item');
  t('Node Gallery');

  return $menu_links;
}
