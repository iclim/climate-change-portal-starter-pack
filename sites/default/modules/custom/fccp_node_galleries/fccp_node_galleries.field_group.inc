<?php
/**
 * @file
 * fccp_node_galleries.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function fccp_node_galleries_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_gallery|node|node_gallery_gallery|form';
  $field_group->group_name = 'group_gallery';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'node_gallery_gallery';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Gallery',
    'weight' => '0',
    'children' => array(
      0 => 'group_general',
      1 => 'group_relations',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-gallery field-group-htabs',
      ),
    ),
  );
  $field_groups['group_gallery|node|node_gallery_gallery|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_general|node|node_gallery_gallery|form';
  $field_group->group_name = 'group_general';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'node_gallery_gallery';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_gallery';
  $field_group->data = array(
    'label' => 'General Information',
    'weight' => '9',
    'children' => array(
      0 => 'body',
      1 => 'title',
      2 => 'path',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-general field-group-htab',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_general|node|node_gallery_gallery|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_related_content|node|node_gallery_gallery|default';
  $field_group->group_name = 'group_related_content';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'node_gallery_gallery';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Related content',
    'weight' => '7',
    'children' => array(
      0 => 'field_related_documents_entity',
      1 => 'field_related_projects_entity',
      2 => 'field_country',
      3 => 'field_organisation_entity',
      4 => 'field_related_news',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-related-content field-group-fieldset',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_related_content|node|node_gallery_gallery|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_relations|node|node_gallery_gallery|form';
  $field_group->group_name = 'group_relations';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'node_gallery_gallery';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_gallery';
  $field_group->data = array(
    'label' => 'Related Content',
    'weight' => '11',
    'children' => array(
      0 => 'field_related_documents_entity',
      1 => 'field_related_projects_entity',
      2 => 'field_country',
      3 => 'field_organisation_entity',
      4 => 'field_related_news',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-relations field-group-htab',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_relations|node|node_gallery_gallery|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Gallery');
  t('General Information');
  t('Related Content');
  t('Related content');

  return $field_groups;
}
