<?php
/**
 * @file
 * fccp_workflow.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fccp_workflow_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
