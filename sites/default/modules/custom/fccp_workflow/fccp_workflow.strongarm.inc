<?php
/**
 * @file
 * fccp_workflow.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function fccp_workflow_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_moderation_default_state_contact';
  $strongarm->value = 'draft';
  $export['workbench_moderation_default_state_contact'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_moderation_default_state_country';
  $strongarm->value = 'draft';
  $export['workbench_moderation_default_state_country'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_moderation_default_state_document';
  $strongarm->value = 'draft';
  $export['workbench_moderation_default_state_document'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_moderation_default_state_donor';
  $strongarm->value = 'draft';
  $export['workbench_moderation_default_state_donor'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_moderation_default_state_link';
  $strongarm->value = 'draft';
  $export['workbench_moderation_default_state_link'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_moderation_default_state_organisation';
  $strongarm->value = 'draft';
  $export['workbench_moderation_default_state_organisation'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_moderation_default_state_product';
  $strongarm->value = 'draft';
  $export['workbench_moderation_default_state_product'] = $strongarm;

  return $export;
}
