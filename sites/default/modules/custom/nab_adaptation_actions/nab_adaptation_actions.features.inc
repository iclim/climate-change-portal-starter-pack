<?php
/**
 * @file
 * nab_adaptation_actions.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function nab_adaptation_actions_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function nab_adaptation_actions_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function nab_adaptation_actions_node_info() {
  $items = array(
    'adaptation_action' => array(
      'name' => t('Adaptation Action'),
      'base' => 'node_content',
      'description' => t('Climate Change Adaptation Actions'),
      'has_title' => '1',
      'title_label' => t('Action Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
