<?php
/**
 * @file
 * nab_adaptation_actions.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function nab_adaptation_actions_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_classification_groups|node|adaptation_action|default';
  $field_group->group_name = 'group_classification_groups';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'adaptation_action';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Classification Groups',
    'weight' => '2',
    'children' => array(
      0 => 'group_classification',
    ),
    'format_type' => 'accordion',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-classification-groups field-group-accordion',
        'effect' => 'bounceslide',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_classification_groups|node|adaptation_action|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_classification|node|adaptation_action|default';
  $field_group->group_name = 'group_classification';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'adaptation_action';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_classification_groups';
  $field_group->data = array(
    'label' => 'Classification',
    'weight' => '7',
    'children' => array(
      0 => 'field_major_impact',
      1 => 'field_topics',
      2 => 'field_sector_impact',
    ),
    'format_type' => 'accordion-item',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-classification field-group-accordion',
        'effect' => 'bounceslide',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_classification|node|adaptation_action|default'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Classification');
  t('Classification Groups');

  return $field_groups;
}
