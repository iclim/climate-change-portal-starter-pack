<?php
/**
 * @file
 * nab_adaptation_actions.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function nab_adaptation_actions_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'adaptation_actions';
  $page->task = 'page';
  $page->admin_title = 'Adaptation Actions';
  $page->admin_description = '';
  $page->path = 'adaptation-actions';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_adaptation_actions__panel';
  $handler->task = 'page';
  $handler->subtask = 'adaptation_actions';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(
      0 => array(
        'identifier' => 'View: Adaptation Actions',
        'keyword' => 'view',
        'name' => 'view:adaptation_actions-ctools_context_1',
        'id' => 1,
      ),
    ),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'nab_searchpages';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'header' => NULL,
      'top_left' => NULL,
      'top_middle' => NULL,
      'top_right' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom_left' => NULL,
      'bottom_middle' => NULL,
      'bottom_right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Adaptation Actions';
  $display->uuid = 'dbeb6ea0-d96b-4844-96f4-59a1dcbe132f';
  $display->storage_type = '';
  $display->storage_id = '';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-50b94ee3-bb6b-40da-945e-02b85348b5e6';
  $pane->panel = 'left';
  $pane->type = 'views_exposed';
  $pane->subtype = 'views_exposed';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'context_view:adaptation_actions-ctools_context_1_1',
    'override_title' => 1,
    'override_title_text' => 'Keyword Search',
    'override_title_heading' => 'span',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '50b94ee3-bb6b-40da-945e-02b85348b5e6';
  $display->content['new-50b94ee3-bb6b-40da-945e-02b85348b5e6'] = $pane;
  $display->panels['left'][0] = 'new-50b94ee3-bb6b-40da-945e-02b85348b5e6';
  $pane = new stdClass();
  $pane->pid = 'new-9666dbf8-19f0-4032-a84b-989cf2067afc';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-crqI031cl6jxlwqt0YlxGuWNuR2EDLyw';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Topics',
    'override_title_heading' => 'span',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '9666dbf8-19f0-4032-a84b-989cf2067afc';
  $display->content['new-9666dbf8-19f0-4032-a84b-989cf2067afc'] = $pane;
  $display->panels['left'][1] = 'new-9666dbf8-19f0-4032-a84b-989cf2067afc';
  $pane = new stdClass();
  $pane->pid = 'new-9305d979-f793-42c0-876f-3b0b9f7aed16';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-0EgsPmvCYJQaXdrPWYtXV1h5uBeP9Mr9';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Major Impacts',
    'override_title_heading' => 'span',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '9305d979-f793-42c0-876f-3b0b9f7aed16';
  $display->content['new-9305d979-f793-42c0-876f-3b0b9f7aed16'] = $pane;
  $display->panels['left'][2] = 'new-9305d979-f793-42c0-876f-3b0b9f7aed16';
  $pane = new stdClass();
  $pane->pid = 'new-6aa7b928-08c8-4608-8714-64204e079b4b';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-OUBocNpHvf4Dtw7IM5oZGvjhuLV4RtBn';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Sector Impact',
    'override_title_heading' => 'span',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '6aa7b928-08c8-4608-8714-64204e079b4b';
  $display->content['new-6aa7b928-08c8-4608-8714-64204e079b4b'] = $pane;
  $display->panels['left'][3] = 'new-6aa7b928-08c8-4608-8714-64204e079b4b';
  $pane = new stdClass();
  $pane->pid = 'new-b448c4fb-64d1-44e3-b69a-ac14350a9ee6';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'adaptation_actions-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'b448c4fb-64d1-44e3-b69a-ac14350a9ee6';
  $display->content['new-b448c4fb-64d1-44e3-b69a-ac14350a9ee6'] = $pane;
  $display->panels['right'][0] = 'new-b448c4fb-64d1-44e3-b69a-ac14350a9ee6';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-b448c4fb-64d1-44e3-b69a-ac14350a9ee6';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['adaptation_actions'] = $page;

  return $pages;

}
