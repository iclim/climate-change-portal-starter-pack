<?php
/**
 * @file
 * nab_announcements.file_default_displays.inc
 */

/**
 * Implements hook_file_default_displays().
 */
function nab_announcements_file_default_displays() {
  $export = array();

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__announcement__file_field_colorbox';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = array(
    'colorbox_node_style' => '',
    'colorbox_node_style_first' => '',
    'colorbox_image_style' => '',
    'colorbox_gallery' => 'post',
    'colorbox_gallery_custom' => '',
    'colorbox_caption' => 'auto',
    'colorbox_caption_custom' => '',
  );
  $export['image__announcement__file_field_colorbox'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__announcement__file_field_download_filesize';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = '';
  $export['image__announcement__file_field_download_filesize'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__announcement__file_field_file_date';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = '';
  $export['image__announcement__file_field_file_date'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__announcement__file_field_file_default';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = '';
  $export['image__announcement__file_field_file_default'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__announcement__file_field_file_download_link';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = array(
    'text' => 'Download [file:name]',
  );
  $export['image__announcement__file_field_file_download_link'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__announcement__file_field_file_table';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = '';
  $export['image__announcement__file_field_file_table'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__announcement__file_field_file_url_plain';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = '';
  $export['image__announcement__file_field_file_url_plain'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__announcement__file_field_filename_filesize';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = '';
  $export['image__announcement__file_field_filename_filesize'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__announcement__file_field_icon_filesize';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = '';
  $export['image__announcement__file_field_icon_filesize'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__announcement__file_field_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'announcement',
    'image_link' => '',
  );
  $export['image__announcement__file_field_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__announcement__file_field_media_large_icon';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = array(
    'image_style' => '',
  );
  $export['image__announcement__file_field_media_large_icon'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__announcement__file_field_node_title_file';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = '';
  $export['image__announcement__file_field_node_title_file'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__announcement__file_field_node_title_linked_to_file';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = '';
  $export['image__announcement__file_field_node_title_linked_to_file'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__announcement__file_field_slick';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = array(
    'optionset' => 'default',
    'skin' => '',
    'optionset_thumbnail' => '',
    'skin_thumbnail' => '',
    'skin_arrows' => '',
    'skin_dots' => '',
    'asnavfor_main' => '',
    'asnavfor_thumbnail' => '',
    'asnavfor_auto' => 0,
    'image_style' => '',
    'thumbnail_style' => '',
    'thumbnail_hover' => 0,
    'slide_layout' => '',
    'thumbnail_caption' => '',
    'slide_overlay' => '',
    'slide_title' => '',
    'slide_link' => '',
    'slide_classes' => '',
    'slide_caption' => array(
      'field_file_image_alt_text' => 0,
      'field_file_image_title_text' => 0,
    ),
    'view_mode' => '',
    'media_switch' => '',
    'iframe_lazy' => 0,
    'aspect_ratio' => '',
    'colorbox_style' => '',
    'mousewheel' => 0,
    'markup' => 0,
    'cache' => '',
    'override' => 0,
    'overridables' => array(
      'arrows' => 0,
      'autoplay' => 0,
      'dots' => 0,
      'draggable' => 0,
    ),
    'current_view_mode' => 'announcement',
  );
  $export['image__announcement__file_field_slick'] = $file_display;

  return $export;
}
