<?php
/**
 * @file
 * nab_beans.bean.inc
 */

/**
 * Implements hook_bean_admin_ui_types().
 */
function nab_beans_bean_admin_ui_types() {
  $export = array();

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'basic_content';
  $bean_type->label = 'Basic Content';
  $bean_type->options = '';
  $bean_type->description = 'Basic title and body block, good for snippets of custom HTML.';
  $export['basic_content'] = $bean_type;

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'featured_content';
  $bean_type->label = 'Featured Content';
  $bean_type->options = '';
  $bean_type->description = 'A featured content block used to promote a link to a site section or webpage.';
  $export['featured_content'] = $bean_type;

  return $export;
}
