<?php
/**
 * @file
 * nab_beans.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function nab_beans_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'bean|basic_content|form';
  $ds_layout->entity_type = 'bean';
  $ds_layout->bundle = 'basic_content';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'label',
        1 => 'title',
        2 => 'field_body',
      ),
      'hidden' => array(
        3 => 'view_mode',
        4 => 'revision',
        5 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'label' => 'ds_content',
      'title' => 'ds_content',
      'field_body' => 'ds_content',
      'view_mode' => 'hidden',
      'revision' => 'hidden',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['bean|basic_content|form'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'bean|raw_html|form';
  $ds_layout->entity_type = 'bean';
  $ds_layout->bundle = 'raw_html';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'label',
        1 => 'title',
        2 => 'field_raw_html_contents',
      ),
      'hidden' => array(
        3 => 'revision',
        4 => 'view_mode',
        5 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'label' => 'ds_content',
      'title' => 'ds_content',
      'field_raw_html_contents' => 'ds_content',
      'revision' => 'hidden',
      'view_mode' => 'hidden',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['bean|raw_html|form'] = $ds_layout;

  return $export;
}
