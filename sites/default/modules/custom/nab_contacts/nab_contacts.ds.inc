<?php
/**
 * @file
 * nab_contacts.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function nab_contacts_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|contact|search_index';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'contact';
  $ds_fieldsetting->view_mode = 'search_index';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|contact|search_index'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|organisation|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'organisation';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'related_contacts' => array(
      'weight' => '18',
      'label' => 'above',
      'format' => 'default',
      'formatter_settings' => array(
        'show_title' => 0,
        'title_wrapper' => '',
        'ctools' => 'a:3:{s:4:"conf";a:16:{s:23:"override_pager_settings";i:0;s:9:"use_pager";i:0;s:14:"nodes_per_page";s:1:"0";s:8:"pager_id";s:1:"0";s:6:"offset";s:1:"0";s:9:"more_link";i:0;s:10:"feed_icons";i:0;s:10:"panel_args";i:0;s:12:"link_to_view";i:0;s:4:"args";s:0:"";s:3:"url";s:0:"";s:7:"display";s:7:"default";s:7:"context";a:1:{i:0;s:29:"argument_entity_id:node_1.nid";}s:14:"override_title";i:1;s:19:"override_title_text";s:16:"Related Contacts";s:22:"override_title_heading";s:2:"h3";}s:4:"type";s:5:"views";s:7:"subtype";s:21:"organisation_contacts";}',
        'load_terms' => 0,
      ),
    ),
  );
  $export['node|organisation|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|organisation|search_index';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'organisation';
  $ds_fieldsetting->view_mode = 'search_index';
  $ds_fieldsetting->settings = array(
    'related_contacts' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|organisation|search_index'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function nab_contacts_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'related_contacts';
  $ds_field->label = 'Related Contacts';
  $ds_field->field_type = 7;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = 'organisation|*';
  $ds_field->properties = array(
    'default' => array(),
    'settings' => array(
      'show_title' => array(
        'type' => 'checkbox',
      ),
      'title_wrapper' => array(
        'type' => 'textfield',
        'description' => 'Eg: h1, h2, p',
      ),
      'ctools' => array(
        'type' => 'ctools',
      ),
    ),
  );
  $export['related_contacts'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function nab_contacts_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|contact|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'contact';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_fluid';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_image_file',
        1 => 'body',
        2 => 'field_first_name',
        3 => 'field_last_name',
        4 => 'field_email',
        5 => 'field_city',
        6 => 'field_phone',
        7 => 'field_fax',
        8 => 'field_gender',
        9 => 'field_position',
        10 => 'field_department',
        11 => 'field_organisation_entity',
        12 => 'field_street_address',
        13 => 'field_contact_country',
      ),
      'right' => array(
        14 => 'field_other_information',
        15 => 'field_contact_type',
        16 => 'field_related_links_entity',
        17 => 'field_related_content_entity',
        18 => 'field_related_organisation',
        19 => 'field_related_projects_entity',
        20 => 'field_tags',
        21 => 'group_other_info_group',
        22 => 'group_other_information',
      ),
    ),
    'fields' => array(
      'field_image_file' => 'left',
      'body' => 'left',
      'field_first_name' => 'left',
      'field_last_name' => 'left',
      'field_email' => 'left',
      'field_city' => 'left',
      'field_phone' => 'left',
      'field_fax' => 'left',
      'field_gender' => 'left',
      'field_position' => 'left',
      'field_department' => 'left',
      'field_organisation_entity' => 'left',
      'field_street_address' => 'left',
      'field_contact_country' => 'left',
      'field_other_information' => 'right',
      'field_contact_type' => 'right',
      'field_related_links_entity' => 'right',
      'field_related_content_entity' => 'right',
      'field_related_organisation' => 'right',
      'field_related_projects_entity' => 'right',
      'field_tags' => 'right',
      'group_other_info_group' => 'right',
      'group_other_information' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['node|contact|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|contact|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'contact';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'group_contact',
        1 => 'title',
        2 => 'path',
        3 => 'field_first_name',
        6 => 'field_last_name',
        7 => 'field_email',
        10 => 'field_street_address',
        11 => 'field_city',
        13 => 'field_contact_country',
        15 => 'field_department',
        16 => 'field_phone',
        17 => 'field_fax',
        18 => 'field_gender',
        19 => 'field_other_information',
        20 => 'field_organisation_entity',
        21 => 'field_position',
        22 => 'field_image_file',
        23 => 'field_contact_type',
        24 => 'field_topics',
        26 => 'group_general',
      ),
      'hidden' => array(
        4 => 'body',
        5 => 'field_related_links_entity',
        8 => 'field_related_projects_entity',
        9 => 'field_related_organisation',
        12 => 'field_related_content_entity',
        14 => 'field_tags',
        25 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'group_contact' => 'ds_content',
      'title' => 'ds_content',
      'path' => 'ds_content',
      'field_first_name' => 'ds_content',
      'body' => 'hidden',
      'field_related_links_entity' => 'hidden',
      'field_last_name' => 'ds_content',
      'field_email' => 'ds_content',
      'field_related_projects_entity' => 'hidden',
      'field_related_organisation' => 'hidden',
      'field_street_address' => 'ds_content',
      'field_city' => 'ds_content',
      'field_related_content_entity' => 'hidden',
      'field_contact_country' => 'ds_content',
      'field_tags' => 'hidden',
      'field_department' => 'ds_content',
      'field_phone' => 'ds_content',
      'field_fax' => 'ds_content',
      'field_gender' => 'ds_content',
      'field_other_information' => 'ds_content',
      'field_organisation_entity' => 'ds_content',
      'field_position' => 'ds_content',
      'field_image_file' => 'ds_content',
      'field_contact_type' => 'ds_content',
      'field_topics' => 'ds_content',
      '_add_existing_field' => 'hidden',
      'group_general' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|contact|form'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|contact|search_index';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'contact';
  $ds_layout->view_mode = 'search_index';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_image_file',
        2 => 'body',
        3 => 'field_first_name',
        4 => 'field_last_name',
        5 => 'field_email',
        6 => 'field_city',
        7 => 'field_phone',
        8 => 'field_fax',
        9 => 'field_gender',
        10 => 'field_position',
        11 => 'field_department',
        12 => 'field_organisation_entity',
        13 => 'field_street_address',
        14 => 'field_contact_country',
        15 => 'field_other_information',
        16 => 'field_contact_type',
        17 => 'field_related_links_entity',
        18 => 'field_related_content_entity',
        19 => 'field_related_organisation',
        20 => 'field_related_projects_entity',
        21 => 'field_tags',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_image_file' => 'ds_content',
      'body' => 'ds_content',
      'field_first_name' => 'ds_content',
      'field_last_name' => 'ds_content',
      'field_email' => 'ds_content',
      'field_city' => 'ds_content',
      'field_phone' => 'ds_content',
      'field_fax' => 'ds_content',
      'field_gender' => 'ds_content',
      'field_position' => 'ds_content',
      'field_department' => 'ds_content',
      'field_organisation_entity' => 'ds_content',
      'field_street_address' => 'ds_content',
      'field_contact_country' => 'ds_content',
      'field_other_information' => 'ds_content',
      'field_contact_type' => 'ds_content',
      'field_related_links_entity' => 'ds_content',
      'field_related_content_entity' => 'ds_content',
      'field_related_organisation' => 'ds_content',
      'field_related_projects_entity' => 'ds_content',
      'field_tags' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|contact|search_index'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|organisation|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'organisation';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_fluid';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'group_additional_information',
        1 => 'field_related_links_entity',
        4 => 'field_acronym',
        5 => 'field_partner_category',
        6 => 'field_organisation_type',
        7 => 'field_related_funds',
        8 => 'field_donor_type',
        9 => 'field_tags',
        10 => 'field_related_projects_entity',
        11 => 'field_related_documents_entity',
        12 => 'field_related_news',
        13 => 'field_country',
        14 => 'field_related_events',
        15 => 'related_contacts',
        16 => 'group_organisation_details',
        17 => 'group_related_content',
      ),
      'right' => array(
        2 => 'field_image',
        3 => 'field_the_description',
      ),
    ),
    'fields' => array(
      'group_additional_information' => 'left',
      'field_related_links_entity' => 'left',
      'field_image' => 'right',
      'field_the_description' => 'right',
      'field_acronym' => 'left',
      'field_partner_category' => 'left',
      'field_organisation_type' => 'left',
      'field_related_funds' => 'left',
      'field_donor_type' => 'left',
      'field_tags' => 'left',
      'field_related_projects_entity' => 'left',
      'field_related_documents_entity' => 'left',
      'field_related_news' => 'left',
      'field_country' => 'left',
      'field_related_events' => 'left',
      'related_contacts' => 'left',
      'group_organisation_details' => 'left',
      'group_related_content' => 'left',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['node|organisation|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|organisation|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'organisation';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'group_org',
        1 => 'path',
        3 => 'group_general',
        4 => 'group_relations',
        5 => 'title',
        6 => 'field_acronym',
        7 => 'field_the_description',
        8 => 'field_image',
        9 => 'field_organisation_type',
        10 => 'field_related_links_entity',
        11 => 'field_partner_category',
        12 => 'field_topics',
        13 => 'field_tags',
        14 => 'field_country',
        15 => 'field_related_funds',
        16 => 'field_related_projects_entity',
        17 => 'field_related_documents_entity',
        18 => 'field_related_news',
        19 => 'field_related_events',
      ),
      'hidden' => array(
        2 => 'field_donor_type',
        20 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'group_org' => 'ds_content',
      'path' => 'ds_content',
      'field_donor_type' => 'hidden',
      'group_general' => 'ds_content',
      'group_relations' => 'ds_content',
      'title' => 'ds_content',
      'field_acronym' => 'ds_content',
      'field_the_description' => 'ds_content',
      'field_image' => 'ds_content',
      'field_organisation_type' => 'ds_content',
      'field_related_links_entity' => 'ds_content',
      'field_partner_category' => 'ds_content',
      'field_topics' => 'ds_content',
      'field_tags' => 'ds_content',
      'field_country' => 'ds_content',
      'field_related_funds' => 'ds_content',
      'field_related_projects_entity' => 'ds_content',
      'field_related_documents_entity' => 'ds_content',
      'field_related_news' => 'ds_content',
      'field_related_events' => 'ds_content',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|organisation|form'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|organisation|search_index';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'organisation';
  $ds_layout->view_mode = 'search_index';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'related_contacts',
        2 => 'field_the_description',
        3 => 'field_related_links_entity',
        4 => 'field_image',
        5 => 'field_acronym',
        6 => 'field_partner_category',
        7 => 'field_organisation_type',
        8 => 'field_donor_type',
        9 => 'field_related_funds',
        10 => 'field_related_projects_entity',
        11 => 'field_tags',
        12 => 'field_related_documents_entity',
        13 => 'field_related_news',
        14 => 'field_country',
        15 => 'field_related_events',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'related_contacts' => 'ds_content',
      'field_the_description' => 'ds_content',
      'field_related_links_entity' => 'ds_content',
      'field_image' => 'ds_content',
      'field_acronym' => 'ds_content',
      'field_partner_category' => 'ds_content',
      'field_organisation_type' => 'ds_content',
      'field_donor_type' => 'ds_content',
      'field_related_funds' => 'ds_content',
      'field_related_projects_entity' => 'ds_content',
      'field_tags' => 'ds_content',
      'field_related_documents_entity' => 'ds_content',
      'field_related_news' => 'ds_content',
      'field_country' => 'ds_content',
      'field_related_events' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|organisation|search_index'] = $ds_layout;

  return $export;
}
