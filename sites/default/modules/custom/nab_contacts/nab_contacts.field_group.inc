<?php
/**
 * @file
 * nab_contacts.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function nab_contacts_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_additional_information|node|organisation|default';
  $field_group->group_name = 'group_additional_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Additional Information',
    'weight' => '0',
    'children' => array(
      0 => 'group_organisation_details',
      1 => 'group_related_content',
    ),
    'format_type' => 'accordion',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-additional-information field-group-accordion',
        'effect' => 'bounceslide',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_additional_information|node|organisation|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_contact|node|contact|form';
  $field_group->group_name = 'group_contact';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'contact';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Contact',
    'weight' => '0',
    'children' => array(
      0 => 'group_general',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-contact field-group-htabs',
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_contact|node|contact|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_general|node|contact|form';
  $field_group->group_name = 'group_general';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'contact';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_contact';
  $field_group->data = array(
    'label' => 'General Information',
    'weight' => '22',
    'children' => array(
      0 => 'field_first_name',
      1 => 'field_last_name',
      2 => 'field_email',
      3 => 'field_city',
      4 => 'field_department',
      5 => 'field_phone',
      6 => 'field_fax',
      7 => 'field_gender',
      8 => 'field_other_information',
      9 => 'field_position',
      10 => 'field_image_file',
      11 => 'field_street_address',
      12 => 'field_contact_type',
      13 => 'field_organisation_entity',
      14 => 'field_contact_country',
      15 => 'field_topics',
      16 => 'title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'General Information',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-general field-group-htab',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_general|node|contact|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_general|node|organisation|form';
  $field_group->group_name = 'group_general';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_org';
  $field_group->data = array(
    'label' => 'General Information',
    'weight' => '14',
    'children' => array(
      0 => 'field_acronym',
      1 => 'field_image',
      2 => 'field_organisation_type',
      3 => 'field_related_links_entity',
      4 => 'field_partner_category',
      5 => 'field_the_description',
      6 => 'field_topics',
      7 => 'title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'General Information',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_general|node|organisation|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_organisation_details|node|organisation|default';
  $field_group->group_name = 'group_organisation_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_additional_information';
  $field_group->data = array(
    'label' => 'Organisation Details',
    'weight' => '20',
    'children' => array(
      0 => 'field_acronym',
      1 => 'field_organisation_type',
      2 => 'field_partner_category',
      3 => 'field_donor_type',
      4 => 'field_tags',
    ),
    'format_type' => 'accordion-item',
    'format_settings' => array(
      'label' => 'Organisation Details',
      'instance_settings' => array(
        'classes' => 'group-organisation-details field-group-accordion-item',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_organisation_details|node|organisation|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_org|node|organisation|form';
  $field_group->group_name = 'group_org';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Org',
    'weight' => '0',
    'children' => array(
      0 => 'group_general',
      1 => 'group_relations',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Org',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => '',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_org|node|organisation|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_other_info_group|node|contact|default';
  $field_group->group_name = 'group_other_info_group';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'contact';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Other Info Group',
    'weight' => '21',
    'children' => array(
      0 => 'group_other_information',
    ),
    'format_type' => 'accordion',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-other-info-group field-group-accordion',
        'effect' => 'bounceslide',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_other_info_group|node|contact|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_other_information|node|contact|default';
  $field_group->group_name = 'group_other_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'contact';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_other_info_group';
  $field_group->data = array(
    'label' => 'Other Details',
    'weight' => '26',
    'children' => array(
      0 => 'field_other_information',
      1 => 'field_tags',
      2 => 'field_contact_type',
      3 => 'field_related_links_entity',
      4 => 'field_related_content_entity',
      5 => 'field_related_projects_entity',
      6 => 'field_related_organisation',
    ),
    'format_type' => 'accordion-item',
    'format_settings' => array(
      'label' => 'Other Details',
      'instance_settings' => array(
        'classes' => 'group-other-information field-group-accordion-item',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_other_information|node|contact|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_related_content|node|organisation|default';
  $field_group->group_name = 'group_related_content';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_additional_information';
  $field_group->data = array(
    'label' => 'Related Content',
    'weight' => '21',
    'children' => array(
      0 => 'field_related_funds',
      1 => 'field_related_projects_entity',
      2 => 'field_related_documents_entity',
      3 => 'field_related_news',
      4 => 'field_related_events',
      5 => 'field_country',
      6 => 'related_contacts',
    ),
    'format_type' => 'accordion-item',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => 'group-related-content field-group-accordion-item',
      ),
    ),
  );
  $field_groups['group_related_content|node|organisation|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_relations|node|contact|form';
  $field_group->group_name = 'group_relations';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'contact';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Related Information',
    'weight' => '3',
    'children' => array(
      0 => 'field_tags',
      1 => 'field_related_links_entity',
      2 => 'field_related_content_entity',
      3 => 'field_related_projects_entity',
      4 => 'field_related_organisation',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-relations field-group-htab',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_relations|node|contact|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_relations|node|organisation|form';
  $field_group->group_name = 'group_relations';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_org';
  $field_group->data = array(
    'label' => 'Related Content',
    'weight' => '15',
    'children' => array(
      0 => 'field_related_funds',
      1 => 'field_related_projects_entity',
      2 => 'field_related_documents_entity',
      3 => 'field_related_news',
      4 => 'field_related_events',
      5 => 'field_country',
      6 => 'field_tags',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Related Content',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_relations|node|organisation|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Additional Information');
  t('Contact');
  t('General Information');
  t('Org');
  t('Organisation Details');
  t('Other Details');
  t('Other Info Group');
  t('Related Content');
  t('Related Information');

  return $field_groups;
}
