<?php
/**
 * @file
 * nab_contacts.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function nab_contacts_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'organisation_contacts';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Organisation Contacts';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Contextual filter: Content: Organisation (field_organisation_entity) */
  $handler->display->display_options['arguments']['field_organisation_entity_target_id']['id'] = 'field_organisation_entity_target_id';
  $handler->display->display_options['arguments']['field_organisation_entity_target_id']['table'] = 'field_data_field_organisation_entity';
  $handler->display->display_options['arguments']['field_organisation_entity_target_id']['field'] = 'field_organisation_entity_target_id';
  $handler->display->display_options['arguments']['field_organisation_entity_target_id']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['field_organisation_entity_target_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_organisation_entity_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_organisation_entity_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_organisation_entity_target_id']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'contact' => 'contact',
  );
  $export['organisation_contacts'] = $view;

  $view = new view();
  $view->name = 'organisation_search';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'search_api_index_pccp_full_index';
  $view->human_name = 'Organisation Search';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Results';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Search';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'views-row';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'title' => 'title',
    'field_acronym' => 'field_acronym',
  );
  /* Field: Indexed Node: Node ID */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'search_api_index_pccp_full_index';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nid']['separator'] = '';
  $handler->display->display_options['fields']['nid']['link_to_entity'] = 0;
  /* Field: Indexed Node: Image */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'search_api_index_pccp_full_index';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_image']['alter']['path'] = '/node/[nid]';
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_image']['element_wrapper_class'] = 'result-image';
  $handler->display->display_options['fields']['field_image']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['type'] = 'file_rendered';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'file_view_mode' => 'teaser',
  );
  /* Field: Indexed Node: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'search_api_index_pccp_full_index';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_type'] = 'h3';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_entity'] = 1;
  /* Field: Indexed Node: Position */
  $handler->display->display_options['fields']['field_position']['id'] = 'field_position';
  $handler->display->display_options['fields']['field_position']['table'] = 'search_api_index_pccp_full_index';
  $handler->display->display_options['fields']['field_position']['field'] = 'field_position';
  $handler->display->display_options['fields']['field_position']['label'] = '';
  $handler->display->display_options['fields']['field_position']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_position']['hide_empty'] = TRUE;
  /* Field: Indexed Node: Acronym */
  $handler->display->display_options['fields']['field_acronym']['id'] = 'field_acronym';
  $handler->display->display_options['fields']['field_acronym']['table'] = 'search_api_index_pccp_full_index';
  $handler->display->display_options['fields']['field_acronym']['field'] = 'field_acronym';
  $handler->display->display_options['fields']['field_acronym']['label'] = '';
  $handler->display->display_options['fields']['field_acronym']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_acronym']['alter']['text'] = '([field_acronym-value])';
  $handler->display->display_options['fields']['field_acronym']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_acronym']['hide_empty'] = TRUE;
  /* Field: Indexed Node: Description */
  $handler->display->display_options['fields']['field_description']['id'] = 'field_description';
  $handler->display->display_options['fields']['field_description']['table'] = 'search_api_index_pccp_full_index';
  $handler->display->display_options['fields']['field_description']['field'] = 'field_description';
  $handler->display->display_options['fields']['field_description']['label'] = '';
  $handler->display->display_options['fields']['field_description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_description']['type'] = 'text_trimmed';
  $handler->display->display_options['fields']['field_description']['settings'] = array(
    'trim_length' => '600',
  );
  /* Field: Indexed Node: Description */
  $handler->display->display_options['fields']['field_the_description']['id'] = 'field_the_description';
  $handler->display->display_options['fields']['field_the_description']['table'] = 'search_api_index_pccp_full_index';
  $handler->display->display_options['fields']['field_the_description']['field'] = 'field_the_description';
  $handler->display->display_options['fields']['field_the_description']['label'] = '';
  $handler->display->display_options['fields']['field_the_description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_the_description']['type'] = 'text_summary_or_trimmed';
  $handler->display->display_options['fields']['field_the_description']['settings'] = array(
    'trim_length' => '600',
  );
  /* Field: Indexed Node: Email */
  $handler->display->display_options['fields']['field_email']['id'] = 'field_email';
  $handler->display->display_options['fields']['field_email']['table'] = 'search_api_index_pccp_full_index';
  $handler->display->display_options['fields']['field_email']['field'] = 'field_email';
  $handler->display->display_options['fields']['field_email']['label'] = '';
  $handler->display->display_options['fields']['field_email']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_email']['hide_empty'] = TRUE;
  /* Field: Indexed Node: Organisation */
  $handler->display->display_options['fields']['field_organisation_entity']['id'] = 'field_organisation_entity';
  $handler->display->display_options['fields']['field_organisation_entity']['table'] = 'search_api_index_pccp_full_index';
  $handler->display->display_options['fields']['field_organisation_entity']['field'] = 'field_organisation_entity';
  $handler->display->display_options['fields']['field_organisation_entity']['label'] = '';
  $handler->display->display_options['fields']['field_organisation_entity']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_organisation_entity']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['field_organisation_entity']['view_mode'] = 'full';
  $handler->display->display_options['fields']['field_organisation_entity']['bypass_access'] = 0;
  /* Sort criterion: Indexed Node: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'search_api_index_pccp_full_index';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  $handler->display->display_options['filter_groups']['groups'] = array(
    1 => 'AND',
    2 => 'OR',
  );
  /* Filter criterion: Search: Fulltext search */
  $handler->display->display_options['filters']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['table'] = 'search_api_index_pccp_full_index';
  $handler->display->display_options['filters']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['group'] = 1;
  $handler->display->display_options['filters']['search_api_views_fulltext']['exposed'] = TRUE;
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator_id'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['identifier'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Indexed Node: Content type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'search_api_index_pccp_full_index';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'organisation' => 'organisation',
  );
  $handler->display->display_options['filters']['type']['group'] = 2;
  /* Filter criterion: Indexed Node: Contact Type */
  $handler->display->display_options['filters']['field_contact_type']['id'] = 'field_contact_type';
  $handler->display->display_options['filters']['field_contact_type']['table'] = 'search_api_index_pccp_full_index';
  $handler->display->display_options['filters']['field_contact_type']['field'] = 'field_contact_type';
  $handler->display->display_options['filters']['field_contact_type']['value'] = array(
    160 => '160',
  );
  $handler->display->display_options['filters']['field_contact_type']['group'] = 2;

  /* Display: Organisation Search */
  $handler = $view->new_display('page', 'Organisation Search', 'page');
  $handler->display->display_options['enabled'] = FALSE;
  $handler->display->display_options['exposed_block'] = TRUE;
  $handler->display->display_options['path'] = 'organisation-search';

  /* Display: Organisation Search Pane */
  $handler = $view->new_display('panel_pane', 'Organisation Search Pane', 'organisation_search_pane');
  $handler->display->display_options['exposed_block'] = TRUE;

  /* Display: Data export */
  $handler = $view->new_display('views_data_export', 'Data export', 'views_data_export_1');
  $handler->display->display_options['defaults']['access'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'access data exports';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'views_data_export_csv';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Indexed Node: Node ID */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'search_api_index_pccp_full_index';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nid']['separator'] = '';
  $handler->display->display_options['fields']['nid']['link_to_entity'] = 0;
  /* Field: Indexed Node: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'search_api_index_pccp_full_index';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'title';
  $handler->display->display_options['fields']['title']['element_type'] = 'h3';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_entity'] = 1;
  /* Field: Indexed Node: Position */
  $handler->display->display_options['fields']['field_position']['id'] = 'field_position';
  $handler->display->display_options['fields']['field_position']['table'] = 'search_api_index_pccp_full_index';
  $handler->display->display_options['fields']['field_position']['field'] = 'field_position';
  $handler->display->display_options['fields']['field_position']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_position']['hide_empty'] = TRUE;
  /* Field: Indexed Node: Acronym */
  $handler->display->display_options['fields']['field_acronym']['id'] = 'field_acronym';
  $handler->display->display_options['fields']['field_acronym']['table'] = 'search_api_index_pccp_full_index';
  $handler->display->display_options['fields']['field_acronym']['field'] = 'field_acronym';
  $handler->display->display_options['fields']['field_acronym']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_acronym']['alter']['text'] = '([field_acronym-value])';
  $handler->display->display_options['fields']['field_acronym']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_acronym']['hide_empty'] = TRUE;
  /* Field: Indexed Node: Description */
  $handler->display->display_options['fields']['field_description']['id'] = 'field_description';
  $handler->display->display_options['fields']['field_description']['table'] = 'search_api_index_pccp_full_index';
  $handler->display->display_options['fields']['field_description']['field'] = 'field_description';
  $handler->display->display_options['fields']['field_description']['label'] = 'Organisation description';
  $handler->display->display_options['fields']['field_description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_description']['type'] = 'text_trimmed';
  $handler->display->display_options['fields']['field_description']['settings'] = array(
    'trim_length' => '600',
  );
  /* Field: Indexed Node: Description */
  $handler->display->display_options['fields']['field_the_description']['id'] = 'field_the_description';
  $handler->display->display_options['fields']['field_the_description']['table'] = 'search_api_index_pccp_full_index';
  $handler->display->display_options['fields']['field_the_description']['field'] = 'field_the_description';
  $handler->display->display_options['fields']['field_the_description']['label'] = 'Contact description';
  $handler->display->display_options['fields']['field_the_description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_the_description']['type'] = 'text_summary_or_trimmed';
  $handler->display->display_options['fields']['field_the_description']['settings'] = array(
    'trim_length' => '600',
  );
  /* Field: Indexed Node: Email */
  $handler->display->display_options['fields']['field_email']['id'] = 'field_email';
  $handler->display->display_options['fields']['field_email']['table'] = 'search_api_index_pccp_full_index';
  $handler->display->display_options['fields']['field_email']['field'] = 'field_email';
  $handler->display->display_options['fields']['field_email']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_email']['hide_empty'] = TRUE;
  /* Field: Indexed Node: Organisation */
  $handler->display->display_options['fields']['field_organisation_entity']['id'] = 'field_organisation_entity';
  $handler->display->display_options['fields']['field_organisation_entity']['table'] = 'search_api_index_pccp_full_index';
  $handler->display->display_options['fields']['field_organisation_entity']['field'] = 'field_organisation_entity';
  $handler->display->display_options['fields']['field_organisation_entity']['label'] = 'Contact organisation';
  $handler->display->display_options['fields']['field_organisation_entity']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_organisation_entity']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['field_organisation_entity']['view_mode'] = 'full';
  $handler->display->display_options['fields']['field_organisation_entity']['bypass_access'] = 0;
  $handler->display->display_options['path'] = 'directory.csv';
  $handler->display->display_options['displays'] = array(
    'organisation_search_pane' => 'organisation_search_pane',
    'default' => 0,
    'page' => 0,
  );
  $export['organisation_search'] = $view;

  return $export;
}
