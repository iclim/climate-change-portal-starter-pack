<?php
/**
 * @file
 * nab_documents.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function nab_documents_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_categorisation|node|document|default';
  $field_group->group_name = 'group_categorisation';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'document';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_other_info_group';
  $field_group->data = array(
    'label' => 'Categorisation',
    'weight' => '54',
    'children' => array(
      0 => 'field_focus_area',
      1 => 'field_topics',
      2 => 'field_resource_type',
      3 => 'field_tags',
      4 => 'field_target_audience',
    ),
    'format_type' => 'accordion-item',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => 'group-categorisation field-group-accordion-item',
      ),
    ),
  );
  $field_groups['group_categorisation|node|document|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_categorisation|node|document|form';
  $field_group->group_name = 'group_categorisation';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'document';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_document';
  $field_group->data = array(
    'label' => 'Categorisation',
    'weight' => '3',
    'children' => array(
      0 => 'field_focus_area',
      1 => 'field_topics',
      2 => 'field_tags',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Categorisation',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-categorisation field-group-htab',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_categorisation|node|document|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_document_information|node|document|default';
  $field_group->group_name = 'group_document_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'document';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_document_tabs';
  $field_group->data = array(
    'label' => 'Document Information',
    'weight' => '12',
    'children' => array(
      0 => 'field_author',
      1 => 'field_rights_statement',
      2 => 'field_attribution_statement',
      3 => 'field_corporate_author',
      4 => 'field_general_note',
      5 => 'field_variant_title',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => 'group-document-information field-group-accordion-item',
        'description' => '',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_document_information|node|document|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_document_tabs|node|document|default';
  $field_group->group_name = 'group_document_tabs';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'document';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Document Tabs',
    'weight' => '3',
    'children' => array(
      0 => 'group_document_information',
      1 => 'group_publication_details',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-document-tabs field-group-htabs',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_document_tabs|node|document|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_document|node|document|form';
  $field_group->group_name = 'group_document';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'document';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Document',
    'weight' => '0',
    'children' => array(
      0 => 'group_categorisation',
      1 => 'group_educational',
      2 => 'group_files',
      3 => 'group_general',
      4 => 'group_other',
      5 => 'group_relations',
      6 => 'group_stakeholders',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Document',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-document field-group-htabs',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_document|node|document|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_educational_resource|node|document|default';
  $field_group->group_name = 'group_educational_resource';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'document';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_other_info_group';
  $field_group->data = array(
    'label' => 'Educational Resource',
    'weight' => '56',
    'children' => array(
      0 => 'field_educational_resource_type',
      1 => 'field_financed_supported_by',
      2 => 'field_subject_strand',
      3 => 'field_climate_change_topic',
      4 => 'field_drm_topic',
      5 => 'field_tvet_training_area_focus',
      6 => 'field_regional_focus',
      7 => 'field_utilised_in',
    ),
    'format_type' => 'accordion-item',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => 'group-educational-resource field-group-accordion-item',
      ),
    ),
  );
  $field_groups['group_educational_resource|node|document|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_educational|node|document|form';
  $field_group->group_name = 'group_educational';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'document';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_document';
  $field_group->data = array(
    'label' => 'Educational Resource',
    'weight' => '7',
    'children' => array(
      0 => 'field_educational_resource_type',
      1 => 'field_financed_supported_by',
      2 => 'field_subject_strand',
      3 => 'field_climate_change_topic',
      4 => 'field_drm_topic',
      5 => 'field_tvet_training_area_focus',
      6 => 'field_regional_focus',
      7 => 'field_utilised_in',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Educational Resource',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-educational field-group-htab',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_educational|node|document|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_files|node|document|default';
  $field_group->group_name = 'group_files';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'document';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Files',
    'weight' => '20',
    'children' => array(),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => 'group-files field-group-div',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_files|node|document|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_files|node|document|form';
  $field_group->group_name = 'group_files';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'document';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_document';
  $field_group->data = array(
    'label' => 'Files',
    'weight' => '2',
    'children' => array(
      0 => 'field_document_files',
      1 => 'field_cover_image',
      2 => 'field_url',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Files',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-files field-group-htab',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_files|node|document|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_general|node|document|form';
  $field_group->group_name = 'group_general';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'document';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_document';
  $field_group->data = array(
    'label' => 'General Information',
    'weight' => '1',
    'children' => array(
      0 => 'field_publication_year',
      1 => 'field_author',
      2 => 'field_the_description',
      3 => 'field_resource_type',
      4 => 'title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'General Information',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-general field-group-htab',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_general|node|document|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_linkwrapper|node|document|default';
  $field_group->group_name = 'group_linkwrapper';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'document';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Link Wrapper',
    'weight' => '2',
    'children' => array(
      0 => 'field_document_files',
      1 => 'field_url',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Link Wrapper',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-linkwrapper field-group-html-element',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_linkwrapper|node|document|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_other_info_group|node|document|default';
  $field_group->group_name = 'group_other_info_group';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'document';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Other Information',
    'weight' => '7',
    'children' => array(
      0 => 'group_categorisation',
      1 => 'group_educational_resource',
      2 => 'group_related_content',
    ),
    'format_type' => 'accordion',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-other-info-group field-group-accordion',
        'effect' => 'bounceslide',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_other_info_group|node|document|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_other|node|document|form';
  $field_group->group_name = 'group_other';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'document';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_document';
  $field_group->data = array(
    'label' => 'Other',
    'weight' => '5',
    'children' => array(
      0 => 'field_rights_statement',
      1 => 'field_attribution_statement',
      2 => 'field_bibliography_note',
      3 => 'field_cataloging_source',
      4 => 'field_corporate_author',
      5 => 'field_edition',
      6 => 'field_event',
      7 => 'field_general_note',
      8 => 'field_isbn',
      9 => 'field_issn',
      10 => 'field_physical_description',
      11 => 'field_place_of_publication',
      12 => 'field_publication_day_month',
      13 => 'field_publisher',
      14 => 'field_series',
      15 => 'field_source_of_acquisition',
      16 => 'field_target_audience',
      17 => 'field_variant_title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Other',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-other field-group-htab',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_other|node|document|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_publication_details|node|document|default';
  $field_group->group_name = 'group_publication_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'document';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_document_tabs';
  $field_group->data = array(
    'label' => 'Publication Details',
    'weight' => '13',
    'children' => array(
      0 => 'field_publication_year',
      1 => 'field_bibliography_note',
      2 => 'field_cataloging_source',
      3 => 'field_edition',
      4 => 'field_event',
      5 => 'field_isbn',
      6 => 'field_issn',
      7 => 'field_physical_description',
      8 => 'field_place_of_publication',
      9 => 'field_publication_day_month',
      10 => 'field_publisher',
      11 => 'field_series',
      12 => 'field_source_of_acquisition',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-publication-details field-group-htab',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_publication_details|node|document|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_related_content|node|document|default';
  $field_group->group_name = 'group_related_content';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'document';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_other_info_group';
  $field_group->data = array(
    'label' => 'Related Information',
    'weight' => '55',
    'children' => array(
      0 => 'field_related_corporations',
      1 => 'field_related_name_s_',
      2 => 'field_related_links_entity',
      3 => 'field_related_projects_entity',
      4 => 'field_country',
    ),
    'format_type' => 'accordion-item',
    'format_settings' => array(
      'label' => 'Related Information',
      'instance_settings' => array(
        'classes' => 'group-related-content field-group-accordion-item',
      ),
      'formatter' => 'closed',
    ),
  );
  $field_groups['group_related_content|node|document|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_relations|node|document|form';
  $field_group->group_name = 'group_relations';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'document';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_document';
  $field_group->data = array(
    'label' => 'Related Information',
    'weight' => '4',
    'children' => array(
      0 => 'field_related_corporations',
      1 => 'field_related_name_s_',
      2 => 'field_related_links_entity',
      3 => 'field_country',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Related Information',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-relations field-group-htab',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_relations|node|document|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_stakeholders|node|document|form';
  $field_group->group_name = 'group_stakeholders';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'document';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_document';
  $field_group->data = array(
    'label' => 'Stakeholders',
    'weight' => '6',
    'children' => array(
      0 => 'field_organisation_entity',
      1 => 'field_project_contact',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-stakeholders field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_stakeholders|node|document|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_verification|node|document|form';
  $field_group->group_name = 'group_verification';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'document';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_document';
  $field_group->data = array(
    'label' => 'Verification',
    'weight' => '6',
    'children' => array(
      0 => 'field_verified_pccp',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-verification field-group-htab',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_verification|node|document|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Categorisation');
  t('Document');
  t('Document Information');
  t('Document Tabs');
  t('Educational Resource');
  t('Files');
  t('General Information');
  t('Link Wrapper');
  t('Other');
  t('Other Information');
  t('Publication Details');
  t('Related Information');
  t('Stakeholders');
  t('Verification');

  return $field_groups;
}
