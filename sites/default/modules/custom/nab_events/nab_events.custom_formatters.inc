<?php
/**
 * @file
 * nab_events.custom_formatters.inc
 */

/**
 * Implements hook_custom_formatters_defaults().
 */
function nab_events_custom_formatters_defaults() {
  $export = array();

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'event_search_term';
  $formatter->label = 'event_search_term';
  $formatter->description = '';
  $formatter->mode = 'php';
  $formatter->field_types = 'taxonomy_term_reference';
  $formatter->code = '$field_name = $variables["#instance"][\'field_name\'];
$url = "/events-search?f[0]=".$field_name.":";

if (count($variables["#items"])):

   foreach($variables["#items"] as $item):
      $term = taxonomy_term_load($item[\'tid\']);
      print \'<div><a href="\'.$url .$item[\'tid\']  .\'">\'.$term->name.\'</a></div>\';
   endforeach;

endif;';
  $formatter->fapi = '';
  $export['event_search_term'] = $formatter;

  return $export;
}
