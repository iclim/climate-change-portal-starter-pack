<?php
/**
 * @file
 * nab_events.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function nab_events_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|event|search_index';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'event';
  $ds_fieldsetting->view_mode = 'search_index';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|event|search_index'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function nab_events_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|event|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'event';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'bootstrap_8_4';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_image',
        1 => 'field_the_description',
        3 => 'field_venue',
        5 => 'field_tags',
        6 => 'field_document_files',
        7 => 'field_location',
      ),
      'right' => array(
        2 => 'field_event_type',
        4 => 'field_start_end_date',
        8 => 'group_additional_info',
        9 => 'field_organisation_entity',
        10 => 'field_country',
        11 => 'field_related_links_entity',
        12 => 'group_event_details',
        13 => 'group_related_info',
      ),
    ),
    'fields' => array(
      'field_image' => 'left',
      'field_the_description' => 'left',
      'field_event_type' => 'right',
      'field_venue' => 'left',
      'field_start_end_date' => 'right',
      'field_tags' => 'left',
      'field_document_files' => 'left',
      'field_location' => 'left',
      'group_additional_info' => 'right',
      'field_organisation_entity' => 'right',
      'field_country' => 'right',
      'field_related_links_entity' => 'right',
      'group_event_details' => 'right',
      'group_related_info' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|event|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|event|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'event';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'group_event',
        1 => 'path',
        2 => 'title',
        3 => 'field_start_end_date',
        6 => 'field_image',
        7 => 'field_event_type',
        9 => 'field_the_description',
        11 => 'field_venue',
        12 => 'field_location',
        13 => 'field_tags',
        14 => 'field_document_files',
        15 => 'field_organisation_entity',
        17 => 'group_general',
        18 => 'group_relations',
      ),
      'hidden' => array(
        4 => 'field_country',
        5 => 'field_related_links_entity',
        8 => 'field_related_documents_entity',
        10 => 'field_location_text_',
        16 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'group_event' => 'ds_content',
      'path' => 'ds_content',
      'title' => 'ds_content',
      'field_start_end_date' => 'ds_content',
      'field_country' => 'hidden',
      'field_related_links_entity' => 'hidden',
      'field_image' => 'ds_content',
      'field_event_type' => 'ds_content',
      'field_related_documents_entity' => 'hidden',
      'field_the_description' => 'ds_content',
      'field_location_text_' => 'hidden',
      'field_venue' => 'ds_content',
      'field_location' => 'ds_content',
      'field_tags' => 'ds_content',
      'field_document_files' => 'ds_content',
      'field_organisation_entity' => 'ds_content',
      '_add_existing_field' => 'hidden',
      'group_general' => 'ds_content',
      'group_relations' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|event|form'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|event|search_index';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'event';
  $ds_layout->view_mode = 'search_index';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_start_date',
        2 => 'field_the_description',
        3 => 'field_venue',
        4 => 'field_end_date',
        5 => 'field_location_map_',
        6 => 'field_event_type',
        7 => 'field_tags',
        8 => 'field_document_files',
        9 => 'field_organisation_entity',
        10 => 'field_country',
        11 => 'field_related_links_entity',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_start_date' => 'ds_content',
      'field_the_description' => 'ds_content',
      'field_venue' => 'ds_content',
      'field_end_date' => 'ds_content',
      'field_location_map_' => 'ds_content',
      'field_event_type' => 'ds_content',
      'field_tags' => 'ds_content',
      'field_document_files' => 'ds_content',
      'field_organisation_entity' => 'ds_content',
      'field_country' => 'ds_content',
      'field_related_links_entity' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|event|search_index'] = $ds_layout;

  return $export;
}
