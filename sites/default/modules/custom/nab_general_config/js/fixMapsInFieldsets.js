(function ($) {
  Drupal.behaviors.nab_general_config = {
    attach: function (context, settings) {
      $('fieldset.group-sites').on('collapsed', function (e) {
        //When its not collapsed
        if (!e.value) {
          //Wait for the fieldset time (200) plus a bit
          setTimeout(function () {
            //Redo all the maps
            $.each(Drupal.settings.getlocations, (function (k, v) {
              Drupal.getlocations.redoMap(k);
            }));
          }, 200 + 50);
        }
      });
    }
  }
})(jQuery);


