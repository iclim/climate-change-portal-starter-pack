<?php
/**
 * @file
 * nab_general_config.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function nab_general_config_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|country|search_index';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'country';
  $ds_fieldsetting->view_mode = 'search_index';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|country|search_index'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|donor|search_index';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'donor';
  $ds_fieldsetting->view_mode = 'search_index';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|donor|search_index'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|page|search_index';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'page';
  $ds_fieldsetting->view_mode = 'search_index';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|page|search_index'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function nab_general_config_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'file|image|default';
  $ds_layout->entity_type = 'file';
  $ds_layout->bundle = 'image';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'file',
      ),
    ),
    'fields' => array(
      'file' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['file|image|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'file|image|original';
  $ds_layout->entity_type = 'file';
  $ds_layout->bundle = 'image';
  $ds_layout->view_mode = 'original';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'file',
      ),
    ),
    'fields' => array(
      'file' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['file|image|original'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'file|image|teaser';
  $ds_layout->entity_type = 'file';
  $ds_layout->bundle = 'image';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'file',
      ),
    ),
    'fields' => array(
      'file' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['file|image|teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|country|search_index';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'country';
  $ds_layout->view_mode = 'search_index';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_related_projects_entity',
        2 => 'field_related_documents_entity',
        3 => 'field_related_content_entity',
        4 => 'field_related_news',
        5 => 'field_related_events',
        6 => 'field_related_links_entity',
        7 => 'field_country_overview',
        8 => 'field_country_national_prioritie',
        9 => 'field_country_key_national_poli',
        10 => 'field_country_adaptation',
        11 => 'field_country_partnerships',
        12 => 'field_country_climate_informatio',
        13 => 'field_country_education',
        14 => 'field_country_mitigation',
        15 => 'field_country_ongoing_challenges',
        16 => 'field_country_governance',
        17 => 'field_country_focal_points',
        18 => 'field_country_references',
        19 => 'field_flag_image',
        20 => 'field_iso_code',
        21 => 'field_population',
        22 => 'field_languages',
        23 => 'field_map_image',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_related_projects_entity' => 'ds_content',
      'field_related_documents_entity' => 'ds_content',
      'field_related_content_entity' => 'ds_content',
      'field_related_news' => 'ds_content',
      'field_related_events' => 'ds_content',
      'field_related_links_entity' => 'ds_content',
      'field_country_overview' => 'ds_content',
      'field_country_national_prioritie' => 'ds_content',
      'field_country_key_national_poli' => 'ds_content',
      'field_country_adaptation' => 'ds_content',
      'field_country_partnerships' => 'ds_content',
      'field_country_climate_informatio' => 'ds_content',
      'field_country_education' => 'ds_content',
      'field_country_mitigation' => 'ds_content',
      'field_country_ongoing_challenges' => 'ds_content',
      'field_country_governance' => 'ds_content',
      'field_country_focal_points' => 'ds_content',
      'field_country_references' => 'ds_content',
      'field_flag_image' => 'ds_content',
      'field_iso_code' => 'ds_content',
      'field_population' => 'ds_content',
      'field_languages' => 'ds_content',
      'field_map_image' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|country|search_index'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|donor|search_index';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'donor';
  $ds_layout->view_mode = 'search_index';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_related_organisation',
        2 => 'field_aid_type',
        3 => 'field_objectives',
        4 => 'field_fund_status',
        5 => 'field_country_focus',
        6 => 'field_application_procedure',
        7 => 'field_pccp_sectoral_focus',
        8 => 'field_logo',
        9 => 'field_contact_details',
        10 => 'field_related_funds',
        11 => 'field_country',
        12 => 'field_website',
        13 => 'field_related_projects_entity',
        14 => 'field_related_documents_entity',
        15 => 'field_global_funding_usd',
        16 => 'field_global_funding_allocation_',
        17 => 'field_cofinancing',
        18 => 'field_financing_modality',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_related_organisation' => 'ds_content',
      'field_aid_type' => 'ds_content',
      'field_objectives' => 'ds_content',
      'field_fund_status' => 'ds_content',
      'field_country_focus' => 'ds_content',
      'field_application_procedure' => 'ds_content',
      'field_pccp_sectoral_focus' => 'ds_content',
      'field_logo' => 'ds_content',
      'field_contact_details' => 'ds_content',
      'field_related_funds' => 'ds_content',
      'field_country' => 'ds_content',
      'field_website' => 'ds_content',
      'field_related_projects_entity' => 'ds_content',
      'field_related_documents_entity' => 'ds_content',
      'field_global_funding_usd' => 'ds_content',
      'field_global_funding_allocation_' => 'ds_content',
      'field_cofinancing' => 'ds_content',
      'field_financing_modality' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|donor|search_index'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|page|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'page';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'bootstrap_12';
  $ds_layout->settings = array(
    'regions' => array(
      'central' => array(
        0 => 'body',
      ),
    ),
    'fields' => array(
      'body' => 'central',
    ),
    'classes' => array(),
    'wrappers' => array(
      'central' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|page|full'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|page|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'page';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'bootstrap_8';
  $ds_layout->settings = array(
    'regions' => array(
      'central' => array(
        0 => 'body',
        1 => 'node_link',
      ),
    ),
    'fields' => array(
      'body' => 'central',
      'node_link' => 'central',
    ),
    'classes' => array(),
    'wrappers' => array(
      'central' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|page|teaser'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function nab_general_config_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'medium';
  $ds_view_mode->label = 'Medium';
  $ds_view_mode->entities = array(
    'file' => 'file',
  );
  $export['medium'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'original';
  $ds_view_mode->label = 'Original';
  $ds_view_mode->entities = array(
    'file' => 'file',
  );
  $export['original'] = $ds_view_mode;

  return $export;
}
