<?php
/**
 * @file
 * nab_general_config.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function nab_general_config_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-about-us.
  $menus['menu-about-us'] = array(
    'menu_name' => 'menu-about-us',
    'title' => 'About us',
    'description' => 'Mega menu about us',
  );
  // Exported menu: menu-news-and-events.
  $menus['menu-news-and-events'] = array(
    'menu_name' => 'menu-news-and-events',
    'title' => 'News and events',
    'description' => 'Mega menu news and events',
  );
  // Exported menu: menu-projects.
  $menus['menu-projects'] = array(
    'menu_name' => 'menu-projects',
    'title' => 'Projects',
    'description' => 'Mega menu projects',
  );
  // Exported menu: menu-resources.
  $menus['menu-resources'] = array(
    'menu_name' => 'menu-resources',
    'title' => 'Resources',
    'description' => 'Mega menu resources',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('About us');
  t('Mega menu about us');
  t('Mega menu news and events');
  t('Mega menu projects');
  t('Mega menu resources');
  t('News and events');
  t('Projects');
  t('Resources');

  return $menus;
}
