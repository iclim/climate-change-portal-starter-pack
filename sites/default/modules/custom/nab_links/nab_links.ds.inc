<?php
/**
 * @file
 * nab_links.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function nab_links_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|link|search_index';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'link';
  $ds_fieldsetting->view_mode = 'search_index';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|link|search_index'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function nab_links_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|link|search_index';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'link';
  $ds_layout->view_mode = 'search_index';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_url',
        2 => 'field_description',
        3 => 'field_topics',
        4 => 'field_related_content_entity',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_url' => 'ds_content',
      'field_description' => 'ds_content',
      'field_topics' => 'ds_content',
      'field_related_content_entity' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|link|search_index'] = $ds_layout;

  return $export;
}
