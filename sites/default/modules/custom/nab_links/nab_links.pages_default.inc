<?php
/**
 * @file
 * nab_links.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function nab_links_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'links';
  $page->task = 'page';
  $page->admin_title = 'Links';
  $page->admin_description = '';
  $page->path = 'links';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Links',
    'name' => 'main-menu',
    'weight' => '50',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_links__panel';
  $handler->task = 'page';
  $handler->subtask = 'links';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(
      0 => array(
        'identifier' => 'View: Links',
        'keyword' => 'view',
        'name' => 'view:links-ctools_context_1',
        'id' => 1,
      ),
    ),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'nab_searchpages';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'header' => NULL,
      'top_left' => NULL,
      'top_middle' => NULL,
      'top_right' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom_left' => NULL,
      'bottom_middle' => NULL,
      'bottom_right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'ca9e2a07-50fc-407f-a430-30e2f2daf431';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_links__panel';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-82db3674-af5b-426a-8342-892160bf0682';
  $pane->panel = 'left';
  $pane->type = 'views_exposed';
  $pane->subtype = 'views_exposed';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'context_view:links-ctools_context_1_1',
    'override_title' => 1,
    'override_title_text' => 'Keyword Search',
    'override_title_heading' => 'span',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '82db3674-af5b-426a-8342-892160bf0682';
  $display->content['new-82db3674-af5b-426a-8342-892160bf0682'] = $pane;
  $display->panels['left'][0] = 'new-82db3674-af5b-426a-8342-892160bf0682';
  $pane = new stdClass();
  $pane->pid = 'new-638c03fa-4cb4-4283-877a-fa1fd1173e58';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-crqI031cl6jxlwqt0YlxGuWNuR2EDLyw';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Topics',
    'override_title_heading' => 'span',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '638c03fa-4cb4-4283-877a-fa1fd1173e58';
  $display->content['new-638c03fa-4cb4-4283-877a-fa1fd1173e58'] = $pane;
  $display->panels['left'][1] = 'new-638c03fa-4cb4-4283-877a-fa1fd1173e58';
  $pane = new stdClass();
  $pane->pid = 'new-60d9bfff-902d-44b3-8465-9ddb9b337b97';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'links-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '60d9bfff-902d-44b3-8465-9ddb9b337b97';
  $display->content['new-60d9bfff-902d-44b3-8465-9ddb9b337b97'] = $pane;
  $display->panels['right'][0] = 'new-60d9bfff-902d-44b3-8465-9ddb9b337b97';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['links'] = $page;

  return $pages;

}
