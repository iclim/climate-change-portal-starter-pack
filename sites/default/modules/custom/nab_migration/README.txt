Description:
============

This module includes migrations to bring content from the existing Drupal 7
version of nab.vu into the SPREP Climate Portal Starter Pack.

Original CSV data came from "Vanuatu NAB.VU metadata updated by RB.xlsx", which
was imported into Google Sheets and modified.

Modified version:
https://docs.google.com/spreadsheets/d/1Sk04ZeZpZC2wKhjLRferKrz7H9j1troh8e-wNlLkg_c/edit?usp=sharing

Requirements:
=============

1. Connection info for the old database must be added to settings.php:

    $databases['migrate'] = array(
      'default' =>
        array (
          'database' => 'nab',
          'username' => 'dbuser',
          'password' => 'dbpass',
          'host' => 'localhost',
          'port' => '',
          'driver' => 'mysql',
          'prefix' => 'main_',
        ),
    );

Note: It's necessary to use the key "migrate".

2. Files (images, PDFs, etc.) from the old site must be copied into the starter
pack files directory. The Files migration expects everything in "public://nab/".

Usage:
======

1. Load the Starter Pack database dump
2. Enable the module and set up the requirements.
3. Run "drush ms". If it shows totals the database connect is set up correctly.
4. Run "drush mi --all" to migration everything.

Known issues:
=============

- Running some migrations may show that some items were ignored. That's normal,
  those items are just being mapped as opposed to created as new entities.
- Some migrations will report item counts for "Total" and none for "Imported".
- DocumentTypes migration will say that matching terms cannot be found.