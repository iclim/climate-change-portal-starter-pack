<?php
/**
 * @file
 * Contains MigrateGeoJSONList and MigrateGeoJSONItem.
 */

/**
 * List class for handling GeoJSON.
 */
class MigrateGeoJSONList extends MigrateListJSON {

  protected function getIDsFromJSON(array $data) {
    $ids = array();

    if (!empty($data['features'])) {
      $ids = array_keys($data['features']);
      // Need to shift the array by 1 so we don't start at 0
      // since MigrateSourceList::getNextRow() will skip empty IDs.
      foreach ($ids as &$id) {
        $id++;
      }
    }

    return $ids;
  }

  public function computeCount() {
    $count = 0;
    $json = file_get_contents($this->listUrl);
    if ($json) {
      $data = drupal_json_decode($json);
      if (isset($data['features'])) {
        $count = count($data['features']);
      }
    }
    return $count;
  }
}

/**
 * Item class for handling GeoJSON.
 */
class MigrateGeoJSONItem extends MigrateItemJSON {

  protected $data;

  public function getItem($id) {

    // All IDs have been shifted by 1 to avoid zero values
    // @see MigrateGeoJSONList::getIDsFromJSON().
    $key = $id - 1;

    // Cache the parsed JSON.
    if (!isset($this->data)) {
      $json = file_get_contents($this->itemUrl);
      if ($json) {
        $this->data = drupal_json_decode($json);
      }
    }

    // Pull out the data for this particular ID and create a row.
    if (isset($this->data['features'][$key]) && $this->data['features'][$key]['type'] == 'Feature') {
      $data = $this->data['features'][$key];
      $row = new stdClass();
      $row->properties = $data['properties'];
      $row->geometry = $data['geometry'];
      return $row;
    }

    return NULL;
  }
}
