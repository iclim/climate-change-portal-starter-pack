<?php
/**
 * @file
 * Contains MigrateGetLocationsFieldHandler.
 */

/**
 * Class MigrateGetLocationsFieldHandler
 *
 * Field mapping plugin for getlocations_fields type fields that takes an array
 * of location values corresponding with the getlocations_fields table.
 */
class MigrateGetLocationsFieldHandler extends MigrateFieldHandler {

  /**
   * Overrides MigrateFieldHandler::__construct().
   */
  public function __construct() {
    $this->registerTypes(array('getlocations_fields'));
  }

  /**
   * Implements MigrateFieldHandler::prepare().
   */
  public function prepare($entity, array $field_info, array $instance, array $values) {
    $result = array();

    // Get Locations won't handle "update" migrations well. New entities only.
    if (!empty($entity->nid)) {
      return FALSE;
    }

    $language = $this->getFieldLanguage($entity, $field_info, array());

    foreach (array_values($values) as $delta => $value) {
      $result[$language][$delta] = $value;
    }
    
    return $result;
  }
}
