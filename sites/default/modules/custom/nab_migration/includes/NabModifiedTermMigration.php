<?php

/**
 * @file
 * Contains NabModifiedTermMigration.
 */

/**
 * Migration class that modifies terms as they're being migrated.
 */
class NabModifiedTermMigration extends DrupalTerm7Migration {

  /**
   * Full system path to the CSV file.
   */
  protected $csvFilepath;

  /**
   * Parsed mappings from CSV file.
   */
  protected $csvMappings;

  /**
   * Whether or not the CSV uses a header row.
   */
  protected $csvHeaderRow = FALSE;

  /**
   * Overrides DrupalTerm7Migration::__construct().
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    if (isset($arguments['mappings_csv'])) {
      $this->csvFilepath = DRUPAL_ROOT . '/' . drupal_get_path('module', 'nab_migration') . '/data/' . $arguments['mappings_csv'];
    }
  }

  /**
   * Implements Migration::preImport().
   */
  public function preImport() {
    parent::preImport();
    $this->loadCsv();
  }

  /**
   * Overrides DrupalTerm7Migration::prepareRow().
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    $this->modifyTerm($row);
    return TRUE;
  }

  /**
   * Modifies constructed term from ::prepareRow().
   * 
   * This is meant to be overridden in child classes.
   */
  protected function modifyTerm($term) { }

  /**
   * Loads the source and target data from the specified CSV.
   */
  protected function loadCsv() {
    $this->csvMappings = array();

    if (!file_exists($this->csvFilepath)) {
      throw new MigrateException(t('The CSV file does not exist: !filepath', array('!filepath' => $this->csvFilepath)));
    }

    $csv = file($this->csvFilepath);
    $rows = array_map('str_getcsv', $csv);
    if ($this->csvHeaderRow) {
      array_shift($rows);
    }

    foreach ($rows as $row) {
      $source_name = $this->trim(strtolower(array_shift($row)));
      $this->csvMappings[$source_name] = $row;
    }
  }

  /**
   * Apparently PHP's trim() does not handle some unicode whitespace.
   */
  protected function trim($string) {
    return preg_replace('/^\p{Z}+|\p{Z}+$/u', '', $string);
  }
}

