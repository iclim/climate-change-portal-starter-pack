<?php

/**
 * @file
 * Contains NabModifiedTermMigration.
 */

/**
 * Migration class that imports (only) new nodes from CSV.
 */
class NabNewNodeMigration extends Migration {

  /**
   * Full system path to the CSV file.
   */
  protected $csvFilepath;

  /**
   * Target content type.
   */
  protected $destinationType;

  /**
   * List of existing nodes in the destination.
   */
  protected $existingNodeList;

  /**
   * Overrides DrupalTerm7Migration::__construct().
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->csvFilepath = DRUPAL_ROOT . '/' . drupal_get_path('module', 'nab_migration') . '/data/' . $arguments['csv'];
    $this->destinationType = $arguments['destination_type'];

    $csvFields = array(
      array('name', 'Name of the item.'),
      array('old_name', 'Name of the item in old NAB system.'),
    );

    $this->source = new MigrateSourceCSV($this->csvFilepath, $csvFields, array('header_rows' => 1));
    $this->destination = new MigrateDestinationNode($this->destinationType);
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'name' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );

    $this->addFieldMapping('title', 'name_formatted');
  }

  /**
   * Implements Migration::preImport().
   */
  public function preImport() {
    parent::preImport();
    $this->loadNodeList();
  }

  /**
   * Overrides DrupalTerm7Migration::prepareRow().
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    return $this->prepareNode($row);
  }

  /**
   * Modifies constructed node from ::prepareRow().
   *
   * This is meant to be overridden in child classes. Return FALSE to skip.
   */
  protected function prepareNode($row) {
    $row->name_formatted = $row->name;
    return TRUE;
  }

  /**
   * Gets all existing nodes by title.
   */
  protected function loadNodeList() {
    $list = db_select('node', 'n')
      ->condition('type', $this->destinationType)
      ->fields('n', array('nid', 'title'))
      ->orderBy('created', 'DESC')
      ->execute()
      ->fetchAllKeyed(0, 1);
    $this->existingNodeList = array_map('strtolower', $list);
  }
}

