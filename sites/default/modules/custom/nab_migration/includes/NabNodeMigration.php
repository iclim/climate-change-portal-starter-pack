<?php
/**
 * @file
 * Contains NabNodeMigration.
 */

/**
 * Class NabNodeMigration
 *
 * Base class for node migrations containing shared functionality.
 */
class NabNodeMigration extends DrupalNode7Migration {

  /**
   * Map of country codes to country names from Location module.
   */
  protected $countries;

  /**
   * Overrides Migration::handleSourceMigration();
   */
  public function handleSourceMigration($source_migrations, $source_keys, $default = NULL, $migration = NULL) {
    if ($source_migrations == 'Topics') {
      $migrations = array(
        'ThemesAndSectors',
        'ThemesAndSectors2',
        'ThemesAndSectors3',
        'SpecificSectors',
        'SpecificSectors2',
        'ContentTopics',
        'ContentTopics2',
        'Hazards',
      );
      return $this->handleMultipleMigrations($migrations, $source_keys, $default, $migration);
    }
    return parent::handleSourceMigration($source_migrations, $source_keys, $default, $migration);
  }

  /**
   * Helper for aggregating lookups on multiple migrations.
   */
  public function handleMultipleMigrations($migrations, $source_keys, $default, $migration) {
    $results = array();

    foreach ($migrations as $migration) {
      $result = parent::handleSourceMigration($migration, $source_keys, $default, $migration);
      $result = (array) $result;
      $results = array_merge($results, $result);
    }

    return $this->uniquify($results);
  }

  /**
   * Aggregates multi-value text fields into single values for when the
   * destination field is limited to 1.
   */
  public function multiValueToSingle($value) {
    if (is_array($value)) {
      $value = implode(PHP_EOL . PHP_EOL, $value);
    }
    return $value;
  }

  /**
   * Loads source data from Location module.
   *
   * Locations *should* be a separate Migration, but the Get Locations module
   * architecture makes it very difficult to do that.
   */
  public function loadLocations($lids) {
    // Query for country list and statically cache.
    if (!isset($this->countries)) {
      $this->countries = Database::getConnection('default', $this->sourceConnection)
        ->select('location_country', 'lc')
        ->fields('lc', array('code', 'name'))
        ->execute()
        ->fetchAllKeyed(0, 1);
    }
    // Query for location data based on LID field values.
    $results = Database::getConnection('default', $this->sourceConnection)
      ->select('location', 'l')
      ->fields('l', array('lid', 'name', 'country', 'latitude', 'longitude'))
      ->condition('lid', $lids, 'IN')
      ->execute()
      ->fetchAll();
    return array_map(function ($result) { return (array) $result; }, $results);
  }

  /**
   * Mapping callback to add all parent terms for topic reference fields.
   */
  public function addParentTopics($values) {
    $tids = array();
    foreach ($values as $tid) {
      foreach (taxonomy_get_parents_all($tid) as $found_term) {
        $tids[] = $found_term->tid;
      }
    }
    return $this->uniquify($tids);
  }

  /**
   * Cleans and dedupes and array of IDs.
   */
  public function uniquify($ids) {
    return array_values(array_unique(array_filter($ids)));
  }
}