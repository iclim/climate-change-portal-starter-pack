<?php

/**
 * @file
 * Contains NabStaticTermMigration.
 */

/**
 * Migration class that can map old term names to new term names.
 *
 * Some migrations get broken up into multiple classes because Migrate works on
 * the assumption of 1:1 mappings from source to destination. The CSV data we're
 * working with is often 1:2 and 1:3. The recommended way to handle this is to
 * do separate migrations. We use the "migration_key" setting to determine which
 * item (n) of the 1:n mappings to migrate in each class.
 *
 * @see https://www.drupal.org/node/2250057
 */
class NabStaticTermMigration extends DrupalTerm7Migration {

  /**
   * Flat list of lowercased term names from the dest database, keyed by TID.
   */
  protected $destTermNames;

  /**
   * Hierarchy of terms from the dest database, keyed by lowercase term name.
   */
  protected $destTermTree;

  /**
   * Map of old term names (in source) to TIDs (in destination).
   */
  protected $sourceNamesToDestTids;

  /**
   * Full system path to the CSV file.
   */
  protected $csvFilepath;

  /**
   * Map of source names to target names from CSV.
   */
  protected $csvMappings;

  /**
   * Whether or not the CSV uses a header row.
   */
  protected $csvHeaderRow = TRUE;

  /**
   * For sets of migrations, the sequence number of this migration.
   */
  protected $migration_key = 0;

  /**
   * Whether or not new terms should be created when there is no static mapping.
   */
  protected $createNew = FALSE;

  /**
   * Overrides DrupalTerm7Migration::__construct().
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    if (isset($arguments['mappings_csv'])) {
      $this->csvFilepath = DRUPAL_ROOT . '/' . drupal_get_path('module', 'nab_migration') . '/data/' . $arguments['mappings_csv'];
    }

    if (isset($arguments['migration_key'])) {
      $this->migration_key = $arguments['migration_key'];
    }
  }

  /**
   * Implements Migration::preImport().
   */
  public function preImport() {
    parent::preImport();
    $this->loadTerms();
    $this->loadCsv();
    $this->mapCsvToDestinationTerms();
  }

  /**
   * Overrides DrupalTerm7Migration::prepareRow().
   *
   * This is where the mapping is done. We always skip the row by returning
   * FALSE (meaning we never create new terms). But if our lookup finds a
   * matching term then we create a record in the map table.
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    if ($tid = $this->findTidForRow($row)) {
      $this->map->saveIDMapping($row, array($tid), MigrateMap::STATUS_IGNORED, MigrateMap::ROLLBACK_PRESERVE);
      $this->rollbackAction = MigrateMap::ROLLBACK_PRESERVE;
    }
    elseif ($this->createNew) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Implements Migration::createStub().
   */
  public function createStub($migration, $source_key) {
    // We don't allow stubs here since that would defeat the purpose.
    return FALSE;
  }

  /**
   * Finds a corresponding destination TID for the row being imported.
   *
   * This is meant to be overridden in subclasses that need to do clever things.
   */
  protected function findTidForRow($row) {
    $name = strtolower($row->name);

    if (isset($this->sourceNamesToDestTids[$name][$this->migration_key])) {
      return $this->sourceNamesToDestTids[$name][$this->migration_key];
    }

    return FALSE;
  }

  /**
   * Finds corresponding destination TIDs for target string from the CSV.
   *
   * This is meant to be overridden in subclasses that need to do clever things.
   */
  protected function mapCsvTarget($target) {
    $name = strtolower($target);
    $tids = array_keys($this->destTermNames, $name);
    if (empty($tids)) {
      $this->logMissingTarget($target);
    }
    return $tids;
  }

  /**
   * Associates target term name strings from CSV with destination TIDs.
   */
  protected function mapCsvToDestinationTerms() {
    $this->sourceNamesToDestTids = array();

    foreach ($this->csvMappings as $source => $target) {
      // Some CSV items are meant to be skipped and specify an empty target.
      if (empty($target)) {
        continue;
      }

      if ($tids = $this->mapCsvTarget($target)) {
        $key = strtolower($source);
        $this->sourceNamesToDestTids[$key] = $tids;
      }
    }
  }

  /**
   * Logs a message when a target name from the CSV does not match anything.
   */
  protected function logMissingTarget($target) {
    $message = t('No matching term for "@name" in the @vocab vocabulary (@file).',
      array('@name' => $target, '@file' => basename($this->csvFilepath), '@vocab' => $this->destinationVocabulary));
    $this->queueMessage($message);
    if (!$this->createNew) {
      self::displayMessage($message);
    }
  }

  /**
   * Loads the source and target data from the specified CSV.
   */
  protected function loadCsv() {
    $this->csvMappings = array();

    if (!file_exists($this->csvFilepath)) {
      throw new MigrateException(t('The CSV file does not exist: !filepath', array('!filepath' => $this->csvFilepath)));
    }

    $csv = file($this->csvFilepath);
    $rows = array_map('str_getcsv', $csv);
    if ($this->csvHeaderRow) {
      array_shift($rows);
    }

    foreach ($rows as $row) {
      $source_name = $this->trim($row[0]);
      $target = $this->trim($row[1]);
      $this->csvMappings[$source_name] = $target;
    }
  }

  /**
   * Loads all terms in the destination vocabulary.
   */
  protected function loadTerms() {
    $vocab = taxonomy_vocabulary_machine_name_load($this->destinationVocabulary);
    $this->destTermNames = $this->prepareList(taxonomy_get_tree($vocab->vid));
    $this->destTermTree = $this->prepareTree(taxonomy_get_tree($vocab->vid));
  }

  /**
   * Builds a list of terms from taxonomy_get_tree() keyed by name.
   */
  protected function prepareList($terms) {
    $list = array();
    foreach ($terms as $term) {
      $list[$term->tid] = $this->trim(strtolower($term->name));
    }
    return $list;
  }

  /**
   * Turns the result of taxonomy_get_tree() into an actual hierarchy.
   *
   * The arrays get keyed by lowercased term names for quick lookups.
   */
  protected function prepareTree($terms) {
    $tree = array();

    $tids_to_term = array();
    foreach ($terms as $term) {
      $tids_to_term[$term->tid] = $term;
    }

    // Handle five levels of hierarchy - probably more than we need.
    foreach (array(0, 1, 2, 3, 4) as $depth) {
      foreach ($terms as $term) {
        if ($depth != $term->depth) {
          continue;
        }

        // Use lowercased term names as keys and replace multiple spaces with
        // single space since some terms seem to have multiple spaces, though
        // it won't appear that way when displayed.
        $key = preg_replace('!\s+!', ' ', strtolower($term->name));

        $term->children = array();

        // Add root terms to the root of the tree.
        if ($depth == 0) {
          $tree[$key] = $term;
        }
        // Add non-root terms to their parent.
        else {
          foreach ($term->parents as $parent_tid) {
            $parent_term = $tids_to_term[$parent_tid];
            $parent_term->children[$key] = $term;
          }
        }
      }
    }

    return $tree;
  }

  /**
   * Apparently PHP's trim() does not handle some unicode whitespace.
   */
  protected function trim($string) {
    return preg_replace('/^\p{Z}+|\p{Z}+$/u', '', $string);
  }
}