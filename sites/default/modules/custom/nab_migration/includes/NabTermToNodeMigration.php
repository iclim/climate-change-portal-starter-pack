<?php

/**
 * @file
 * Contains NabTermToNodeMigration.
 */

/**
 * Migration class that turns taxonomy terms into nodes.
 */
class NabTermToNodeMigration extends DrupalMigration {

  /**
   * Source vocabulary name.
   */
  protected $sourceVocabulary;

  /**
   * Target node type.
   */
  protected $destinationType;

  /**
   * Overrides DrupalMigration::__construct().
   */
  public function __construct(array $arguments) {
    $arguments['source_version'] = 7;

    parent::__construct($arguments);

    $this->sourceVocabulary = $arguments['source_vocabulary'];
    $this->destinationType = $arguments['destination_type'];

    $this->sourceFields += $this->version->getSourceFields('taxonomy_term', $this->sourceVocabulary);
    $this->sourceFields['path'] = t('Path alias');

    $this->source = new MigrateSourceSQL($this->query(), $this->sourceFields, NULL, $this->sourceOptions);
    $this->destination = new MigrateDestinationNode($this->destinationType);
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'tid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'Source taxonomy term TID',
          'alias' => 'td',
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );

    $this->addFieldMapping('title', 'name');
    $this->addFieldMapping('path', 'path');
  }

  /**
   * Overrides DrupalMigration::query().
   */
  protected function query() {
    $query = Database::getConnection('default', $this->sourceConnection)
      ->select('taxonomy_term_data', 'td')
      ->fields('td', array('tid', 'name', 'description', 'weight', 'format'))
      ->distinct();
    $query->innerJoin('taxonomy_vocabulary', 'v', 'td.vid=v.vid');
    $query->condition('v.machine_name', array($this->sourceVocabulary), 'IN');
    return $query;
  }

  /**
   * Overrides DrupalMigration::prepareRow().
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Load up all term fields etc.
    $this->version->getSourceValues($row, $row->tid);

    // Get the alias for the term.
    if ($this->moduleExists('path')) {
      $path = $this->version->getPath('taxonomy/term/' . $row->tid);
      if ($path) {
        $row->path = $path;
      }
    }
  }

  /**
   * Overrides Migration::handleSourceMigration();
   */
  public function handleSourceMigration($source_migrations, $source_keys, $default = NULL, $migration = NULL) {
    if ($source_migrations == 'Topics') {
      $results = array();

      $migrations = array(
        'ThemesAndSectors',
        'ThemesAndSectors2',
        'ThemesAndSectors3',
        'SpecificSectors',
        'SpecificSectors2',
        'ContentTopics',
        'ContentTopics2',
        'Hazards',
      );

      foreach ($migrations as $migration) {
        $result = parent::handleSourceMigration($migration, $source_keys, $default, $migration);
        $result = (array) $result;
        $results = array_merge($results, $result);
      }

      return $this->uniquify($results);
    }

    return parent::handleSourceMigration($source_migrations, $source_keys, $default, $migration);
  }

  /**
   * Cleans and dedupes and array of IDs.
   */
  public function uniquify($ids) {
    return array_values(array_unique(array_filter($ids)));
  }
}