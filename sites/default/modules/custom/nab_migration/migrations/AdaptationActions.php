<?php
/**
 * @file
 * Contains NabAdaptationActionsMigration.
 */

class NabAdaptationActionsMigration extends NabNodeMigration {

  public static $migrationArguments = array(
    'source_type' => 'adaptation_actions',
    'destination_type' => 'adaptation_action',
    'user_migration' => 'Users',
    'dependencies' => array(
      'ThemesAndSectors',
      'MajorImpacts',
      'SectorImpacts',
      'Users',
    ),
  );

  public function __construct(array $arguments) {
    parent::__construct($arguments);

    // Basic fields

    $this->addFieldMapping('field_text', 'field_notes');

    // Reference fields

    // Looks like the terms referenced by field_sectors_and_themes don't exist
    // in the source taxonomy_term_data table.
    $this->addFieldMapping('field_topics', 'field_sectors_and_themes')->sourceMigration('Topics')->callbacks(array($this, 'addParentTopics'));
    $this->addFieldMapping('field_topics:source_type')->defaultValue('tid');

    $this->addFieldMapping('field_major_impact', 'field_major_impact')->sourceMigration('MajorImpacts');
    $this->addFieldMapping('field_major_impact:source_type')->defaultValue('tid');

    $this->addFieldMapping('field_sector_impact', 'field_sector_impact')->sourceMigration('SectorImpacts');
    $this->addFieldMapping('field_sector_impact:source_type')->defaultValue('tid');
  }
}