<?php
/**
 * @file
 * Contains NabArticlesMigration.
 */

class NabArticlesMigration extends NabNodeMigration {

  public static $migrationArguments = array(
    'source_type' => 'article',
    'destination_type' => 'news',
    'user_migration' => 'Users',
    'dependencies' => array(
      'ThemesAndSectors',
      'SpecificSectors',
      'ClimateChangeTerminology',
      'Files',
      'Links',
      'Users',
    ),
  );

  /**
   * Overrides DrupalNode7Migration::__construct().
   */
  public function __construct($arguments) {
    parent::__construct($arguments);

    // Basic fields

    $this->addFieldMapping('field_text', 'body');
    $this->addFieldMapping('field_text:format', 'body:format');

    // Reference fields

    //$this->addFieldMapping('field_related_links_entity', 'field_links')->sourceMigration('Links');
    $this->addFieldMapping('field_document_files', 'field_files')->sourceMigration('Files');
    $this->addFieldMapping('field_document_files:file_class')->defaultValue('MigrateFileFid');

    $this->addFieldMapping('field_topics', 'topics')->sourceMigration('Topics')->callbacks(array($this, 'addParentTopics'));
    $this->addFieldMapping('field_topics:source_type')->defaultValue('tid');

    $this->addFieldMapping('field_tags', 'field_climate_change_tags')->sourceMigration('ClimateChangeTerminology');
    $this->addFieldMapping('field_tags:source_type')->defaultValue('tid');

    $this->addFieldMapping('field_image', 'field_image')->sourceMigration('Files');
    $this->addFieldMapping('field_image:file_class')->defaultValue('MigrateFileFid');
  }

  /**
   * Overrides DrupalNode7Migration::prepareRow().
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Aggregate values from multiple reference fields.
    $row->topics = array();
    $topic_fields = array(
      'field_tags',
      'field_specific_sectors_focus',
    );
    foreach ($topic_fields as $field_name) {
      if (!empty($row->{$field_name})) {
        $row->topics = array_merge($row->topics, $row->{$field_name});
      }
    }
    $row->topics = $this->uniquify($row->topics);

    return TRUE;
  }
}