<?php

/**
 * @file
 * Contains NabClimateChangeTerminologyMigration.
 */

/**
 * Class NabClimateChangeTerminologyMigration
 */
class NabClimateChangeTerminologyMigration extends DrupalTerm7Migration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'climate_change_terminology',
    'destination_vocabulary' => 'tags',
  );
}