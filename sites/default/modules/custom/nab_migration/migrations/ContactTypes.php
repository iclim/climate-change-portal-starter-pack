<?php

/**
 * @file
 * Contains NabContactTypesMigration.
 */

/**
 * Class NabContactTypesMigration
 *
 * CSV corrections:
 * - Created new column for accurate term names.
 * - Fixed typos in source term names.
 */
class NabContactTypesMigration extends NabStaticTermMigration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'contact_type',
    'destination_vocabulary' => 'contact_types',
    'mappings_csv' => 'contact_types.csv',
  );
}