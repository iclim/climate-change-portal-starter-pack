<?php
/**
 * @file
 * Contains NabContactsMigration.
 */

class NabContactsMigration extends NabTermToNodeMigration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'contact',
    'destination_type' => 'contact',
    'dependencies' => array(
      'ThemesAndSectors',
      'SpecificSectors',
      'ContactTypes',
      'Countries',
      'Organisations',
      'Links',
    )
  );

  /**
   * Overrides NabTermToNodeMigration::__construct().
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    // Basic fields

    $this->addFieldMapping('title', 'full_name');
    $this->addFieldMapping('field_first_name', 'name');
    $this->addFieldMapping('field_last_name', 'field_contact_surname');
    $this->addFieldMapping('field_email', 'field_contact_email');
    $this->addFieldMapping('field_phone', 'field_contact_phone');
    $this->addFieldMapping('field_fax', 'field_contact_fax');
    $this->addFieldMapping('field_other_information', 'description');
    $this->addFieldMapping('field_street_address', 'field_mailing_address');

    $this->addFieldMapping('field_gender', 'field_contact_sexe')->callbacks(array($this, 'filterGender'));

    // Reference fields

    $this->addFieldMapping('field_contact_type', 'field_contact_type')->sourceMigration('ContactTypes');
    $this->addFieldMapping('field_contact_type:source_type')->defaultValue('tid');

    $this->addFieldMapping('field_contact_country', 'field_contact_country')->sourceMigration('Countries');

    $this->addFieldMapping('field_tags', 'field_contact_theme')->sourceMigration('Topics');
    $this->addFieldMapping('field_tags:source_type')->defaultValue('tid');

    $this->addFieldMapping('field_organisation_entity', 'field_contact_organisation')->sourceMigration('Organisations');

    $this->addFieldMapping('field_related_links_entity', 'field_contact_web_links')->sourceMigration('Links');
  }

  /**
   * Overrides DrupalTerm7Migration::prepareRow().
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Format node titles as full name instead of only first name.
    $row->full_name = $row->name;
    if (!empty($row->field_contact_surname)) {
      $row->full_name .= ' ' . reset($row->field_contact_surname);
    }

    return TRUE;
  }

  /**
   * Field mapping callback.
   */
  public function filterGender($value) {
    $value = is_array($value) ? reset($value) : $value;
    $value = strtolower($value);
    if ($value == 'male' || $value == 'female') {
      return $value;
    }
    return FALSE;
  }
}