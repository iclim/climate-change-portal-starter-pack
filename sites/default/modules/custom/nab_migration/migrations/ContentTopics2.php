<?php

/**
 * @file
 * Contains NabContentTopics2Migration.
 */

class NabContentTopics2Migration extends NabContentTopicsMigration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'content_topics',
    'destination_vocabulary' => 'topics',
    'mappings_csv' => 'content_topics.csv',
    'migration_key' => 1,
  );
}