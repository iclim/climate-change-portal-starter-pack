<?php
/**
 * @file
 * Contains NabCountriesMigration.
 */

class NabCountriesMigration extends NabTermToNodeMigration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'countries',
    'destination_type' => 'country',
  );

  /**
   * Overrides NabTermToNodeMigration::__construct().
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->addFieldMapping('field_flag_image', 'field_country_flag')->sourceMigration('Files');
    $this->addFieldMapping('field_flag_image:file_class')->defaultValue('MigrateFileFid');
  }
}