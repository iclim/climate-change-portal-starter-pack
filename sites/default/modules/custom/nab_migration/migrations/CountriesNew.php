<?php
/**
 * @file
 * Contains NabCountriesNewMigration.
 */

class NabCountriesNewMigration extends NabNewNodeMigration {

  public static $migrationArguments = array(
    'csv' => 'new_countries.csv',
    'destination_type' => 'country',
    'dependencies' => array(
      'Countries',
    )
  );

  /**
   * Overrides NabNewNodeMigration::modifyNode().
   */
  protected function prepareNode($row) {

    // Look for an existing node from the Countries migration and use instead.
    $country_name = strtolower($row->old_name);
    if ($nid = array_search($country_name, $this->existingNodeList)) {
      $this->map->saveIDMapping($row, array($nid), MigrateMap::STATUS_IGNORED, MigrateMap::ROLLBACK_PRESERVE);
      $this->rollbackAction = MigrateMap::ROLLBACK_PRESERVE;
      return FALSE;
    }

    // We need to clean up the country names here as they're all uppercased.
    // If there's something inside parens we don't want to capitalize that.
    $country_name = $row->name;
    $parts = explode(' (', $country_name);
    if (count($parts) > 1) {
      $row->name_formatted = ucwords(strtolower($parts[0])) . ' (' . $parts[1];
    }
    else {
      $row->name_formatted = ucwords(strtolower($country_name));
    }
    
    return TRUE;
  }
}