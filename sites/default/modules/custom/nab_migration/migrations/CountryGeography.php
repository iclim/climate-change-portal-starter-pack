<?php
/**
 * @file
 * Contains NabCountryGeographyMigration.
 */

/**
 * Class NabCountryGeographyMigration
 *
 * Adds geography data to existing Country nodes. Currently does a simple name
 * match but could be improved to use ISO codes.
 *
 * Geography data from Natural Earth.
 * @see http://www.naturalearthdata.com/downloads/50m-cultural-vectors/
 */
class NabCountryGeographyMigration extends Migration {

  protected $countryList;

  protected $jsonFile;

  public static $migrationArguments = array(
    'file' => 'country_geography.json',
    'dependencies' => array(
      'Countries',
      'CountriesNew',
    ),
  );

  /**
   * Overrides Migration::__construct().
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    // We only want to add to existing nodes, never create or replace.
    $this->systemOfRecord = Migration::DESTINATION;

    $this->jsonFile = DRUPAL_ROOT . '/' . drupal_get_path('module', 'nab_migration') . '/data/' . $arguments['file'];

    $this->source = new MigrateSourceList(
      new MigrateGeoJSONList($this->jsonFile),
      new MigrateGeoJSONItem($this->jsonFile),
      array()
    );
    $this->destination = new MigrateDestinationNode('country');
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'name' => array(
          'type' => 'varchar',
          'length' => 255,
          'description' => 'Country name',
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );

    $this->addFieldMapping('nid', 'country_nid');

    $this->addFieldMapping('field_location_latlon', 'coordinates');
    $this->addFieldMapping('field_location_latlon:input_format')->defaultValue(GEOFIELD_INPUT_LAT_LON);
  }

  /**
   * Overrides Migration::prepareRow().
   */
  public function prepareRow($row) {
    $country_name = $row->properties['name_long'];

    if ($matching_nid = $this->matchCountry($country_name)) {
      $row->country_nid = $matching_nid;
      $row->coordinates = 'POINT (' . $row->geometry['coordinates'][0] . ' ' . $row->geometry['coordinates'][1] . ')';
    }
    else {
      $message = t('No matching country node found for "@name".', array('@name' => $country_name));
      $this->queueMessage($message, MigrationBase::MESSAGE_WARNING);
      self::displayMessage($message, MigrationBase::MESSAGE_WARNING);
      return FALSE;
    }
  }

  /**
   * Matches a name from GeoJSON with a Country node NID.
   */
  public function matchCountry($country_name) {
    $nid = FALSE;

    // Remove anything in parentheses.
    $parts = explode(' (', $country_name);
    $country_name = strtolower($parts[0]);

    $countries = $this->getCountries();

    if (isset($countries[$country_name])) {
      $nid = $countries[$country_name];
    }

    return $nid;
  }

  /**
   * Loads a list of Country node NIDs keyed by lowercase name.
   */
  public function getCountries() {
    if (!isset($this->countryList)) {
      $results = db_select('node', 'n')
        ->fields('n', array('title', 'nid'))
        ->condition('type', 'country')
        ->execute()
        ->fetchAllKeyed(0, 1);
      foreach ($results as $country_name => $nid) {
        // Remove anything wrapped in parentheses.
        $parts = explode(' (', $country_name);
        $key = strtolower($parts[0]);
        $this->countryList[$key] = $nid;
      }
    }
    return $this->countryList;
  }
}