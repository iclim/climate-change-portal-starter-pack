<?php
/**
 * @file
 * Contains NabDocumentTypesMigration.
 */

/**
 * Class NabDocumentTypesMigration
 *
 * CSV corrections:
 *  - Duplicated original spreadsheet
 *  - Collapsed unused rows and columns
 *  - Fixed name formatting
 *  - Changed multiple term delimiter to semicolon
 */
class NabDocumentTypesMigration extends NabStaticTermMigration {

  protected $createNew = TRUE;

  public static $migrationArguments = array(
    'source_vocabulary' => 'document_type',
    'destination_vocabulary' => 'document_resource_types',
    'mappings_csv' => 'document_types.csv',
    'migration_key' => 0,
    'dependencies' => array(
      'DocumentTypes2',
    ),
  );

  /**
   * Overrides NabStaticTermMigration::prepareRow().
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // The only way the parent class has returned TRUE is if we're supposed to
    // be creating a new term. In this migration, however, we only want to
    // create new terms for terms listed in the CSV (some have been excluded).
    $name = strtolower($row->name);
    $mappings = array_change_key_case($this->csvMappings);
    if (strpos($mappings[$name], '(could add extra terms)') !== FALSE) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Overrides NabStaticTermMigration::mapCsvTarget().
   */
  public function mapCsvTarget($target) {
    $tids = array();

    // Some terms get mapped to multiple destination terms.
    $names = array_map('trim', explode(';', strtolower($target)));
    foreach ($names as $name) {
      $matches = parent::mapCsvTarget($name);
      $tids = array_merge($tids, $matches);
    }

    return $tids;
  }
}