<?php
/**
 * @file
 * Contains NabDocumentTypes2Migration.
 */

class NabDocumentTypes2Migration extends NabDocumentTypesMigration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'document_type',
    'destination_vocabulary' => 'document_resource_types',
    'mappings_csv' => 'document_types.csv',
    'migration_key' => 1,
  );

  /**
   * Overrides NabStaticTermMigration::prepareRow().
   */
  public function prepareRow($row) {
    parent::prepareRow($row);
    // For this second-level migration we never want to create new terms.
    return FALSE;
  }
}