<?php

/**
 * @file
 * Contains NabDocumentsMigration.
 */

class NabDocumentsMigration extends NabNodeMigration {

  public static $migrationArguments = array(
    'source_type' => 'documents',
    'destination_type' => 'document',
    'user_migration' => 'Users',
    'dependencies' => array(
      'ClimateChangeTerminology',
      'NationalFrameworks',
      'SubjectStrands',
      'GeographicFocus',
      'Organisations',
      'DocumentTypes',
      'ContentTopics',
      'ThemesAndSectors',
      'UtilizedIn',
      'Languages',
      'Files',
      'Users',
    ),
  );

  /**
   * Overrides DrupalNode7Migration::__construct().
   */
  public function __construct($arguments) {
    parent::__construct($arguments);

    // Basic fields

    $this->addFieldMapping('field_document_link', 'field_links');
    $this->addFieldMapping('field_author', 'field_author')->callbacks(array($this, 'semicolons'));
    $this->addFieldMapping('field_publication_year', 'field_date_published')->callbacks(array($this, 'dateToYear'));

    $this->addFieldMapping('field_the_description', 'body');
    $this->addFieldMapping('field_the_description:format', 'body:format');

    $this->addFieldMapping('field_publisher', 'field_organisation')->sourceMigration('Organisations')->callbacks(array($this, 'nidsToString'));

    $this->addFieldMapping('field_target_audience', 'field_vanuatu_target_group_')->callbacks(array($this, 'tidsToString'));
    $this->addFieldMapping('field_target_audience:format')->defaultValue('filtered_html');

    // Reference fields

    $this->addFieldMapping('field_national_objective', 'field_national_objectives')->sourceMigration('NationalFrameworks');
    $this->addFieldMapping('field_national_objective:source_type')->defaultValue('tid');

    $this->addFieldMapping('field_tags', 'field_theme')->sourceMigration('ClimateChangeTerminology');
    $this->addFieldMapping('field_tags:source_type')->defaultValue('tid');

    $this->addFieldMapping('field_cover_image', 'field_image')->sourceMigration('Files');
    $this->addFieldMapping('field_cover_image:file_class')->defaultValue('MigrateFileFid');

    $this->addFieldMapping('field_topics', 'topics')->sourceMigration('Topics')->callbacks(array($this, 'addParentTopics'));
    $this->addFieldMapping('field_topics:source_type')->defaultValue('tid');

    $this->addFieldMapping('field_focus_area', 'field_sectors_and_themes')->sourceMigration('ThemesAndSectors4');
    $this->addFieldMapping('field_focus_area:source_type')->defaultValue('tid');

    $this->addFieldMapping('field_document_files', 'field_doc_file')->sourceMigration('Files');
    $this->addFieldMapping('field_document_files:file_class')->defaultValue('MigrateFileFid');
    $this->addFieldMapping('field_document_files:description', 'field_doc_file:description');

    $this->addFieldMapping('field_resource_type', 'field_document_type')->sourceMigration('DocumentTypes');
    $this->addFieldMapping('field_resource_type:source_type')->defaultValue('tid');

    $this->addFieldMapping('field_utilised_in', 'field_utilized_in1')->sourceMigration('UtilizedIn');
    $this->addFieldMapping('field_utilised_in:source_type')->defaultValue('tid');

    $this->addFieldMapping('field_regional_focus', 'field_geographic_focus1')->sourceMigration('GeographicFocus');
    $this->addFieldMapping('field_regional_focus:source_type')->defaultValue('tid');

    $this->addFieldMapping('field_language', 'field_language1')->sourceMigration('Languages');
    $this->addFieldMapping('field_language:source_type')->defaultValue('tid');

    $this->addFieldMapping('field_subject_strand', 'field_syllabus_subject_strand_')->sourceMigration('SubjectStrands');
    $this->addFieldMapping('field_subject_strand:source_type')->defaultValue('tid');

    // Location module field translated into a reference field in prepareRow().
    $this->addFieldMapping('field_country', 'countries');
  }

  /**
   * Overrides DrupalNode7Migration::prepareRow().
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Aggregate values from multiple fields that reference topics.
    $row->topics = array();
    $topic_fields = array(
      'field_sectors_and_themes',
      'field_content_topics',
    );
    foreach ($topic_fields as $field_name) {
      if (!empty($row->{$field_name})) {
        $row->topics = array_merge($row->topics, $row->{$field_name});
      }
    }
    $row->topics = $this->uniquify($row->topics);
  }

  /**
   * Overrides NabNodeMigration::handleSourceMigration();
   */
  public function handleSourceMigration($source_migrations, $source_keys, $default = NULL, $migration = NULL) {
    if ($source_migrations == 'DocumentTypes') {
      $migrations = array(
        'DocumentTypes',
        'DocumentTypes2'
      );
      return $this->handleMultipleMigrations($migrations, $source_keys, $default, $migration);
    }
    if ($source_migrations == 'UtilizedIn') {
      $migrations = array(
        'UtilizedIn',
        'UtilizedIn2'
      );
      return $this->handleMultipleMigrations($migrations, $source_keys, $default, $migration);
    }
    if ($source_migrations == 'GeographicFocus') {
      $migrations = array(
        'GeographicFocus',
        'GeographicFocus2'
      );
      return $this->handleMultipleMigrations($migrations, $source_keys, $default, $migration);
    }
    if ($source_migrations == 'SubjectStrands') {
      $migrations = array(
        'SubjectStrands',
        'SubjectStrands2'
      );
      return $this->handleMultipleMigrations($migrations, $source_keys, $default, $migration);
    }
    return parent::handleSourceMigration($source_migrations, $source_keys, $default, $migration);
  }

  /**
   * Callback that turns multi-value text fields into delimited single values.
   */
  public function semicolons($value) {
    if (is_array($value)) {
      $value = implode('; ', $value);
    }
    return $value;
  }

  /**
   * Callback to turn ISO datestamps into year integers.
   */
  public function dateToYear($datestamp) {
    $datestamp = is_array($datestamp) ? reset($datestamp) : $datestamp;
    if ($info = date_parse($datestamp)) {
      return $info['year'];
    }
    return NULL;
  }

  /**
   * Callback to turn NIDs into a single string of names.
   */
  public function nidsToString($nids) {
    $nids = !is_array($nids) ? array($nids) : $nids;

    $names = array();
    foreach (node_load_multiple($nids) as $node) {
      $names[] = trim($node->title);
    }

    return implode('; ', $names);
  }

  /**
   * Callback to turn TIDs into a string of names.
   */
  public function tidsToString($tids) {
    $tids = !is_array($tids) ? array($tids) : $tids;

    $names = array();
    foreach (taxonomy_term_load_multiple($tids) as $term) {
      $names[] = trim($term->name);
    }

    return implode('; ', $names);
  }
}