<?php
/**
 * @file
 * Contains NabDonorsMigration.
 */

/**
 * Class NabDonorsMigration
 *
 * Migrates organisation terms that are referenced as donors into Donor nodes.
 */
class NabDonorsMigration extends NabTermToNodeMigration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'organisations',
    'destination_type' => 'donor',
  );

  /**
   * Overrides NabTermToNodeMigration::query().
   */
  protected function query() {
    $query = parent::query();
    $query->innerJoin('field_data_field_project_donors', 'fd', 'fd.field_project_donors_tid=td.tid');
    $query->condition('v.machine_name', array($this->sourceVocabulary), 'IN');
    return $query;
  }
}

/**
 * Class OriginalNabDonorsMigration
 *
 * This migration was for the actual "donors" vocabulary, which does not appear
 * to be used in the original NAB site.
 */
class OriginalNabDonorsMigration extends NabTermToNodeMigration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'donors',
    'destination_type' => 'donor',
  );
}