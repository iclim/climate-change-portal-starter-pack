<?php
/**
 * @file
 * Contains NabEventTypesMigration.
 */

class NabEventTypesMigration extends NabStaticTermMigration {

  protected $createNew = TRUE;

  public static $migrationArguments = array(
    'source_vocabulary' => 'event_type',
    'destination_vocabulary' => 'event_types',
    'mappings_csv' => 'event_types.csv',
  );
}