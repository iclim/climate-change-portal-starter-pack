<?php
/**
 * @file
 * Contains NabEventsMigration.
 */

class NabEventsMigration extends NabNodeMigration {

  public static $migrationArguments = array(
    'source_type' => 'event',
    'destination_type' => 'event',
    'user_migration' => 'Users',
    'dependencies' => array(
      'EventTypes',
      'Organisations',
      'Files',
      'Users',
    ),
  );

  public function __construct(array $arguments) {
    parent::__construct($arguments);

    // Basic fields

    $this->addFieldMapping('field_start_end_date', 'field_event_date');
    $this->addFieldMapping('field_start_end_date:value2', 'field_event_date:value2');

    $this->addFieldMapping('field_the_description', 'body');
    $this->addFieldMapping('field_the_description:format', 'body:format');

    // Reference fields

    $this->addFieldMapping('field_event_type', 'field_event_type')->sourceMigration('EventTypes');

    $this->addFieldMapping('field_organisation_entity', 'field_event_organisation')->sourceMigration('Organisations');

    $this->addFieldMapping('field_related_documents_entity', 'field_related_documents_entity')->sourceMigration('Files');
    $this->addFieldMapping('field_related_documents_entity:file_class')->defaultValue('MigrateFileFid');
  }
}