<?php
/**
 * @file
 * Contains NabFilesMigration.
 */

/**
 * Class NabFilesMigration
 *
 * This migration is set up with the assumption that the old files directory
 * will be manually copied into place inside the new files directory. Files do
 * not get copied automatically. The "destination_dir" setting should match the
 * actual location where the files have been placed.
 */
class NabFilesMigration extends DrupalFile7Migration {

  public static $migrationArguments = array(
    'file_class' => 'MigrateFileUriAsIs',
    'destination_dir' => 'public://nab/',
    'user_migration' => 'Users',
    'dependencies' => array(
      'Users',
    ),
  );

  /**
   * Overrides DrupalFile7Migration::prepareRow().
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // DrupalFile7Migration appears to hard-code as MigrateFileUri regardless
    // of the file_class specified, so we force it here.
    $this->destination->setFileClass(self::$migrationArguments['file_class']);
  }

  /**
   * Overrides DrupalFile7Migration::fixUri().
   */
  protected function fixUri($uri) {
    $result = str_replace('public://', self::$migrationArguments['destination_dir'], $uri);
    return $result;
  }
}