<?php
/**
 * @file
 * Contains NabGeographicFocusMigration.
 */

class NabGeographicFocusMigration extends NabStaticTermMigration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'geographic_focus',
    'destination_vocabulary' => 'educational_regional_focus',
    'mappings_csv' => 'geographic_focus.csv',
    'migration_key' => 0,
    'dependencies' => array(
      'GeographicFocus2'
    ),
  );

  /**
   * Overrides NabStaticTermMigration::prepareRow().
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // This is the only term we allow to be created as new.
    if (strtolower($row->name) == 'vanuatu') {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Overrides NabStaticTermMigration::mapCsvTarget().
   */
  public function mapCsvTarget($target) {
    $tids = array();

    // Some terms get mapped to multiple destination terms.
    $names = array_map('trim', explode(',', strtolower($target)));
    foreach ($names as $name) {
      $matches = parent::mapCsvTarget($name);
      $tids = array_merge($tids, $matches);
    }

    return $tids;
  }
}