<?php
/**
 * @file
 * Contains NabGeographicFocus2Migration.
 */

class NabGeographicFocus2Migration extends NabGeographicFocusMigration {

  protected $createNew = TRUE;

  public static $migrationArguments = array(
    'source_vocabulary' => 'geographic_focus',
    'destination_vocabulary' => 'educational_regional_focus',
    'mappings_csv' => 'geographic_focus.csv',
    'migration_key' => 1,
  );
}