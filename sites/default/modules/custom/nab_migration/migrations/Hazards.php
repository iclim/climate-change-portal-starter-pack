<?php

/**
 * @file
 * Contains NabHazardsMigration.
 */

/**
 * Class NabHazardsMigration
 *
 * CSV corrections:
 *  - Em/en dashes replaced with hyphens
 *  - Typos
 */
class NabHazardsMigration extends NabStaticTermMigration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'hazards',
    'destination_vocabulary' => 'topics',
    'mappings_csv' => 'hazards.csv',
  );

  /**
   * Overrides NabStaticTermMigration::mapCsvTarget().
   */
  protected function mapCsvTarget($target) {
    $tids = array();

    $terms = $this->destTermTree;

    $names = array_map('trim', explode(';', strtolower($target)));

    foreach ($names as $name) {
      list($root_name, $child_name) = array_map(array($this, 'trim'), explode('-', $name)) + array(NULL, NULL);

      // Single parent term.
      if ($root_name && empty($child_name)) {
        if (isset($terms[$root_name])) {
          $tids[] = $terms[$root_name]->tid;
        }
        else {
          $this->logMissingTarget($name);
        }
      }

      // Parent term plus child term.
      if ($root_name && $child_name) {
        if (isset($terms[$root_name]) && isset($terms[$root_name]->children[$child_name])) {
          $tids[] = $terms[$root_name]->children[$child_name]->tid;
        }
        else {
          $this->logMissingTarget($name);
        }
      }
    }

    return $tids;
  }
}