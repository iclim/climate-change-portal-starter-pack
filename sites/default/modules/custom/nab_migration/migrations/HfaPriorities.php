<?php

/**
 * @file
 * Contains NabHfaPrioritiesMigration.
 */

/**
 * Class NabHfaPrioritiesMigration
 */
class NabHfaPrioritiesMigration extends NabModifiedTermMigration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'hfa_priorities',
    'destination_vocabulary' => 'hfa_priorities',
    'mappings_csv' => 'hfa_priorities.csv',
  );

  /**
   * Overrides NabModifiedTermMigration::modifyTerm().
   */
  protected function modifyTerm($term) {
    $key = strtolower($term->name);
    if (isset($this->csvMappings[$key])) {
      $text = reset($this->csvMappings[$key]);
      $text = str_replace('Tooltip - ', '', $text);
      $term->name = $text;
    }
  }
}