<?php
/**
 * @file
 * Contains NabLanguagesMigration.
 */

class NabLanguagesMigration extends DrupalTerm7Migration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'language',
    'destination_vocabulary' => 'languages',
  );

  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->addFieldMapping('field_iso_code', 'iso_code');
  }

  /**
   * Overrides NabStaticTermMigration::prepareRow().
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    $iso_codes = array(
      'Bislama' => 'bis',
      'English' => 'eng',
      'Fijian' => 'fis',
      'French' => 'fre',
      'Hindi' => 'hin',
      'ikiribati' => 'gil',
      'Samoan' => 'smo',
      'Tongan' => 'ton',
    );
    $row->iso_code = isset($iso_codes[$row->name]) ? $iso_codes[$row->name] : NULL;
  }
}