<?php

/**
 * @file
 * Contains NabLinksMigration.
 */

/**
 * Migration class that turns all link field items in to Link nodes.
 */
class NabLinksMigration extends DrupalMigration {

  /**
   * Target node type.
   */
  protected $destinationType = 'link';

  /**
   * Fields to be migrated.
   */
  protected $linkFields = array(
    'field_contact_web_links',
    'field_links',
    'field_poject_result_links',
    'field_organisation_web_sites',
  );

  /**
   * Overrides DrupalMigration::__construct().
   */
  public function __construct(array $arguments) {
    $arguments['source_version'] = 7;

    parent::__construct($arguments);

    $this->sourceOptions['map_joinable'] = FALSE;
    $this->source = new NabSourceLinks($this->query(), $this->sourceFields, NULL, $this->sourceOptions);
    $this->destination = new MigrateDestinationNode($this->destinationType);
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'hash' => array(
          'type' => 'varchar',
          'length' => 255,
          'description' => 'Row hash',
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );

    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('url', 'url');
    $this->addFieldMapping('field_url', 'url');
  }

  /**
   * Overrides DrupalMigration::query().
   */
  protected function query() {
    $database = Database::getConnection('default', $this->sourceConnection);

    $query = $previous_query = NULL;

    // Union all the link field tables together.
    foreach ($this->linkFields as $i => $field_name) {
      $alias = "table{$i}";

      $query = $database->select("field_data_{$field_name}", $alias)
        ->fields($alias, array("{$field_name}_url", "{$field_name}_title"));

      if ($previous_query) {
        $query->union($previous_query);
      }

      $previous_query = $query;
    }

    return $query;
  }

  /**
   * Overrides Migration::prepareKey().
   */
  public function prepareKey($source_key, $row) {
    // The "hash" field is not actually populated in the source,
    // we set it in prepareRow(). We'll get notices if we don't override this.
    return array('hash' => md5(serialize((array) $row)));
  }

  /**
   * Overrides DrupalMigration::prepareRow().
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // We add the hash as a secondary source ID so that duplicate link URLs can
    // be migrated. If only the url ID is used, duplicates get collapsed into
    // one row the count is wrong and Migrate thinks some rows have not yet
    // been migrated.
    $row->hash = md5(serialize((array) $row));

    $row->url = '';
    $row->title = '';

    foreach ($this->linkFields as $field_name) {
      $url_field = $field_name . '_url';
      $title_field = $field_name . '_title';

      if (isset($row->{$url_field})) {
        $row->url = $row->{$url_field};
        $row->title = $row->{$title_field};
      }
    }

    // Use the URL as the node title when there's no link title.
    if (empty($row->title)) {
      $row->title = $row->url;
    }

    return !empty($row->url);
  }
}

class NabSourceLinks extends MigrateSourceSQL {

  /**
   * Overrides MigrateSourceSQL::computeCount().
   *
   * The count query from MigrateSourceSQL gets messed up because of our crazy
   * unions. This fixes the count.
   */
  public function computeCount() {
    return $this->query->execute()->rowCount();
  }
}