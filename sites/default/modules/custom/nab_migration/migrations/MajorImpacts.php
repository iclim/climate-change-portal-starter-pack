<?php
/**
 * @file
 * Contains NabMajorImpactsMigration.
 */

class NabMajorImpactsMigration extends DrupalTerm7Migration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'major_impact',
    'destination_vocabulary' => 'major_impact',
  );
}