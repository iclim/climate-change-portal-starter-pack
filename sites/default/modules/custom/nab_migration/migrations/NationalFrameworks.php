<?php

/**
 * @file
 * Contains NabNationalFrameworksMigration.
 */

/**
 * Class NabNationalFrameworksMigration
 */
class NabNationalFrameworksMigration extends DrupalTerm7Migration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'national_policy',
    'destination_vocabulary' => 'national_frameworks',
    'dependencies' => array(
      'Countries',
    ),
  );

  /**
   * Overrides DrupalTerm7Migration::__construct().
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->addFieldMapping('field_framework_country', 'field_country')->sourceMigration('Countries');
  }

  /**
   * Implements Migration::createStub().
   */
  public function createStub($migration, $source_key) {
    // Do not allow stubs to be created by this migration when an entity
    // references a term that no longer exists.
    return FALSE;
  }
}