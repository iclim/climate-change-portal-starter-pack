<?php

/**
 * @file
 * Contains NabOrganisationTypesMigration.
 */

class NabOrganisationTypesMigration extends NabStaticTermMigration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'organisation_type',
    'destination_vocabulary' => 'organisational_categories',
    'mappings_csv' => 'organisation_types.csv',
  );
}