<?php
/**
 * @file
 * Contains NabOrganisationsMigration.
 */

class NabOrganisationsMigration extends NabTermToNodeMigration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'organisations',
    'destination_type' => 'organisation',
    'dependencies' => array(
      'OrganisationTypes',
    ),
  );

  /**
   * Overrides NabTermToNodeMigration::__construct().
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    // Basic fields

    $this->addFieldMapping('field_acronym', 'field_organisation_acronym');
    $this->addFieldMapping('field_the_description', 'description');
    $this->addFieldMapping('field_the_description:format')->defaultValue('filtered_html');

    // Reference fields

    $this->addFieldMapping('field_organisation_type', 'field_organisation_type')->sourceMigration('OrganisationTypes');
    $this->addFieldMapping('field_organisation_type:source_type')->defaultValue('tid');

    $this->addFieldMapping('field_image', 'field_organisation_logo')->sourceMigration('Files');
    $this->addFieldMapping('field_image:file_class')->defaultValue('MigrateFileFid');

    $this->addFieldMapping('field_related_links_entity', 'field_organisation_web_sites')->sourceMigration('Links');
  }
}