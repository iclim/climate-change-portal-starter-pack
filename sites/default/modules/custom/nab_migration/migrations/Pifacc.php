<?php

/**
 * @file
 * Contains NabPifaccMigration.
 */

/**
 * Class NabPifaccMigration
 *
 * CSV corrections:
 *  - Merged two source columns
 *  - Removed hyphens from "T-*" source terms
 *  - Replaced "and" with ampersands
 *  - Fixed name formatting
 */
class NabPifaccMigration extends NabStaticTermMigration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'pifacc',
    'destination_vocabulary' => 'pifacc_themes',
    'mappings_csv' => 'pifacc.csv',
  );
}