<?php

/**
 * @file
 * Contains NabProjectFilesMigration.
 */

/**
 * Migration class that turns field_project_files into Document nodes.
 */
class NabProjectFilesMigration extends DrupalMigration {

  public static $migrationArguments = array(
    'destination_type' => 'document',
    'dependencies' => array(
      'Files',
    ),
  );

  /**
   * Source database table.
   */
  protected $sourceTable = 'field_data_field_project_files';

  /**
   * Overrides DrupalMigration::__construct().
   */
  public function __construct(array $arguments) {
    $arguments['source_version'] = 7;

    parent::__construct($arguments);

    $this->source = new MigrateSourceSQL($this->query(), $this->sourceFields, NULL, $this->sourceOptions);
    $this->destination = new MigrateDestinationNode($arguments['destination_type']);
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'fid' => array(
          'type' => 'int',
          'description' => 'File FID',
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );

    $this->addFieldMapping('title', 'description');

    $this->addFieldMapping('field_document_files', 'fid')->sourceMigration('Files');
    $this->addFieldMapping('field_document_files:file_class')->defaultValue('MigrateFileFid');
  }

  /**
   * Overrides DrupalMigration::query().
   */
  protected function query() {
    $database = Database::getConnection('default', $this->sourceConnection);

    $query = $database->select($this->sourceTable, 'source');
    $query->addField('source', 'field_project_files_fid', 'fid');
    $query->addField('source', 'field_project_files_description', 'description');

    $query->join('file_managed', 'files', 'source.field_project_files_fid = files.fid');
    $query->fields('files', array('filename'));

    $query->distinct(TRUE);
    return $query;
  }

  /**
   * Overrides DrupalMigration::prepareRow().
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    if (empty($row->description)) {
      $row->description = $row->filename;
    }

    return !empty($row->fid);
  }
}
