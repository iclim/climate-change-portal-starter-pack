<?php

/**
 * @file
 * Contains NabProjectScopesMigration.
 */

class NabProjectScopesMigration extends NabStaticTermMigration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'project_scope',
    'destination_vocabulary' => 'project_scope',
    'mappings_csv' => 'project_scopes.csv',
  );
}