<?php

/**
 * @file
 * Contains NabProjectStatusesMigration.
 */

class NabProjectStatusesMigration extends NabStaticTermMigration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'status',
    'destination_vocabulary' => 'project_status',
    'mappings_csv' => 'project_statuses.csv',
  );
}