<?php

/**
 * @file
 * Contains NabProjectTypesMigration.
 */

class NabProjectTypesMigration extends NabStaticTermMigration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'project_type',
    'destination_vocabulary' => 'project_type',
    'mappings_csv' => 'project_types.csv',
  );
}