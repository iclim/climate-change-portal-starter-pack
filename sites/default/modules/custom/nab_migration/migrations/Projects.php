<?php

/**
 * @file
 * Contains NabProjectsMigration.
 */

class NabProjectsMigration extends NabNodeMigration {

  public static $migrationArguments = array(
    'source_type' => 'project',
    'destination_type' => 'product',
    'user_migration' => 'Users',
    'dependencies' => array(
      'ProjectTypes',
      'ProjectStatuses',
      'ProjectScopes',
      'ProjectFiles',
      'Countries',
      'Contacts',
      'Donors',
      'Organisations',
      'Files',
      'Users',
      'ThemesAndSectors',
      'RfaPriorities',
      'HfaPriorities',
      'Hazards',
    ),
  );

  /**
   * Overrides DrupalNode7Migration::__construct().
   */
  public function __construct($arguments) {
    parent::__construct($arguments);

    // Basic fields

    $this->addFieldMapping('field_project_name', 'title');
    $this->addFieldMapping('field_short_title', 'field_project_short_name');
    $this->addFieldMapping('field_start_date', 'field_project_dates');
    $this->addFieldMapping('field_end_date', 'field_project_dates:value2');
    $this->addFieldMapping('field_duration', 'field_project_duration');
    $this->addFieldMapping('field_project_total_funding', 'field_project_total_funding');

    $this->addFieldMapping('field_objectives', 'field_project_objectives')->callbacks(array($this, 'multiValueToSingle'));
    $this->addFieldMapping('field_actions', 'field_project_major_actions')->callbacks(array($this, 'multiValueToSingle'));;
    $this->addFieldMapping('field_activities', 'field_project_activities')->callbacks(array($this, 'multiValueToSingle'));;
    $this->addFieldMapping('field_outputs', 'field_project_outputs')->callbacks(array($this, 'multiValueToSingle'));;
    $this->addFieldMapping('field_project_outcomes', 'field_project_outcomes')->callbacks(array($this, 'multiValueToSingle'));;

    $this->addFieldMapping('field_the_description', 'body');
    $this->addFieldMapping('field_the_description:format', 'body:format');
    $this->addFieldMapping('field_the_description:summary', 'body:summary');

    $this->addFieldMapping('field_project_comments', 'field_project_comments');
    $this->addFieldMapping('field_project_comments:format', 'field_project_comments:format');

    // Reference fields

    $this->addFieldMapping('field_project_type', 'field_project_type')->sourceMigration('ProjectTypes');
    $this->addFieldMapping('field_project_type:source_type')->defaultValue('tid');

    $this->addFieldMapping('field_project_status', 'field_project_status')->sourceMigration('ProjectStatuses');
    $this->addFieldMapping('field_project_status:source_type')->defaultValue('tid');

    $this->addFieldMapping('field_project_scope', 'field_project_scope')->sourceMigration('ProjectScopes');
    $this->addFieldMapping('field_project_scope:source_type')->defaultValue('tid');

    $this->addFieldMapping('field_topics', 'field_project_theme')->sourceMigration('Topics')->callbacks(array($this, 'addParentTopics'));
    $this->addFieldMapping('field_topics:source_type')->defaultValue('tid');

    $this->addFieldMapping('field_pifacc_themes', 'field_project_pifacc')->sourceMigration('Pifacc');
    $this->addFieldMapping('field_pifacc_themes:source_type')->defaultValue('tid');

    $this->addFieldMapping('field_tags', 'field_project_glossary_terms')->sourceMigration('ClimateChangeTerminology');
    $this->addFieldMapping('field_tags:source_type')->defaultValue('tid');

    $this->addFieldMapping('field_hfa_priorities', 'field_project_hfa_priorities')->sourceMigration('HfaPriorities');
    $this->addFieldMapping('field_hfa_priorities:source_type')->defaultValue('tid');

    $this->addFieldMapping('field_rfa_priorities', 'field_project_rfa_priorities')->sourceMigration('RfaPriorities');
    $this->addFieldMapping('field_rfa_priorities:source_type')->defaultValue('tid');

    $this->addFieldMapping('field_national_objective', 'field_project_national_obj')->sourceMigration('NationalFrameworks');
    $this->addFieldMapping('field_national_objective:source_type')->defaultValue('tid');

    $this->addFieldMapping('field_implementing_countries', 'field_country')->sourceMigration('Countries');

    $this->addFieldMapping('field_implementing_agency', 'field_project_implement_agency')->sourceMigration('Organisations');

    $this->addFieldMapping('field_donor_enity', 'field_project_donors')->sourceMigration('Donors');

    $this->addFieldMapping('field_project_sites_3', 'locations');

    // For these fields, multiple source fields get aggregated in prepareRow().
    $this->addFieldMapping('field_organisation_entity', 'organisations')->sourceMigration('Organisations');
    $this->addFieldMapping('field_project_contact', 'contacts')->sourceMigration('Contacts');

    $this->addFieldMapping('field_related_documents_entity', 'field_project_files')->sourceMigration('ProjectFiles');
    $this->addFieldMapping('field_related_documents_entity:file_class')->defaultValue('MigrateFileFid');
  }

  /**
   * Overrides DrupalNode7Migration::prepareRow().
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Make sure the mapping for field_short_title always has a value.
    if (empty($row->field_project_short_name)) {
      $row->field_project_short_name = $row->title;
    }

    // Gather location data from Location module and format for Get Locations.
    if (!empty($row->field_project_sites)) {
      $row->locations = $this->loadLocations($row->field_project_sites);
    }

    // Aggregate values from multiple organisation reference fields.
    $row->organisations = $row->contacts = array();
    $organisation_fields = array(
      'field_project_dev_partner',
      'field_project_implement_agency',
      'field_project_lead_organiation'
    );
    foreach ($organisation_fields as $field_name) {
      if (!empty($row->{$field_name})) {
        $row->organisations = array_merge($row->organisations, $row->{$field_name});
      }
    }
    $row->organisations = $this->uniquify($row->organisations);

    // Aggregate values from multiple contact reference fields.
    $contact_fields = array(
      'field_contact',
      'field_lead_contacts',
      'field_partner_s_contact_s_',
      'field_project_agencys_contacts',
      'field_donors_contacts'
    );
    foreach ($contact_fields as $field_name) {
      if (!empty($row->{$field_name})) {
        $row->contacts = array_merge($row->contacts, $row->{$field_name});
      }
    }
    $row->contacts = $this->uniquify($row->contacts);

    return TRUE;
  }

  /**
   * Overrides Migration::handleSourceMigration();
   */
  public function handleSourceMigration($source_migrations, $source_keys, $default = NULL, $migration = NULL) {
    if ($source_migrations == 'Topics') {
      $migrations = array(
        'ThemesAndSectors',
        'ThemesAndSectors2',
        'ThemesAndSectors3',
        'SpecificSectors',
        'SpecificSectors2',
        'ContentTopics',
        'ContentTopics2',
        'Hazards',
      );
      return $this->handleMultipleMigrations($migrations, $source_keys, $default, $migration);
    }
    return parent::handleSourceMigration($source_migrations, $source_keys, $default, $migration);
  }

}