<?php
/**
 * @file
 * Contains NabResourceLinksMigration.
 */

/**
 * Class NabResourceLinksMigration
 *
 * Migrates resource link nodes.
 */
class NabResourceLinksMigration extends NabNodeMigration {

  public static $migrationArguments = array(
    'source_type' => 'resource_links',
    'destination_type' => 'link',
    'user_migration' => 'Users',
    'dependencies' => array(
      'ThemesAndSectors',
      'SpecificSectors',
      'Users',
    ),
  );

  /**
   * Overrides DrupalNode7Migration::__construct().
   */
  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->addFieldMapping('field_description', 'field_description');
    $this->addFieldMapping('field_url', 'field_poject_result_links');

    $this->addFieldMapping('field_topics', 'topics')->sourceMigration('Topics')->callbacks(array($this, 'addParentTopics'));
    $this->addFieldMapping('field_topics:source_type')->defaultValue('tid');
  }

  /**
   * Overrides DrupalNode7Migration::prepareRow().
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Aggregate values from multiple reference fields.
    $row->topics = array();
    $topic_fields = array(
      'field_sectors_and_themes',
      'field_specific_sectors_focus',
    );
    foreach ($topic_fields as $field_name) {
      if (!empty($row->{$field_name})) {
        $row->topics = array_merge($row->topics, $row->{$field_name});
      }
    }
    $row->topics = $this->uniquify($row->topics);

    return TRUE;
  }
}