<?php

/**
 * @file
 * Contains NabRfaPrioritiesMigration.
 */

/**
 * Class NabRfaPrioritiesMigration
 */
class NabRfaPrioritiesMigration extends NabModifiedTermMigration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'rfa_priorities',
    'destination_vocabulary' => 'rfa_priorities',
    'mappings_csv' => 'rfa_priorities.csv',
  );

  /**
   * Overrides NabModifiedTermMigration::modifyTerm().
   */
  protected function modifyTerm($term) {
    $key = strtolower($term->name);
    if (isset($this->csvMappings[$key])) {
      $text = reset($this->csvMappings[$key]);
      $text = str_replace('Tooltip - ', '', $text);
      $term->name = $text;
    }
  }
}