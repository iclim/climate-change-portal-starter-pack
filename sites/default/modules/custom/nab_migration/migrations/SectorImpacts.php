<?php
/**
 * @file
 * Contains NabSectorImpactsMigration.
 */

class NabSectorImpactsMigration extends DrupalTerm7Migration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'sector_impact',
    'destination_vocabulary' => 'sector_impact',
  );
}