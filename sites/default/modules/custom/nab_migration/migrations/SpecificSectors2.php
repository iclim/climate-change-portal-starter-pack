<?php

/**
 * @file
 * Contains NabSpecificSectors2Migration.
 */

/**
 * Class NabSpecificSectors2Migration
 *
 * Handles any secondary term mappings (migration_key = 1).
 */
class NabSpecificSectors2Migration extends NabSpecificSectorsMigration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'specific_sector_focus',
    'destination_vocabulary' => 'topics',
    'mappings_csv' => 'specific_sectors.csv',
    'migration_key' => 1,
  );
}