<?php
/**
 * @file
 * Contains NabSubjectStrandsMigration.
 */

/**
 * Class NabSubjectStrandsMigration
 *
 * CSV corrections:
 *  - Replaced "and" with ampersands
 *  - Fixed name formatting and removed duplicate row
 */
class NabSubjectStrandsMigration extends NabStaticTermMigration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'subject_syllabus_strand_',
    'destination_vocabulary' => 'subject_strand',
    'mappings_csv' => 'subject_strands.csv',
    'migration_key' => 0,
    'dependencies' => array(
      'SubjectStrands2',
    ),
  );

  /**
   * Overrides NabStaticTermMigration::prepareRow().
   */
  // public function prepareRow($row) {
  //   if (parent::prepareRow($row) === FALSE) {
  //     return FALSE;
  //   }
  // }

  /**
   * Overrides NabStaticTermMigration::mapCsvTarget().
   */
  public function mapCsvTarget($target) {
    $tids = array();

    // Some terms get mapped to multiple destination terms.
    $names = array_map('trim', explode(';', strtolower($target)));
    foreach ($names as $name) {
      $matches = parent::mapCsvTarget($name);
      $tids = array_merge($tids, $matches);
    }

    return $tids;
  }
}