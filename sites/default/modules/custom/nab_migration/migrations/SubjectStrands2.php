<?php
/**
 * @file
 * Contains NabSubjectStrands2Migration.
 */

class NabSubjectStrands2Migration extends NabSubjectStrandsMigration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'subject_syllabus_strand_',
    'destination_vocabulary' => 'subject_strand',
    'mappings_csv' => 'subject_strands.csv',
    'migration_key' => 1,
  );
}