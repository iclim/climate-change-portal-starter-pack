<?php

/**
 * @file
 * Contains NabThemesAndSectorsMigration.
 */

/**
 * Class NabThemesAndSectorsMigration
 *
 * CSV corrections:
 *  - Fixed a number of typos
 */
class NabThemesAndSectorsMigration extends NabStaticTermMigration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'themes_and_sectors',
    'destination_vocabulary' => 'topics',
    'mappings_csv' => 'themes_and_sectors.csv',
    'migration_key' => 0,
    'dependencies' => array(
      'ThemesAndSectors2',
      'ThemesAndSectors3',
      'ThemesAndSectors4',
    )
  );

  /**
   * Overrides NabStaticTermMigration::mapCsvTarget().
   */
  protected function mapCsvTarget($target) {
    $tids = array();

    $terms = $this->destTermTree;

    $names = array_map('trim', explode(';', strtolower($target)));
    $names = array_filter($names, function($name) {
      return (strpos($name, '(focus area taxonomy)') === FALSE);
    });

    foreach ($names as $name) {
      list($root_name, $child_name) = array_map(array($this, 'trim'), explode('-', $name)) + array(NULL, NULL);

      // Single parent term.
      if ($root_name && empty($child_name)) {
        if (isset($terms[$root_name])) {
          $tids[] = $terms[$root_name]->tid;
        }
        else {
          $this->logMissingTarget($name);
        }
      }

      // Parent term plus child term.
      if ($root_name && $child_name) {
        if (isset($terms[$root_name]) && isset($terms[$root_name]->children[$child_name])) {
          $tids[] = $terms[$root_name]->children[$child_name]->tid;
        }
        else {
          $this->logMissingTarget($name);
        }
      }
    }

    return $tids;
  }
}