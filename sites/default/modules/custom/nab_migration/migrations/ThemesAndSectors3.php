<?php

/**
 * @file
 * Contains NabThemesAndSectors3Migration.
 */
class NabThemesAndSectors3Migration extends NabThemesAndSectorsMigration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'themes_and_sectors',
    'destination_vocabulary' => 'topics',
    'mappings_csv' => 'themes_and_sectors.csv',
    'migration_key' => 2,
  );
}