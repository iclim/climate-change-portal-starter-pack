<?php

/**
 * @file
 * Contains NabThemesAndSectors4Migration.
 */

class NabThemesAndSectors4Migration extends NabStaticTermMigration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'themes_and_sectors',
    'destination_vocabulary' => 'focus_area',
    'mappings_csv' => 'themes_and_sectors.csv',
  );

  /**
   * Overrides NabStaticTermMigration::mapCsvTarget().
   */
  protected function mapCsvTarget($target) {
    $tids = array();

    $terms = $this->destTermTree;

    $names = array_map(array($this, 'trim'), explode(';', strtolower($target)));
    $names = array_filter($names, function($name) {
      return (strpos($name, '(focus area taxonomy)') > 0);
    });

    foreach ($names as $name) {
      $name = trim(str_replace('(focus area taxonomy)', '', $name));
      if (isset($terms[$name])) {
        $tids[] = $terms[$name]->tid;
      }
    }

    return $tids;
  }
}