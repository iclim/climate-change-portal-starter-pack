<?php

/**
 * @file
 * Contains UsersMigration.
 */

class NabUsersMigration extends DrupalUser7Migration {

  public static $migrationArguments = array(
    'source_type' => 'user',
    'destination_type' => 'user',
  );
}