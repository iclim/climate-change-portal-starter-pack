<?php
/**
 * @file
 * Contains NabUtilizedInMigration.
 */

class NabUtilizedInMigration extends NabStaticTermMigration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'utilized_in',
    'destination_vocabulary' => 'educational_utilised_in',
    'mappings_csv' => 'utilized_in.csv',
    'migration_key' => 0,
    'dependencies' => array(
      'UtilizedIn2',
    ),
  );

  /**
   * Overrides NabStaticTermMigration::mapCsvTarget().
   */
  public function mapCsvTarget($target) {
    $tids = array();

    // Some terms get mapped to multiple destination terms.
    $names = array_map('trim', explode(',', strtolower($target)));
    foreach ($names as $name) {
      $matches = parent::mapCsvTarget($name);
      $tids = array_merge($tids, $matches);
    }

    return $tids;
  }
}