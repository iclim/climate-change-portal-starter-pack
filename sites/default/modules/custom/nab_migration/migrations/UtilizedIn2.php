<?php
/**
 * @file
 * Contains NabUtilizedIn2Migration.
 */

class NabUtilizedIn2Migration extends NabUtilizedInMigration {

  public static $migrationArguments = array(
    'source_vocabulary' => 'utilized_in',
    'destination_vocabulary' => 'educational_utilised_in',
    'mappings_csv' => 'utilized_in.csv',
    'migration_key' => 1,
  );
}