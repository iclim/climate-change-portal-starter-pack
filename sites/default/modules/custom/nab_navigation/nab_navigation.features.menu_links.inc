<?php
/**
 * @file
 * nab_navigation.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function nab_navigation_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_events:event-calendar.
  $menu_links['main-menu_events:event-calendar'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'event-calendar',
    'router_path' => 'event-calendar',
    'link_title' => 'Events',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_events:event-calendar',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: main-menu_home:<front>.
  $menu_links['main-menu_home:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'identifier' => 'main-menu_home:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_news:news.
  $menu_links['main-menu_news:news'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'news',
    'router_path' => 'news',
    'link_title' => 'News',
    'options' => array(
      'identifier' => 'main-menu_news:news',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: main-menu_regional-search:regional-search.
  $menu_links['main-menu_regional-search:regional-search'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'regional-search',
    'router_path' => 'regional-search',
    'link_title' => 'Regional Search',
    'options' => array(
      'identifier' => 'main-menu_regional-search:regional-search',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_search:sitesearch',
  );
  // Exported menu link: main-menu_topics:topic.
  $menu_links['main-menu_topics:topic'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'topic',
    'router_path' => 'topic',
    'link_title' => 'Topics',
    'options' => array(
      'identifier' => 'main-menu_topics:topic',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -42,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Events');
  t('Home');
  t('News');
  t('Regional Search');
  t('Topics');

  return $menu_links;
}
