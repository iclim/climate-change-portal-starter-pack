<?php
/**
 * @file
 * nab_news.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function nab_news_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|news|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'news';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'post_date' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'ds_post_date_short_date_only',
    ),
  );
  $export['node|news|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|news|search_index';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'news';
  $ds_fieldsetting->view_mode = 'search_index';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|news|search_index'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|news|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'news';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h2',
        'class' => '',
      ),
    ),
    'node_link' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'post_date' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'ds_post_date_short_date_only',
    ),
  );
  $export['node|news|teaser'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function nab_news_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|news|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'news';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'bootstrap_9_3';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_image',
        1 => 'post_date',
        4 => 'field_text',
        5 => 'field_document_files',
      ),
      'right' => array(
        2 => 'field_country',
        3 => 'field_tags',
        6 => 'field_topics',
        7 => 'group_additional_information',
        8 => 'field_related_projects_entity',
        9 => 'field_url',
        10 => 'group_categorisation',
        11 => 'group_related_information',
      ),
    ),
    'fields' => array(
      'field_image' => 'left',
      'post_date' => 'left',
      'field_country' => 'right',
      'field_tags' => 'right',
      'field_text' => 'left',
      'field_document_files' => 'left',
      'field_topics' => 'right',
      'group_additional_information' => 'right',
      'field_related_projects_entity' => 'right',
      'field_url' => 'right',
      'group_categorisation' => 'right',
      'group_related_information' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|news|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|news|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'news';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'group_news',
        1 => 'title',
        3 => 'field_image',
        5 => 'field_text',
        7 => 'field_document_files',
        10 => 'field_tags',
        12 => 'field_country',
        13 => 'field_topics',
        16 => 'field_related_projects_entity',
        17 => 'field_url',
        19 => 'group_general',
        20 => 'group_categorisation',
        21 => 'group_relations',
      ),
      'hidden' => array(
        2 => 'field_related_links_entity',
        4 => 'path',
        6 => 'field_related_documents_entity',
        8 => 'field_file_description',
        9 => 'field_file_s_',
        11 => 'field_archived',
        14 => 'field_hits',
        15 => 'field_joomla_hits',
        18 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'group_news' => 'ds_content',
      'title' => 'ds_content',
      'field_related_links_entity' => 'hidden',
      'field_image' => 'ds_content',
      'path' => 'hidden',
      'field_text' => 'ds_content',
      'field_related_documents_entity' => 'hidden',
      'field_document_files' => 'ds_content',
      'field_file_description' => 'hidden',
      'field_file_s_' => 'hidden',
      'field_tags' => 'ds_content',
      'field_archived' => 'hidden',
      'field_country' => 'ds_content',
      'field_topics' => 'ds_content',
      'field_hits' => 'hidden',
      'field_joomla_hits' => 'hidden',
      'field_related_projects_entity' => 'ds_content',
      'field_url' => 'ds_content',
      '_add_existing_field' => 'hidden',
      'group_general' => 'ds_content',
      'group_categorisation' => 'ds_content',
      'group_relations' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|news|form'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|news|search_index';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'news';
  $ds_layout->view_mode = 'search_index';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_image',
        2 => 'field_text',
        3 => 'field_document_files',
        4 => 'field_country',
        5 => 'field_tags',
        6 => 'field_topics',
        7 => 'field_related_projects_entity',
        8 => 'field_url',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_image' => 'ds_content',
      'field_text' => 'ds_content',
      'field_document_files' => 'ds_content',
      'field_country' => 'ds_content',
      'field_tags' => 'ds_content',
      'field_topics' => 'ds_content',
      'field_related_projects_entity' => 'ds_content',
      'field_url' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|news|search_index'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|news|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'news';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'post_date',
        2 => 'field_text',
        3 => 'node_link',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'post_date' => 'ds_content',
      'field_text' => 'ds_content',
      'node_link' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|news|teaser'] = $ds_layout;

  return $export;
}
