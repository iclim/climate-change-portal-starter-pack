<?php
/**
 * @file
 * nab_news.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function nab_news_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_additional_information|node|news|default';
  $field_group->group_name = 'group_additional_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'news';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Additional Information',
    'weight' => '4',
    'children' => array(
      0 => 'group_categorisation',
      1 => 'group_related_information',
    ),
    'format_type' => 'accordion',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-additional-information field-group-accordion',
        'effect' => 'bounceslide',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_additional_information|node|news|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_categorisation|node|news|default';
  $field_group->group_name = 'group_categorisation';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'news';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_additional_information';
  $field_group->data = array(
    'label' => 'Categorisation',
    'weight' => '18',
    'children' => array(
      0 => 'field_tags',
      1 => 'field_country',
      2 => 'field_topics',
    ),
    'format_type' => 'accordion-item',
    'format_settings' => array(
      'label' => 'Categorisation',
      'instance_settings' => array(
        'classes' => 'group-categorisation field-group-accordion-item',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_categorisation|node|news|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_categorisation|node|news|form';
  $field_group->group_name = 'group_categorisation';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'news';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_news';
  $field_group->data = array(
    'label' => 'Categorisation',
    'weight' => '15',
    'children' => array(
      0 => 'field_tags',
      1 => 'field_topics',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Categorisation',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-categorisation field-group-htab',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_categorisation|node|news|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_files|node|news|default';
  $field_group->group_name = 'group_files';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'news';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Files',
    'weight' => '20',
    'children' => array(
      0 => 'field_file_description',
      1 => 'field_file_s_',
    ),
    'format_type' => 'accordion-item',
    'format_settings' => array(
      'label' => 'Files',
      'instance_settings' => array(
        'classes' => 'group-files field-group-accordion-item',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_files|node|news|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_general|node|news|form';
  $field_group->group_name = 'group_general';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'news';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_news';
  $field_group->data = array(
    'label' => 'General Information',
    'weight' => '14',
    'children' => array(
      0 => 'field_text',
      1 => 'field_document_files',
      2 => 'field_image',
      3 => 'title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'General Information',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-general field-group-htab',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_general|node|news|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_news|node|news|form';
  $field_group->group_name = 'group_news';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'news';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'News',
    'weight' => '0',
    'children' => array(
      0 => 'group_general',
      1 => 'group_relations',
      2 => 'group_categorisation',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-news field-group-htabs',
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_news|node|news|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_related_information|node|news|default';
  $field_group->group_name = 'group_related_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'news';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_additional_information';
  $field_group->data = array(
    'label' => 'Related Information',
    'weight' => '19',
    'children' => array(
      0 => 'field_related_projects_entity',
      1 => 'field_url',
    ),
    'format_type' => 'accordion-item',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => 'group-related-information field-group-accordion-item',
      ),
    ),
  );
  $field_groups['group_related_information|node|news|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_relations|node|news|form';
  $field_group->group_name = 'group_relations';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'news';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_news';
  $field_group->data = array(
    'label' => 'Related Information',
    'weight' => '17',
    'children' => array(
      0 => 'field_country',
      1 => 'field_related_projects_entity',
      2 => 'field_url',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Related Information',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-relations field-group-htab',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_relations|node|news|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Additional Information');
  t('Categorisation');
  t('Files');
  t('General Information');
  t('News');
  t('Related Information');

  return $field_groups;
}
