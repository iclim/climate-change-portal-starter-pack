<?php
/**
 * @file
 * nab_opportunities.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function nab_opportunities_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|job_tender_opportunity|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'job_tender_opportunity';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'bootstrap_8_4';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'body',
      ),
      'right' => array(
        1 => 'field_expiry_date',
        2 => 'field_document_files',
      ),
    ),
    'fields' => array(
      'body' => 'left',
      'field_expiry_date' => 'right',
      'field_document_files' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|job_tender_opportunity|default'] = $ds_layout;

  return $export;
}
