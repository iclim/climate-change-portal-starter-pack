<?php
/**
 * @file
 * nab_opportunities.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function nab_opportunities_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_additional_information|node|job_tender_opportunity|default';
  $field_group->group_name = 'group_additional_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'job_tender_opportunity';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Additional Information',
    'weight' => '2',
    'children' => array(
      0 => 'group_related_documents',
    ),
    'format_type' => 'accordion',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-additional-information field-group-accordion',
        'effect' => 'bounceslide',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_additional_information|node|job_tender_opportunity|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_closing_date|node|job_tender_opportunity|default';
  $field_group->group_name = 'group_closing_date';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'job_tender_opportunity';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Closing Date',
    'weight' => '11',
    'children' => array(),
    'format_type' => 'accordion-item',
    'format_settings' => array(
      'label' => 'Closing Date',
      'instance_settings' => array(
        'classes' => 'group-closing-date field-group-accordion-item',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_closing_date|node|job_tender_opportunity|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_related_documents|node|job_tender_opportunity|default';
  $field_group->group_name = 'group_related_documents';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'job_tender_opportunity';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_additional_information';
  $field_group->data = array(
    'label' => 'Related Documents',
    'weight' => '4',
    'children' => array(
      0 => 'field_related_documents_entity',
    ),
    'format_type' => 'accordion-item',
    'format_settings' => array(
      'label' => 'Related Documents',
      'instance_settings' => array(
        'classes' => 'group-related-documents field-group-accordion-item',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_related_documents|node|job_tender_opportunity|default'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Additional Information');
  t('Closing Date');
  t('Related Documents');

  return $field_groups;
}
