<?php
/**
 * @file
 * nab_opportunities.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function nab_opportunities_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'tender_job_opportunities';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Jobs and tenders';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Jobs and tenders';
  $handler->display->display_options['use_more'] = TRUE;
  $handler->display->display_options['use_more_always'] = TRUE;
  $handler->display->display_options['use_more_text'] = 'View More';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'views-row';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Closing Date */
  $handler->display->display_options['fields']['field_expiry_date']['id'] = 'field_expiry_date';
  $handler->display->display_options['fields']['field_expiry_date']['table'] = 'field_data_field_expiry_date';
  $handler->display->display_options['fields']['field_expiry_date']['field'] = 'field_expiry_date';
  $handler->display->display_options['fields']['field_expiry_date']['settings'] = array(
    'format_type' => 'short_date_only',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Sort criterion: Content: Closing Date (field_expiry_date) */
  $handler->display->display_options['sorts']['field_expiry_date_value']['id'] = 'field_expiry_date_value';
  $handler->display->display_options['sorts']['field_expiry_date_value']['table'] = 'field_data_field_expiry_date';
  $handler->display->display_options['sorts']['field_expiry_date_value']['field'] = 'field_expiry_date_value';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'job_tender_opportunity' => 'job_tender_opportunity',
  );
  /* Filter criterion: Content: Closing Date (field_expiry_date) */
  $handler->display->display_options['filters']['field_expiry_date_value']['id'] = 'field_expiry_date_value';
  $handler->display->display_options['filters']['field_expiry_date_value']['table'] = 'field_data_field_expiry_date';
  $handler->display->display_options['filters']['field_expiry_date_value']['field'] = 'field_expiry_date_value';
  $handler->display->display_options['filters']['field_expiry_date_value']['operator'] = '>';
  $handler->display->display_options['filters']['field_expiry_date_value']['default_date'] = 'now -7 days';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'opportunities_page');
  $handler->display->display_options['defaults']['use_more'] = FALSE;
  $handler->display->display_options['defaults']['use_more_always'] = FALSE;
  $handler->display->display_options['defaults']['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_always'] = TRUE;
  $handler->display->display_options['defaults']['use_more_text'] = FALSE;
  $handler->display->display_options['use_more_text'] = 'View More';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h2';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Closing Date */
  $handler->display->display_options['fields']['field_expiry_date']['id'] = 'field_expiry_date';
  $handler->display->display_options['fields']['field_expiry_date']['table'] = 'field_data_field_expiry_date';
  $handler->display->display_options['fields']['field_expiry_date']['field'] = 'field_expiry_date';
  $handler->display->display_options['fields']['field_expiry_date']['element_label_type'] = 'strong';
  $handler->display->display_options['fields']['field_expiry_date']['settings'] = array(
    'format_type' => 'short_date_only',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 1,
  );
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_summary_or_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '600',
  );
  $handler->display->display_options['path'] = 'jobs-and-tenders';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'opportunities_block');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $export['tender_job_opportunities'] = $view;

  return $export;
}
