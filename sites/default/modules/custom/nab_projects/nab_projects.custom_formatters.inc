<?php
/**
 * @file
 * nab_projects.custom_formatters.inc
 */

/**
 * Implements hook_custom_formatters_defaults().
 */
function nab_projects_custom_formatters_defaults() {
  $export = array();

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'climate_tools_search_term';
  $formatter->label = 'Climate Tools Search Term';
  $formatter->description = '';
  $formatter->mode = 'php';
  $formatter->field_types = 'taxonomy_term_reference';
  $formatter->code = '$field_name = $variables["#instance"][\'field_name\'];
$url = "/projects?f[0]=".$field_name.":";

if (count($variables["#items"])):

   foreach($variables["#items"] as $item):
      $term = taxonomy_term_load($item[\'tid\']);
      print \'<div><a href="\'.$url .$item[\'tid\']  .\'">\'.$term->name.\'</a></div>\';
   endforeach;

endif;';
  $formatter->fapi = '';
  $export['climate_tools_search_term'] = $formatter;

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'donor_search_formatter';
  $formatter->label = 'Donor_Search_formatter';
  $formatter->description = 'This formatter will link taxonomy entries to the donor database search interface';
  $formatter->mode = 'php';
  $formatter->field_types = 'taxonomy_term_reference';
  $formatter->code = '$field_name = $variables["#instance"][\'field_name\'];
$url = "/donor-database?f[0]=".$field_name.":";

if (count($variables["#items"])):

   foreach($variables["#items"] as $item):
      $term = taxonomy_term_load($item[\'tid\']);
      print \'<div><a href="\'.$url .$item[\'tid\']  .\'">\'.$term->name.\'</a></div>\';
   endforeach;

endif;';
  $formatter->fapi = '';
  $export['donor_search_formatter'] = $formatter;

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'product_search_term';
  $formatter->label = 'product_search_term';
  $formatter->description = '';
  $formatter->mode = 'php';
  $formatter->field_types = 'taxonomy_term_reference';
  $formatter->code = '$field_name = $variables["#instance"][\'field_name\'];
$url = "/projects?f[0]=".$field_name.":";

if (count($variables["#items"])):

   foreach($variables["#items"] as $item):
      $term = taxonomy_term_load($item[\'tid\']);
      print \'<div><a href="\'.$url .$item[\'tid\']  .\'">\'.$term->name.\'</a></div>\';
   endforeach;

endif;';
  $formatter->fapi = '';
  $export['product_search_term'] = $formatter;

  return $export;
}
