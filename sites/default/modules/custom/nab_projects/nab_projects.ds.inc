<?php
/**
 * @file
 * nab_projects.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function nab_projects_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|donor|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'donor';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'p',
        'class' => '',
      ),
    ),
  );
  $export['node|donor|teaser'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|funding_opportunity|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'funding_opportunity';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'post_date' => array(
      'weight' => '0',
      'label' => 'inline',
      'format' => 'ds_post_date_short_date_only',
    ),
  );
  $export['node|funding_opportunity|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|product|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'product';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'related_news' => array(
      'weight' => '25',
      'label' => 'above',
      'format' => 'default',
      'formatter_settings' => array(
        'show_title' => 0,
        'title_wrapper' => '',
        'ctools' => 'a:3:{s:4:"conf";a:16:{s:23:"override_pager_settings";i:0;s:9:"use_pager";i:0;s:14:"nodes_per_page";s:1:"0";s:8:"pager_id";s:1:"0";s:6:"offset";s:1:"0";s:9:"more_link";i:0;s:10:"feed_icons";i:0;s:10:"panel_args";i:0;s:12:"link_to_view";i:0;s:4:"args";s:0:"";s:3:"url";s:0:"";s:7:"display";s:7:"default";s:7:"context";a:1:{i:0;s:29:"argument_entity_id:node_1.nid";}s:14:"override_title";i:1;s:19:"override_title_text";s:4:"News";s:22:"override_title_heading";s:2:"h3";}s:4:"type";s:5:"views";s:7:"subtype";s:20:"project_related_news";}',
        'load_terms' => 0,
      ),
    ),
    'post_date' => array(
      'weight' => '1',
      'label' => 'inline',
      'format' => 'ds_post_date_short_date_only',
    ),
  );
  $export['node|product|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|product|search_index';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'product';
  $ds_fieldsetting->view_mode = 'search_index';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|product|search_index'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function nab_projects_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'related_news';
  $ds_field->label = 'News';
  $ds_field->field_type = 7;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = 'product|*';
  $ds_field->properties = array(
    'default' => array(),
    'settings' => array(
      'show_title' => array(
        'type' => 'checkbox',
      ),
      'title_wrapper' => array(
        'type' => 'textfield',
        'description' => 'Eg: h1, h2, p',
      ),
      'ctools' => array(
        'type' => 'ctools',
      ),
    ),
  );
  $export['related_news'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function nab_projects_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|country|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'country';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_fluid';
  $ds_layout->settings = array(
    'regions' => array(
      'right' => array(
        0 => 'field_related_projects_entity',
        1 => 'field_related_documents_entity',
        2 => 'field_related_content_entity',
        3 => 'field_related_news',
        4 => 'field_related_events',
        5 => 'field_related_links_entity',
        18 => 'field_flag_image',
        19 => 'field_iso_code',
        20 => 'group_more_info_group',
        21 => 'field_population',
        22 => 'field_languages',
        23 => 'field_map_image',
        24 => 'group_country_details',
        25 => 'group_related_info',
      ),
      'left' => array(
        6 => 'field_country_overview',
        7 => 'field_country_national_prioritie',
        8 => 'field_country_key_national_poli',
        9 => 'field_country_adaptation',
        10 => 'field_country_partnerships',
        11 => 'field_country_climate_informatio',
        12 => 'field_country_education',
        13 => 'field_country_mitigation',
        14 => 'field_country_ongoing_challenges',
        15 => 'field_country_governance',
        16 => 'field_country_focal_points',
        17 => 'field_country_references',
      ),
    ),
    'fields' => array(
      'field_related_projects_entity' => 'right',
      'field_related_documents_entity' => 'right',
      'field_related_content_entity' => 'right',
      'field_related_news' => 'right',
      'field_related_events' => 'right',
      'field_related_links_entity' => 'right',
      'field_country_overview' => 'left',
      'field_country_national_prioritie' => 'left',
      'field_country_key_national_poli' => 'left',
      'field_country_adaptation' => 'left',
      'field_country_partnerships' => 'left',
      'field_country_climate_informatio' => 'left',
      'field_country_education' => 'left',
      'field_country_mitigation' => 'left',
      'field_country_ongoing_challenges' => 'left',
      'field_country_governance' => 'left',
      'field_country_focal_points' => 'left',
      'field_country_references' => 'left',
      'field_flag_image' => 'right',
      'field_iso_code' => 'right',
      'group_more_info_group' => 'right',
      'field_population' => 'right',
      'field_languages' => 'right',
      'field_map_image' => 'right',
      'group_country_details' => 'right',
      'group_related_info' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['node|country|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|country|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'country';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'group_country',
        1 => 'title',
        2 => 'path',
        4 => 'field_iso_code',
        5 => 'group_general',
        6 => 'field_population',
        7 => 'field_country_overview',
        10 => 'group_content',
        11 => 'field_languages',
        12 => 'field_country_national_prioritie',
        13 => 'field_flag_image',
        14 => 'field_country_governance',
        16 => 'field_country_key_national_poli',
        17 => 'field_map_image',
        19 => 'field_location_latlon',
        20 => 'field_country_adaptation',
        22 => 'field_country_partnerships',
        24 => 'field_country_climate_informatio',
        25 => 'field_country_education',
        26 => 'field_country_mitigation',
        27 => 'field_country_ongoing_challenges',
        28 => 'field_country_focal_points',
        29 => 'field_country_references',
      ),
      'hidden' => array(
        3 => 'group_relations',
        8 => '_add_existing_field',
        9 => 'field_related_projects_entity',
        15 => 'field_related_documents_entity',
        18 => 'field_related_links_entity',
        21 => 'field_related_events',
        23 => 'field_related_news',
      ),
    ),
    'fields' => array(
      'group_country' => 'ds_content',
      'title' => 'ds_content',
      'path' => 'ds_content',
      'group_relations' => 'hidden',
      'field_iso_code' => 'ds_content',
      'group_general' => 'ds_content',
      'field_population' => 'ds_content',
      'field_country_overview' => 'ds_content',
      '_add_existing_field' => 'hidden',
      'field_related_projects_entity' => 'hidden',
      'group_content' => 'ds_content',
      'field_languages' => 'ds_content',
      'field_country_national_prioritie' => 'ds_content',
      'field_flag_image' => 'ds_content',
      'field_country_governance' => 'ds_content',
      'field_related_documents_entity' => 'hidden',
      'field_country_key_national_poli' => 'ds_content',
      'field_map_image' => 'ds_content',
      'field_related_links_entity' => 'hidden',
      'field_location_latlon' => 'ds_content',
      'field_country_adaptation' => 'ds_content',
      'field_related_events' => 'hidden',
      'field_country_partnerships' => 'ds_content',
      'field_related_news' => 'hidden',
      'field_country_climate_informatio' => 'ds_content',
      'field_country_education' => 'ds_content',
      'field_country_mitigation' => 'ds_content',
      'field_country_ongoing_challenges' => 'ds_content',
      'field_country_focal_points' => 'ds_content',
      'field_country_references' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|country|form'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|donor|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'donor';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_fluid';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_related_organisation',
        1 => 'field_aid_type',
        2 => 'field_objectives',
        3 => 'field_fund_status',
        5 => 'field_application_procedure',
      ),
      'right' => array(
        4 => 'field_country_focus',
        6 => 'field_logo',
        7 => 'field_pccp_sectoral_focus',
        8 => 'field_related_funds',
        9 => 'field_contact_details',
        10 => 'field_country',
        11 => 'field_website',
        12 => 'field_related_projects_entity',
        13 => 'group_other_info',
        14 => 'field_related_documents_entity',
        15 => 'field_global_funding_usd',
        16 => 'field_global_funding_allocation_',
        17 => 'field_cofinancing',
        18 => 'field_financing_modality',
        19 => 'group_contact',
        20 => 'group_finance',
        21 => 'group_relation_information',
      ),
    ),
    'fields' => array(
      'field_related_organisation' => 'left',
      'field_aid_type' => 'left',
      'field_objectives' => 'left',
      'field_fund_status' => 'left',
      'field_country_focus' => 'right',
      'field_application_procedure' => 'left',
      'field_logo' => 'right',
      'field_pccp_sectoral_focus' => 'right',
      'field_related_funds' => 'right',
      'field_contact_details' => 'right',
      'field_country' => 'right',
      'field_website' => 'right',
      'field_related_projects_entity' => 'right',
      'group_other_info' => 'right',
      'field_related_documents_entity' => 'right',
      'field_global_funding_usd' => 'right',
      'field_global_funding_allocation_' => 'right',
      'field_cofinancing' => 'right',
      'field_financing_modality' => 'right',
      'group_contact' => 'right',
      'group_finance' => 'right',
      'group_relation_information' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['node|donor|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|donor|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'donor';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'group_fund',
        1 => 'path',
        3 => 'field_global_funding_allocation_',
        4 => 'field_global_funding_usd',
        5 => 'field_cofinancing',
        6 => 'field_financing_modality',
        7 => 'title',
        8 => 'field_related_organisation',
        9 => 'group_general',
        10 => 'field_aid_type',
        11 => 'group_finance',
        12 => 'field_objectives',
        13 => 'group_relations',
        14 => 'field_fund_status',
        15 => 'field_website',
        16 => 'field_logo',
        17 => 'field_country_focus',
        18 => 'field_application_procedure',
        19 => 'field_contact_details',
        20 => 'field_pccp_sectoral_focus',
        21 => 'field_related_funds',
        22 => 'field_country',
        23 => 'field_related_documents_entity',
      ),
      'hidden' => array(
        2 => 'field_related_projects_entity',
        24 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'group_fund' => 'ds_content',
      'path' => 'ds_content',
      'field_related_projects_entity' => 'hidden',
      'field_global_funding_allocation_' => 'ds_content',
      'field_global_funding_usd' => 'ds_content',
      'field_cofinancing' => 'ds_content',
      'field_financing_modality' => 'ds_content',
      'title' => 'ds_content',
      'field_related_organisation' => 'ds_content',
      'group_general' => 'ds_content',
      'field_aid_type' => 'ds_content',
      'group_finance' => 'ds_content',
      'field_objectives' => 'ds_content',
      'group_relations' => 'ds_content',
      'field_fund_status' => 'ds_content',
      'field_website' => 'ds_content',
      'field_logo' => 'ds_content',
      'field_country_focus' => 'ds_content',
      'field_application_procedure' => 'ds_content',
      'field_contact_details' => 'ds_content',
      'field_pccp_sectoral_focus' => 'ds_content',
      'field_related_funds' => 'ds_content',
      'field_country' => 'ds_content',
      'field_related_documents_entity' => 'ds_content',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|donor|form'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|donor|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'donor';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_related_organisation',
        2 => 'field_contact_details',
        3 => 'field_website',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_related_organisation' => 'ds_content',
      'field_contact_details' => 'ds_content',
      'field_website' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|donor|teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|funding_opportunity|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'funding_opportunity';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_fluid';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'post_date',
        1 => 'body',
      ),
      'right' => array(
        2 => 'group_additional_information',
        3 => 'group_funding_details',
        4 => 'field_logo',
        5 => 'field_donor_enity',
        6 => 'field_expiry_date',
        7 => 'field_topics',
      ),
    ),
    'fields' => array(
      'post_date' => 'left',
      'body' => 'left',
      'group_additional_information' => 'right',
      'group_funding_details' => 'right',
      'field_logo' => 'right',
      'field_donor_enity' => 'right',
      'field_expiry_date' => 'right',
      'field_topics' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['node|funding_opportunity|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|link|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'link';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_fluid';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_url',
        1 => 'field_description',
      ),
      'right' => array(
        2 => 'group_additional_info',
        3 => 'field_topics',
        4 => 'field_related_content_entity',
        5 => 'group_related_content',
      ),
    ),
    'fields' => array(
      'field_url' => 'left',
      'field_description' => 'left',
      'group_additional_info' => 'right',
      'field_topics' => 'right',
      'field_related_content_entity' => 'right',
      'group_related_content' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['node|link|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|link|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'link';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_description',
        2 => 'field_topics',
        3 => 'field_url',
      ),
      'hidden' => array(
        4 => 'path',
        5 => 'field_related_content_entity',
        6 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_description' => 'ds_content',
      'field_topics' => 'ds_content',
      'field_url' => 'ds_content',
      'path' => 'hidden',
      'field_related_content_entity' => 'hidden',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|link|form'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|product|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'product';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'nab_projects';
  $ds_layout->settings = array(
    'regions' => array(
      'top' => array(
        0 => 'field_project_status',
        1 => 'post_date',
      ),
      'top_left' => array(
        2 => 'group_project_primary',
        8 => 'field_objectives',
        16 => 'field_outputs',
        18 => 'field_activities',
        21 => 'field_actions',
        32 => 'field_project_outcomes',
        33 => 'field_the_description',
        34 => 'field_file_s_',
        35 => 'group_description',
        36 => 'group_objectives',
        37 => 'group_outcomes',
        38 => 'group_outputs',
      ),
      'top_middle' => array(
        3 => 'field_organisation_entity',
        4 => 'field_project_contact',
      ),
      'bottom_left' => array(
        5 => 'group_resources',
        20 => 'field_project_sites_3',
        25 => 'field_related_documents_entity',
        42 => 'group_related_documents',
        43 => 'group_project_sites',
      ),
      'right' => array(
        6 => 'field_project_scope',
        9 => 'field_project_type',
        10 => 'field_focus_area',
        11 => 'group_metadata',
        12 => 'field_national_framework',
        13 => 'field_hfa_priorities',
        14 => 'field_rfa_priorities',
        15 => 'field_start_date',
        17 => 'field_end_date',
        19 => 'field_implementing_countries',
        22 => 'field_implementing_agency',
        23 => 'field_donor_enity',
        24 => 'field_project_total_funding',
        26 => 'field_amount_donor_currency_',
        27 => 'field_related_projects_entity',
        28 => 'field_pifacc_themes',
        29 => 'field_topics',
        30 => 'field_tags',
        31 => 'related_news',
        39 => 'group_related_content',
        40 => 'group_funding',
        41 => 'group_project_information',
      ),
      'bottom_middle' => array(
        7 => 'field_related_links_entity',
      ),
    ),
    'fields' => array(
      'field_project_status' => 'top',
      'post_date' => 'top',
      'group_project_primary' => 'top_left',
      'field_organisation_entity' => 'top_middle',
      'field_project_contact' => 'top_middle',
      'group_resources' => 'bottom_left',
      'field_project_scope' => 'right',
      'field_related_links_entity' => 'bottom_middle',
      'field_objectives' => 'top_left',
      'field_project_type' => 'right',
      'field_focus_area' => 'right',
      'group_metadata' => 'right',
      'field_national_framework' => 'right',
      'field_hfa_priorities' => 'right',
      'field_rfa_priorities' => 'right',
      'field_start_date' => 'right',
      'field_outputs' => 'top_left',
      'field_end_date' => 'right',
      'field_activities' => 'top_left',
      'field_implementing_countries' => 'right',
      'field_project_sites_3' => 'bottom_left',
      'field_actions' => 'top_left',
      'field_implementing_agency' => 'right',
      'field_donor_enity' => 'right',
      'field_project_total_funding' => 'right',
      'field_related_documents_entity' => 'bottom_left',
      'field_amount_donor_currency_' => 'right',
      'field_related_projects_entity' => 'right',
      'field_pifacc_themes' => 'right',
      'field_topics' => 'right',
      'field_tags' => 'right',
      'related_news' => 'right',
      'field_project_outcomes' => 'top_left',
      'field_the_description' => 'top_left',
      'field_file_s_' => 'top_left',
      'group_description' => 'top_left',
      'group_objectives' => 'top_left',
      'group_outcomes' => 'top_left',
      'group_outputs' => 'top_left',
      'group_related_content' => 'right',
      'group_funding' => 'right',
      'group_project_information' => 'right',
      'group_related_documents' => 'bottom_left',
      'group_project_sites' => 'bottom_left',
    ),
    'classes' => array(),
    'wrappers' => array(
      'top' => 'div',
      'top_left' => 'div',
      'top_middle' => 'div',
      'bottom_left' => 'div',
      'bottom_middle' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['node|product|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|product|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'product';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'group_project',
        1 => 'path',
        6 => 'field_focus_area',
        7 => 'field_pifacc_themes',
        8 => 'field_hfa_priorities',
        9 => 'field_rfa_priorities',
        10 => 'field_tags',
        11 => 'field_short_title',
        12 => 'field_national_objective',
        13 => 'title',
        14 => 'field_project_name',
        15 => 'field_project_status',
        16 => 'field_the_description',
        17 => 'group_general',
        18 => 'field_objectives',
        19 => 'group_categorisation',
        20 => 'group_donor',
        21 => 'field_project_outcomes',
        22 => 'field_outputs',
        23 => 'group_other',
        24 => 'field_activities',
        25 => 'group_sites',
        26 => 'field_project_scope',
        27 => 'group_relations',
        28 => 'field_topics',
        29 => 'field_project_type',
        30 => 'field_start_date',
        31 => 'field_national_framework',
        32 => 'field_end_date',
        33 => 'field_actions',
        34 => 'field_file_s_',
        35 => 'field_project_comments',
        36 => 'field_implementing_countries',
        37 => 'field_related_documents_entity',
        38 => 'field_related_projects_entity',
        39 => 'field_related_links_entity',
        40 => 'field_project_sites_3',
        41 => 'field_donor_enity',
        42 => 'field_project_total_funding',
        43 => 'field_amount_donor_currency_',
        44 => 'field_implementing_agency',
        45 => 'field_organisation_entity',
        46 => 'field_project_contact',
      ),
      'hidden' => array(
        2 => 'field_duration',
        3 => 'field_verified_pccp',
        4 => 'field_old_drupal_id',
        5 => 'field_imported_project',
        47 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'group_project' => 'ds_content',
      'path' => 'ds_content',
      'field_duration' => 'hidden',
      'field_verified_pccp' => 'hidden',
      'field_old_drupal_id' => 'hidden',
      'field_imported_project' => 'hidden',
      'field_focus_area' => 'ds_content',
      'field_pifacc_themes' => 'ds_content',
      'field_hfa_priorities' => 'ds_content',
      'field_rfa_priorities' => 'ds_content',
      'field_tags' => 'ds_content',
      'field_short_title' => 'ds_content',
      'field_national_objective' => 'ds_content',
      'title' => 'ds_content',
      'field_project_name' => 'ds_content',
      'field_project_status' => 'ds_content',
      'field_the_description' => 'ds_content',
      'group_general' => 'ds_content',
      'field_objectives' => 'ds_content',
      'group_categorisation' => 'ds_content',
      'group_donor' => 'ds_content',
      'field_project_outcomes' => 'ds_content',
      'field_outputs' => 'ds_content',
      'group_other' => 'ds_content',
      'field_activities' => 'ds_content',
      'group_sites' => 'ds_content',
      'field_project_scope' => 'ds_content',
      'group_relations' => 'ds_content',
      'field_topics' => 'ds_content',
      'field_project_type' => 'ds_content',
      'field_start_date' => 'ds_content',
      'field_national_framework' => 'ds_content',
      'field_end_date' => 'ds_content',
      'field_actions' => 'ds_content',
      'field_file_s_' => 'ds_content',
      'field_project_comments' => 'ds_content',
      'field_implementing_countries' => 'ds_content',
      'field_related_documents_entity' => 'ds_content',
      'field_related_projects_entity' => 'ds_content',
      'field_related_links_entity' => 'ds_content',
      'field_project_sites_3' => 'ds_content',
      'field_donor_enity' => 'ds_content',
      'field_project_total_funding' => 'ds_content',
      'field_amount_donor_currency_' => 'ds_content',
      'field_implementing_agency' => 'ds_content',
      'field_organisation_entity' => 'ds_content',
      'field_project_contact' => 'ds_content',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|product|form'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|product|search_index';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'product';
  $ds_layout->view_mode = 'search_index';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_project_status',
        2 => 'field_project_name',
        3 => 'field_related_links_entity',
        4 => 'field_project_scope',
        5 => 'field_project_type',
        6 => 'field_objectives',
        7 => 'field_project_contact',
        8 => 'field_organisation_entity',
        9 => 'field_focus_area',
        10 => 'field_national_framework',
        11 => 'field_hfa_priorities',
        12 => 'field_rfa_priorities',
        13 => 'field_start_date',
        14 => 'field_end_date',
        15 => 'field_outputs',
        16 => 'field_activities',
        17 => 'field_implementing_countries',
        18 => 'field_actions',
        19 => 'field_implementing_agency',
        20 => 'field_project_sites_3',
        21 => 'field_donor_enity',
        22 => 'field_related_documents_entity',
        23 => 'field_project_total_funding',
        24 => 'field_amount_donor_currency_',
        25 => 'field_related_projects_entity',
        26 => 'field_pifacc_themes',
        27 => 'field_topics',
        28 => 'field_tags',
        29 => 'field_project_outcomes',
        30 => 'field_the_description',
        31 => 'field_file_s_',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_project_status' => 'ds_content',
      'field_project_name' => 'ds_content',
      'field_related_links_entity' => 'ds_content',
      'field_project_scope' => 'ds_content',
      'field_project_type' => 'ds_content',
      'field_objectives' => 'ds_content',
      'field_project_contact' => 'ds_content',
      'field_organisation_entity' => 'ds_content',
      'field_focus_area' => 'ds_content',
      'field_national_framework' => 'ds_content',
      'field_hfa_priorities' => 'ds_content',
      'field_rfa_priorities' => 'ds_content',
      'field_start_date' => 'ds_content',
      'field_end_date' => 'ds_content',
      'field_outputs' => 'ds_content',
      'field_activities' => 'ds_content',
      'field_implementing_countries' => 'ds_content',
      'field_actions' => 'ds_content',
      'field_implementing_agency' => 'ds_content',
      'field_project_sites_3' => 'ds_content',
      'field_donor_enity' => 'ds_content',
      'field_related_documents_entity' => 'ds_content',
      'field_project_total_funding' => 'ds_content',
      'field_amount_donor_currency_' => 'ds_content',
      'field_related_projects_entity' => 'ds_content',
      'field_pifacc_themes' => 'ds_content',
      'field_topics' => 'ds_content',
      'field_tags' => 'ds_content',
      'field_project_outcomes' => 'ds_content',
      'field_the_description' => 'ds_content',
      'field_file_s_' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|product|search_index'] = $ds_layout;

  return $export;
}
