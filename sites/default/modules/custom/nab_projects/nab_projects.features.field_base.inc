<?php
/**
 * @file
 * nab_projects.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function nab_projects_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_actions'.
  $field_bases['field_actions'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_actions',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_long',
  );

  // Exported field_base: 'field_activities'.
  $field_bases['field_activities'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_activities',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_long',
  );

  // Exported field_base: 'field_donor_enity'.
  $field_bases['field_donor_enity'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_donor_enity',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'donor' => 'donor',
        ),
      ),
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_duration'.
  $field_bases['field_duration'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_duration',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_end_date'.
  $field_bases['field_end_date'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_end_date',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'date',
    'settings' => array(
      'cache_count' => 4,
      'cache_enabled' => 0,
      'granularity' => array(
        'day' => 'day',
        'hour' => 0,
        'minute' => 0,
        'month' => 'month',
        'second' => 0,
        'year' => 'year',
      ),
      'timezone_db' => '',
      'todate' => '',
      'tz_handling' => 'none',
    ),
    'translatable' => 0,
    'type' => 'date',
  );

  // Exported field_base: 'field_focus_area'.
  $field_bases['field_focus_area'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_focus_area',
    'indexes' => array(
      'tid' => array(
        0 => 'tid',
      ),
    ),
    'locked' => 0,
    'module' => 'taxonomy',
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => 'focus_area',
          'parent' => 0,
        ),
      ),
    ),
    'translatable' => 0,
    'type' => 'taxonomy_term_reference',
  );

  // Exported field_base: 'field_framework_country'.
  $field_bases['field_framework_country'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_framework_country',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'direction' => 'ASC',
          'property' => 'title',
          'type' => 'property',
        ),
        'target_bundles' => array(
          'country' => 'country',
        ),
      ),
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_implementing_agency'.
  $field_bases['field_implementing_agency'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_implementing_agency',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'organisation' => 'organisation',
        ),
      ),
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_implementing_countries'.
  $field_bases['field_implementing_countries'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_implementing_countries',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'country' => 'country',
        ),
      ),
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_imported_project'.
  $field_bases['field_imported_project'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_imported_project',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        0 => '',
        1 => '',
      ),
      'allowed_values_function' => '',
      'allowed_values_php' => '',
    ),
    'translatable' => 0,
    'type' => 'list_boolean',
  );

  // Exported field_base: 'field_iso_code'.
  $field_bases['field_iso_code'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_iso_code',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_location_latlon'.
  $field_bases['field_location_latlon'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_location_latlon',
    'indexes' => array(
      'bbox' => array(
        0 => 'top',
        1 => 'bottom',
        2 => 'left',
        3 => 'right',
      ),
      'bottom' => array(
        0 => 'bottom',
      ),
      'centroid' => array(
        0 => 'lat',
        1 => 'lon',
      ),
      'geohash' => array(
        0 => 'geohash',
      ),
      'lat' => array(
        0 => 'lat',
      ),
      'left' => array(
        0 => 'left',
      ),
      'lon' => array(
        0 => 'lon',
      ),
      'right' => array(
        0 => 'right',
      ),
      'top' => array(
        0 => 'top',
      ),
    ),
    'locked' => 0,
    'module' => 'geofield',
    'settings' => array(
      'backend' => 'default',
      'srid' => 4326,
    ),
    'translatable' => 0,
    'type' => 'geofield',
  );

  // Exported field_base: 'field_national_framework'.
  $field_bases['field_national_framework'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_national_framework',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_long',
  );

  // Exported field_base: 'field_objectives'.
  $field_bases['field_objectives'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_objectives',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_long',
  );

  // Exported field_base: 'field_old_drupal_id'.
  $field_bases['field_old_drupal_id'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_old_drupal_id',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_organisation_entity'.
  $field_bases['field_organisation_entity'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_organisation_entity',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'organisation' => 'organisation',
        ),
      ),
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_outputs'.
  $field_bases['field_outputs'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_outputs',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_long',
  );

  // Exported field_base: 'field_pifacc_themes'.
  $field_bases['field_pifacc_themes'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_pifacc_themes',
    'indexes' => array(
      'tid' => array(
        0 => 'tid',
      ),
    ),
    'locked' => 0,
    'module' => 'taxonomy',
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => 'pifacc27_95901',
          'parent' => 0,
        ),
      ),
    ),
    'translatable' => 0,
    'type' => 'taxonomy_term_reference',
  );

  // Exported field_base: 'field_project_comments'.
  $field_bases['field_project_comments'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_project_comments',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_with_summary',
  );

  // Exported field_base: 'field_project_contact'.
  $field_bases['field_project_contact'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_project_contact',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'contact' => 'contact',
        ),
      ),
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_project_name'.
  $field_bases['field_project_name'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_project_name',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_with_summary',
  );

  // Exported field_base: 'field_project_outcomes'.
  $field_bases['field_project_outcomes'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_project_outcomes',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_with_summary',
  );

  // Exported field_base: 'field_project_scope'.
  $field_bases['field_project_scope'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_project_scope',
    'indexes' => array(
      'tid' => array(
        0 => 'tid',
      ),
    ),
    'locked' => 0,
    'module' => 'taxonomy',
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => 'project_scope',
          'parent' => 0,
        ),
      ),
    ),
    'translatable' => 0,
    'type' => 'taxonomy_term_reference',
  );

  // Exported field_base: 'field_project_sites_3'.
  $field_bases['field_project_sites_3'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_project_sites_3',
    'indexes' => array(
      'glid' => array(
        0 => 'glid',
      ),
    ),
    'locked' => 0,
    'module' => 'getlocations_fields',
    'settings' => array(
      'autocomplete_bias' => 0,
      'baselayers' => array(
        'DeLorme' => 0,
        'ESRI' => 0,
        'Hybrid' => 1,
        'Map' => 1,
        'NatGeoWorldMap' => 0,
        'OCM' => 0,
        'OCML' => 0,
        'OCMO' => 0,
        'OCMT' => 0,
        'OSM' => 1,
        'OSMBW' => 0,
        'OSMDE' => 0,
        'OceanBasemap' => 0,
        'Physical' => 1,
        'Satellite' => 1,
        'Stamen' => 0,
        'StamenBG' => 0,
        'StamenHY' => 0,
        'StamenLA' => 0,
        'StamenLI' => 0,
        'StamenLT' => 0,
        'Watercolor' => 0,
        'WorldGrayCanvas' => 0,
        'WorldImagery' => 0,
        'WorldPhysical' => 0,
        'WorldShadedRelief' => 0,
        'WorldTerrain' => 0,
        'WorldTopoMap' => 0,
      ),
      'circles_apply' => 0,
      'circles_clickable' => 0,
      'circles_coords' => '',
      'circles_enable' => 0,
      'circles_fillcolor' => '#FF0000',
      'circles_fillopacity' => 0.35,
      'circles_message' => '',
      'circles_radius' => 0,
      'circles_strokecolor' => '#FF0000',
      'circles_strokeopacity' => 0.8,
      'circles_strokeweight' => 3,
      'city_autocomplete' => 0,
      'comment_map_marker' => 'drupal',
      'controltype' => 'small',
      'country' => 'FJ',
      'draggable' => 1,
      'fullscreen' => 0,
      'fullscreen_controlposition' => '',
      'geocoder_enable' => 0,
      'geojson_data' => '',
      'geojson_enable' => 0,
      'geojson_options' => '',
      'getdirections_link' => 0,
      'gps_bubble' => 0,
      'gps_button' => 0,
      'gps_button_label' => '',
      'gps_center' => 0,
      'gps_geocode' => 0,
      'gps_marker' => '',
      'gps_marker_title' => '',
      'gps_type' => 0,
      'gps_zoom' => -1,
      'highlight_enable' => 0,
      'highlight_fillcolor' => '#FF0000',
      'highlight_fillopacity' => 0.35,
      'highlight_radius' => 10,
      'highlight_strokecolor' => '#FF0000',
      'highlight_strokeopacity' => 0.8,
      'highlight_strokeweight' => 3,
      'input_additional_required' => 4,
      'input_additional_weight' => 0,
      'input_additional_width' => 40,
      'input_address_width' => 40,
      'input_city_required' => 0,
      'input_city_weight' => 0,
      'input_city_width' => 40,
      'input_clear_button_weight' => 0,
      'input_country_required' => 0,
      'input_country_weight' => 0,
      'input_country_width' => 40,
      'input_fax_required' => 4,
      'input_fax_weight' => 0,
      'input_fax_width' => 20,
      'input_geobutton_weight' => 0,
      'input_geolocation_button_weight' => 0,
      'input_latitude_weight' => 0,
      'input_latitude_width' => 20,
      'input_longitude_weight' => 0,
      'input_longitude_width' => 20,
      'input_map_marker' => 'lblue',
      'input_map_show' => 1,
      'input_map_weight' => 0,
      'input_marker_weight' => 0,
      'input_mobile_required' => 4,
      'input_mobile_weight' => 0,
      'input_mobile_width' => 20,
      'input_name_required' => 0,
      'input_name_weight' => 0,
      'input_name_width' => 40,
      'input_phone_required' => 4,
      'input_phone_weight' => 0,
      'input_phone_width' => 20,
      'input_postal_code_required' => 4,
      'input_postal_code_weight' => 0,
      'input_postal_code_width' => 40,
      'input_province_required' => 4,
      'input_province_weight' => 0,
      'input_province_width' => 40,
      'input_smart_ip_button_weight' => 0,
      'input_street_required' => 4,
      'input_street_weight' => 0,
      'input_street_width' => 40,
      'jquery_colorpicker_enabled' => 0,
      'kml_group' => array(
        'kml_url' => '',
        'kml_url_button' => 0,
        'kml_url_button_label' => 'Kml Layer',
        'kml_url_button_state' => 0,
        'kml_url_click' => 1,
        'kml_url_infowindow' => 0,
        'kml_url_viewport' => 1,
      ),
      'latlon_warning' => 0,
      'latlong' => '40,0',
      'map_backgroundcolor' => '',
      'map_marker' => 'drupal',
      'map_settings_allow' => 0,
      'mapcontrolposition' => '',
      'mapheight' => '200px',
      'maptype' => 'Map',
      'mapwidth' => '300px',
      'maxzoom_map' => -1,
      'minzoom_map' => -1,
      'mtc' => 'standard',
      'node_map_marker' => 'drupal',
      'nodoubleclickzoom' => 0,
      'nokeyboard' => 0,
      'only_continents' => '',
      'only_countries' => '',
      'overview' => 1,
      'overview_opened' => 1,
      'pancontrol' => 1,
      'pancontrolposition' => '',
      'per_item_marker' => 0,
      'polygons_clickable' => 0,
      'polygons_coords' => '',
      'polygons_enable' => 0,
      'polygons_fillcolor' => '#FF0000',
      'polygons_fillopacity' => 0.35,
      'polygons_message' => '',
      'polygons_strokecolor' => '#FF0000',
      'polygons_strokeopacity' => 0.8,
      'polygons_strokeweight' => 3,
      'polylines_clickable' => 0,
      'polylines_coords' => '',
      'polylines_enable' => 0,
      'polylines_message' => '',
      'polylines_strokecolor' => '#FF0000',
      'polylines_strokeopacity' => 0.8,
      'polylines_strokeweight' => 3,
      'province_autocomplete' => 0,
      'rectangles_apply' => 0,
      'rectangles_clickable' => 0,
      'rectangles_coords' => '',
      'rectangles_dist' => 0,
      'rectangles_enable' => 0,
      'rectangles_fillcolor' => '#FF0000',
      'rectangles_fillopacity' => 0.35,
      'rectangles_message' => '',
      'rectangles_strokecolor' => '#FF0000',
      'rectangles_strokeopacity' => 0.8,
      'rectangles_strokeweight' => 3,
      'restrict_by_country' => 0,
      'scale' => 0,
      'scalecontrolposition' => '',
      'scrollwheel' => 0,
      'search_country' => 'WS',
      'search_places' => 0,
      'search_places_dd' => 0,
      'search_places_label' => 'Google Places Search',
      'search_places_list' => 0,
      'search_places_placeholder' => '',
      'search_places_position' => 'outside_above',
      'search_places_size' => 40,
      'show_bubble_on_one_marker' => 0,
      'show_maplinks' => 0,
      'show_search_distance' => 0,
      'street_num_pos' => 1,
      'streetview_settings_allow' => 0,
      'sv_addresscontrol' => 1,
      'sv_addresscontrolposition' => '',
      'sv_clicktogo' => 1,
      'sv_enable' => 0,
      'sv_heading' => 0,
      'sv_imagedatecontrol' => 0,
      'sv_linkscontrol' => 1,
      'sv_pancontrol' => 1,
      'sv_pancontrolposition' => '',
      'sv_pitch' => 0,
      'sv_scrollwheel' => 1,
      'sv_showfirst' => 0,
      'sv_zoom' => 1,
      'sv_zoomcontrol' => 'default',
      'sv_zoomcontrolposition' => '',
      'svcontrolposition' => '',
      'use_address' => 2,
      'use_clear_button' => 1,
      'use_country_dropdown' => 1,
      'use_geolocation_button' => 0,
      'use_smart_ip_button' => 0,
      'use_smart_ip_latlon' => 0,
      'user_map_marker' => 'drupal',
      'views_search_center' => 0,
      'views_search_marker' => 'drupal',
      'views_search_marker_enable' => 0,
      'views_search_marker_toggle' => 0,
      'views_search_marker_toggle_active' => 0,
      'views_search_radshape_enable' => 0,
      'views_search_radshape_fillcolor' => '#FF0000',
      'views_search_radshape_fillopacity' => 0.35,
      'views_search_radshape_strokecolor' => '#FF0000',
      'views_search_radshape_strokeopacity' => 0.8,
      'views_search_radshape_strokeweight' => 3,
      'views_search_radshape_toggle' => 0,
      'views_search_radshape_toggle_active' => 0,
      'vocabulary_map_marker' => 'drupal',
      'what3words_collect' => 0,
      'zoom' => 3,
      'zoomcontrolposition' => '',
    ),
    'translatable' => 0,
    'type' => 'getlocations_fields',
  );

  // Exported field_base: 'field_project_status'.
  $field_bases['field_project_status'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_project_status',
    'indexes' => array(
      'tid' => array(
        0 => 'tid',
      ),
    ),
    'locked' => 0,
    'module' => 'taxonomy',
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => 'project_status',
          'parent' => 0,
        ),
      ),
    ),
    'translatable' => 0,
    'type' => 'taxonomy_term_reference',
  );

  // Exported field_base: 'field_project_total_funding'.
  $field_bases['field_project_total_funding'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_project_total_funding',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'number',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'number_integer',
  );

  // Exported field_base: 'field_project_type'.
  $field_bases['field_project_type'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_project_type',
    'indexes' => array(
      'tid' => array(
        0 => 'tid',
      ),
    ),
    'locked' => 0,
    'module' => 'taxonomy',
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => 'project_type',
          'parent' => 0,
        ),
      ),
    ),
    'translatable' => 0,
    'type' => 'taxonomy_term_reference',
  );

  // Exported field_base: 'field_related_documents_entity'.
  $field_bases['field_related_documents_entity'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_related_documents_entity',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'document' => 'document',
        ),
      ),
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_related_links_entity'.
  $field_bases['field_related_links_entity'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_related_links_entity',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'link' => 'link',
        ),
      ),
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_related_projects_entity'.
  $field_bases['field_related_projects_entity'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_related_projects_entity',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'product' => 'product',
        ),
      ),
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_short_title'.
  $field_bases['field_short_title'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_short_title',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_start_date'.
  $field_bases['field_start_date'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_start_date',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'date',
    'settings' => array(
      'cache_count' => 4,
      'cache_enabled' => 0,
      'granularity' => array(
        'day' => 'day',
        'hour' => 0,
        'minute' => 0,
        'month' => 'month',
        'second' => 0,
        'year' => 'year',
      ),
      'timezone_db' => '',
      'todate' => '',
      'tz_handling' => 'none',
    ),
    'translatable' => 0,
    'type' => 'date',
  );

  // Exported field_base: 'field_tags'.
  $field_bases['field_tags'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_tags',
    'indexes' => array(
      'tid' => array(
        0 => 'tid',
      ),
    ),
    'locked' => 0,
    'module' => 'taxonomy',
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => 'tags',
          'parent' => 0,
        ),
      ),
    ),
    'translatable' => 0,
    'type' => 'taxonomy_term_reference',
  );

  // Exported field_base: 'field_the_description'.
  $field_bases['field_the_description'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_the_description',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_with_summary',
  );

  // Exported field_base: 'field_verified_pccp'.
  $field_bases['field_verified_pccp'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_verified_pccp',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        0 => '',
        1 => '',
      ),
      'allowed_values_function' => '',
      'allowed_values_php' => '',
    ),
    'translatable' => 0,
    'type' => 'list_boolean',
  );

  return $field_bases;
}
