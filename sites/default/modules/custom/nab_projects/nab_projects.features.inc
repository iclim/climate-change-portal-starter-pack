<?php
/**
 * @file
 * nab_projects.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function nab_projects_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "custom_formatters" && $api == "custom_formatters") {
    return array("version" => "2");
  }
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function nab_projects_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function nab_projects_node_info() {
  $items = array(
    'country' => array(
      'name' => t('Country'),
      'base' => 'node_content',
      'description' => t('Country Content Type'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
    'donor' => array(
      'name' => t('Donor'),
      'base' => 'node_content',
      'description' => t('Donor Content Type with related Fund'),
      'has_title' => '1',
      'title_label' => t('Fund Name'),
      'help' => '',
    ),
    'fund' => array(
      'name' => t('Fund'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'funding_opportunity' => array(
      'name' => t('Funding opportunity'),
      'base' => 'node_content',
      'description' => t('Identified opportunity to receive climate related project funding.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'link' => array(
      'name' => t('Link'),
      'base' => 'node_content',
      'description' => t('Link Content Type'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
