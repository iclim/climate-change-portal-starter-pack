<?php
/**
 * @file
 * Code for the NAB Projects feature.
 */

include_once 'nab_projects.features.inc';

/**
 * Implements hook_entity_property_info_alter().
 */
function nab_projects_entity_property_info_alter(&$info) {
  // We expose coordinates as properties to get Views and Search API integration
  // for free. The Getlocations Fields module does not integrate out of the box.
  $properties = &$info['node']['bundles']['product']['properties'];
  $properties['latitude'] = array(
    'label' => 'Project Latitude',
    'description' => 'Project location latitude (from field_project_sites_3)',
    'type' => 'list<text>',
    'getter callback' => '_nab_projects_get_latitude',
    'computed' => TRUE,
    'entity views field' => TRUE,
  );
  $properties['longitude'] = array(
    'label' => 'Project Longitude',
    'description' => 'Project location longitude (from field_project_sites_3)',
    'type' => 'list<text>',
    'getter callback' => '_nab_projects_get_longitude',
    'computed' => TRUE,
    'entity views field' => TRUE,
  );
}

/**
 * Entity property 'getter callback' for 'latitude' property.
 */
function _nab_projects_get_latitude($node) {
  $latitude = array();
  if (!empty($node->field_project_sites_3[LANGUAGE_NONE])) {
    foreach ($node->field_project_sites_3[LANGUAGE_NONE] as $delta => $item) {
      $latitude[$delta] = $item['latitude'];
    }
  }
  return $latitude;
}

/**
 * Entity property 'getter callback' for 'longitude' property.
 */
function _nab_projects_get_longitude($node) {
  $longitude = array();
  if (!empty($node->field_project_sites_3[LANGUAGE_NONE])) {
    foreach ($node->field_project_sites_3[LANGUAGE_NONE] as $delta => $item) {
      $longitude[$delta] = $item['longitude'];
    }
  }
  return $longitude;
}

/**
 * Implementation of hook_preprocess_menu_local_task
 * @param type $vars
 */
function nab_projects_preprocess_menu_local_task(&$vars) {

  //Keep the query parameters on the local tasks when on the project search area
  if ($vars['element']['#link']['tab_root'] == 'projects') {
    $query_params = drupal_get_query_parameters();
    $vars['element']['#link']['localized_options']['query'] = $query_params;
  }
}
