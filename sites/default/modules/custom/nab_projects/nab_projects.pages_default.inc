<?php
/**
 * @file
 * nab_projects.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function nab_projects_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'project_search';
  $page->task = 'page';
  $page->admin_title = 'Search for projects';
  $page->admin_description = '';
  $page->path = 'projects/list';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'default tab',
    'title' => 'List',
    'name' => 'menu-projects',
    'weight' => '0',
    'parent' => array(
      'type' => 'normal',
      'title' => 'Search for projects',
      'name' => 'menu-projects',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_project_search__panel_context_3e8a29b8-c7fd-4a09-8f54-cb475ba86b3e';
  $handler->task = 'page';
  $handler->subtask = 'project_search';
  $handler->handler = 'panel_context';
  $handler->weight = 1;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 1,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '0',
          ),
          'context' => 'argument_string_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'name' => '',
    'panels_page_title_state' => 1,
    'panels_page_title' => 'Project Search',
  );
  $display = new panels_display();
  $display->layout = 'nab_searchpages';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'middle' => NULL,
      'right' => NULL,
      'bottom' => NULL,
      'left' => NULL,
      'header' => NULL,
      'top_left' => NULL,
      'top_middle' => NULL,
      'top_right' => NULL,
      'bottom_left' => NULL,
      'bottom_middle' => NULL,
      'bottom_right' => NULL,
    ),
    'left' => array(
      'style' => 'default',
    ),
  );
  $display->cache = array();
  $display->title = 'Project Search';
  $display->uuid = '2e5a4d67-05f9-4a47-8498-41a2631b768e';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_project_search__panel_context_4b91fc25-b0ad-43f7-a7f4-8b9ab16d4491';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-20da722b-7bc1-4ebb-bf79-f03e20d2f06f';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'views--exp-project_search-page';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'inherit_path' => 1,
    'override_title' => 1,
    'override_title_text' => 'Keyword Search',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '20da722b-7bc1-4ebb-bf79-f03e20d2f06f';
  $display->content['new-20da722b-7bc1-4ebb-bf79-f03e20d2f06f'] = $pane;
  $display->panels['left'][0] = 'new-20da722b-7bc1-4ebb-bf79-f03e20d2f06f';
  $pane = new stdClass();
  $pane->pid = 'new-7788f1a1-e787-4e0f-b484-2c16b5f564d7';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'current_search-pccp_current_search';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Active Filters',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '7788f1a1-e787-4e0f-b484-2c16b5f564d7';
  $display->content['new-7788f1a1-e787-4e0f-b484-2c16b5f564d7'] = $pane;
  $display->panels['left'][1] = 'new-7788f1a1-e787-4e0f-b484-2c16b5f564d7';
  $pane = new stdClass();
  $pane->pid = 'new-313d44a6-8c44-4d85-a40f-ec218e609bac';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-crqI031cl6jxlwqt0YlxGuWNuR2EDLyw';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Topics',
    'override_title_heading' => 'h4',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'panel-topics',
    'css_class' => '',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '313d44a6-8c44-4d85-a40f-ec218e609bac';
  $display->content['new-313d44a6-8c44-4d85-a40f-ec218e609bac'] = $pane;
  $display->panels['left'][2] = 'new-313d44a6-8c44-4d85-a40f-ec218e609bac';
  $pane = new stdClass();
  $pane->pid = 'new-dafbdb80-e6d1-418a-b1c7-b11b8851275f';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-uSv8YSjPfWRrj5Q4x08P6g3FZLAVBIKg';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Project Status',
    'override_title_heading' => 'h4',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = 'dafbdb80-e6d1-418a-b1c7-b11b8851275f';
  $display->content['new-dafbdb80-e6d1-418a-b1c7-b11b8851275f'] = $pane;
  $display->panels['left'][3] = 'new-dafbdb80-e6d1-418a-b1c7-b11b8851275f';
  $pane = new stdClass();
  $pane->pid = 'new-73478ac0-4b26-4cfc-a467-de8477439bcb';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-pQqe2qp8eLGT01GmeN5uITBOSDlMABU8';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = '73478ac0-4b26-4cfc-a467-de8477439bcb';
  $display->content['new-73478ac0-4b26-4cfc-a467-de8477439bcb'] = $pane;
  $display->panels['left'][4] = 'new-73478ac0-4b26-4cfc-a467-de8477439bcb';
  $pane = new stdClass();
  $pane->pid = 'new-42fa8d40-39d2-4f7f-beac-000bb9edbf5d';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-pSfVNYxz0AY5ntCaxJ0DUR0G9LWqhQmL';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Project Type',
    'override_title_heading' => 'h4',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 5;
  $pane->locks = array();
  $pane->uuid = '42fa8d40-39d2-4f7f-beac-000bb9edbf5d';
  $display->content['new-42fa8d40-39d2-4f7f-beac-000bb9edbf5d'] = $pane;
  $display->panels['left'][5] = 'new-42fa8d40-39d2-4f7f-beac-000bb9edbf5d';
  $pane = new stdClass();
  $pane->pid = 'new-4ba10bd1-e853-4703-abbb-922a3970293c';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-ELwPJqMJeQI0xzGvy2ZlsFPcc0FgIl3v';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Implementing Countries',
    'override_title_heading' => 'h4',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 6;
  $pane->locks = array();
  $pane->uuid = '4ba10bd1-e853-4703-abbb-922a3970293c';
  $display->content['new-4ba10bd1-e853-4703-abbb-922a3970293c'] = $pane;
  $display->panels['left'][6] = 'new-4ba10bd1-e853-4703-abbb-922a3970293c';
  $pane = new stdClass();
  $pane->pid = 'new-ce870cb2-d319-46fa-8ee7-bd0b41610da3';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-llQxJfi4pzmOrl0dPgF2PpUctBDAGvyr';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Organisation',
    'override_title_heading' => 'h4',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 7;
  $pane->locks = array();
  $pane->uuid = 'ce870cb2-d319-46fa-8ee7-bd0b41610da3';
  $display->content['new-ce870cb2-d319-46fa-8ee7-bd0b41610da3'] = $pane;
  $display->panels['left'][7] = 'new-ce870cb2-d319-46fa-8ee7-bd0b41610da3';
  $pane = new stdClass();
  $pane->pid = 'new-3fe93b73-f5c0-4c40-b3b0-ec00ee7e5e80';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-Kz8LHZCBeQycBCKuVr3eQURHVEUixGiA';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Implementing Agency',
    'override_title_heading' => 'h4',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 8;
  $pane->locks = array();
  $pane->uuid = '3fe93b73-f5c0-4c40-b3b0-ec00ee7e5e80';
  $display->content['new-3fe93b73-f5c0-4c40-b3b0-ec00ee7e5e80'] = $pane;
  $display->panels['left'][8] = 'new-3fe93b73-f5c0-4c40-b3b0-ec00ee7e5e80';
  $pane = new stdClass();
  $pane->pid = 'new-1b741263-0365-487e-95c1-bec16d3d11ee';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-zMezFTj7rksp0qu6al0d9iKrO8l1xet2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Donor',
    'override_title_heading' => 'h4',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 9;
  $pane->locks = array();
  $pane->uuid = '1b741263-0365-487e-95c1-bec16d3d11ee';
  $display->content['new-1b741263-0365-487e-95c1-bec16d3d11ee'] = $pane;
  $display->panels['left'][9] = 'new-1b741263-0365-487e-95c1-bec16d3d11ee';
  $pane = new stdClass();
  $pane->pid = 'new-c45c78a0-f416-4f2f-89c6-0a3ed45616dd';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-luI0jh0x9FtV8diaDNwNUcMEMYiYzvyJ';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Tags',
    'override_title_heading' => 'h4',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 10;
  $pane->locks = array();
  $pane->uuid = 'c45c78a0-f416-4f2f-89c6-0a3ed45616dd';
  $display->content['new-c45c78a0-f416-4f2f-89c6-0a3ed45616dd'] = $pane;
  $display->panels['left'][10] = 'new-c45c78a0-f416-4f2f-89c6-0a3ed45616dd';
  $pane = new stdClass();
  $pane->pid = 'new-d1712340-222c-4e80-b32e-00296a0897d3';
  $pane->panel = 'right';
  $pane->type = 'block';
  $pane->subtype = 'current_search-current_search_results_info';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => 'Results',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd1712340-222c-4e80-b32e-00296a0897d3';
  $display->content['new-d1712340-222c-4e80-b32e-00296a0897d3'] = $pane;
  $display->panels['right'][0] = 'new-d1712340-222c-4e80-b32e-00296a0897d3';
  $pane = new stdClass();
  $pane->pid = 'new-f09e895c-c804-4e42-8b37-45fcdd5aa2c0';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'project_search-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'f09e895c-c804-4e42-8b37-45fcdd5aa2c0';
  $display->content['new-f09e895c-c804-4e42-8b37-45fcdd5aa2c0'] = $pane;
  $display->panels['right'][1] = 'new-f09e895c-c804-4e42-8b37-45fcdd5aa2c0';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['project_search'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'search_for_projects_map';
  $page->task = 'page';
  $page->admin_title = 'Search for projects';
  $page->admin_description = '';
  $page->path = 'projects/map';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'tab',
    'title' => 'Map',
    'name' => 'menu-projects',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => 'Projects list',
      'name' => 'menu-projects',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_search_for_projects_map__panel_context_07381043-2184-4cf4-874c-13ccf885d145';
  $handler->task = 'page';
  $handler->subtask = 'search_for_projects_map';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 1,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '0',
          ),
          'context' => 'argument_string_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'name' => '',
    'panels_page_title_state' => 1,
    'panels_page_title' => 'Project search',
  );
  $display = new panels_display();
  $display->layout = 'nab_searchpages';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'middle' => NULL,
      'right' => NULL,
      'bottom' => NULL,
      'left' => NULL,
      'header' => NULL,
      'top_left' => NULL,
      'top_middle' => NULL,
      'top_right' => NULL,
      'bottom_left' => NULL,
      'bottom_middle' => NULL,
      'bottom_right' => NULL,
    ),
    'left' => array(
      'style' => 'default',
    ),
  );
  $display->cache = array();
  $display->title = 'Project Search';
  $display->uuid = '2e5a4d67-05f9-4a47-8498-41a2631b768e';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_project_search__panel_context_4b91fc25-b0ad-43f7-a7f4-8b9ab16d4491';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-20da722b-7bc1-4ebb-bf79-f03e20d2f06f';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'views--exp-project_search-page';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'inherit_path' => 1,
    'override_title' => 1,
    'override_title_text' => 'Keyword Search',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '20da722b-7bc1-4ebb-bf79-f03e20d2f06f';
  $display->content['new-20da722b-7bc1-4ebb-bf79-f03e20d2f06f'] = $pane;
  $display->panels['left'][0] = 'new-20da722b-7bc1-4ebb-bf79-f03e20d2f06f';
  $pane = new stdClass();
  $pane->pid = 'new-313d44a6-8c44-4d85-a40f-ec218e609bac';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-crqI031cl6jxlwqt0YlxGuWNuR2EDLyw';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Topics',
    'override_title_heading' => 'h4',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'panel-topics',
    'css_class' => '',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '313d44a6-8c44-4d85-a40f-ec218e609bac';
  $display->content['new-313d44a6-8c44-4d85-a40f-ec218e609bac'] = $pane;
  $display->panels['left'][1] = 'new-313d44a6-8c44-4d85-a40f-ec218e609bac';
  $pane = new stdClass();
  $pane->pid = 'new-7788f1a1-e787-4e0f-b484-2c16b5f564d7';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'current_search-pccp_current_search';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Active Filters',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '7788f1a1-e787-4e0f-b484-2c16b5f564d7';
  $display->content['new-7788f1a1-e787-4e0f-b484-2c16b5f564d7'] = $pane;
  $display->panels['left'][2] = 'new-7788f1a1-e787-4e0f-b484-2c16b5f564d7';
  $pane = new stdClass();
  $pane->pid = 'new-dafbdb80-e6d1-418a-b1c7-b11b8851275f';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-uSv8YSjPfWRrj5Q4x08P6g3FZLAVBIKg';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Project Status',
    'override_title_heading' => 'h4',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = 'dafbdb80-e6d1-418a-b1c7-b11b8851275f';
  $display->content['new-dafbdb80-e6d1-418a-b1c7-b11b8851275f'] = $pane;
  $display->panels['left'][3] = 'new-dafbdb80-e6d1-418a-b1c7-b11b8851275f';
  $pane = new stdClass();
  $pane->pid = 'new-42fa8d40-39d2-4f7f-beac-000bb9edbf5d';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-pSfVNYxz0AY5ntCaxJ0DUR0G9LWqhQmL';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Project Type',
    'override_title_heading' => 'h4',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = '42fa8d40-39d2-4f7f-beac-000bb9edbf5d';
  $display->content['new-42fa8d40-39d2-4f7f-beac-000bb9edbf5d'] = $pane;
  $display->panels['left'][4] = 'new-42fa8d40-39d2-4f7f-beac-000bb9edbf5d';
  $pane = new stdClass();
  $pane->pid = 'new-4ba10bd1-e853-4703-abbb-922a3970293c';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-ELwPJqMJeQI0xzGvy2ZlsFPcc0FgIl3v';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Implementing Countries',
    'override_title_heading' => 'h4',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 5;
  $pane->locks = array();
  $pane->uuid = '4ba10bd1-e853-4703-abbb-922a3970293c';
  $display->content['new-4ba10bd1-e853-4703-abbb-922a3970293c'] = $pane;
  $display->panels['left'][5] = 'new-4ba10bd1-e853-4703-abbb-922a3970293c';
  $pane = new stdClass();
  $pane->pid = 'new-3fe93b73-f5c0-4c40-b3b0-ec00ee7e5e80';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-Kz8LHZCBeQycBCKuVr3eQURHVEUixGiA';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Implementing Agency',
    'override_title_heading' => 'h4',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 6;
  $pane->locks = array();
  $pane->uuid = '3fe93b73-f5c0-4c40-b3b0-ec00ee7e5e80';
  $display->content['new-3fe93b73-f5c0-4c40-b3b0-ec00ee7e5e80'] = $pane;
  $display->panels['left'][6] = 'new-3fe93b73-f5c0-4c40-b3b0-ec00ee7e5e80';
  $pane = new stdClass();
  $pane->pid = 'new-1b741263-0365-487e-95c1-bec16d3d11ee';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-zMezFTj7rksp0qu6al0d9iKrO8l1xet2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Donor',
    'override_title_heading' => 'h4',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 7;
  $pane->locks = array();
  $pane->uuid = '1b741263-0365-487e-95c1-bec16d3d11ee';
  $display->content['new-1b741263-0365-487e-95c1-bec16d3d11ee'] = $pane;
  $display->panels['left'][7] = 'new-1b741263-0365-487e-95c1-bec16d3d11ee';
  $pane = new stdClass();
  $pane->pid = 'new-c45c78a0-f416-4f2f-89c6-0a3ed45616dd';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-luI0jh0x9FtV8diaDNwNUcMEMYiYzvyJ';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Tags',
    'override_title_heading' => 'h4',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 8;
  $pane->locks = array();
  $pane->uuid = 'c45c78a0-f416-4f2f-89c6-0a3ed45616dd';
  $display->content['new-c45c78a0-f416-4f2f-89c6-0a3ed45616dd'] = $pane;
  $display->panels['left'][8] = 'new-c45c78a0-f416-4f2f-89c6-0a3ed45616dd';
  $pane = new stdClass();
  $pane->pid = 'new-d1712340-222c-4e80-b32e-00296a0897d3';
  $pane->panel = 'right';
  $pane->type = 'block';
  $pane->subtype = 'current_search-current_search_results_info';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd1712340-222c-4e80-b32e-00296a0897d3';
  $display->content['new-d1712340-222c-4e80-b32e-00296a0897d3'] = $pane;
  $display->panels['right'][0] = 'new-d1712340-222c-4e80-b32e-00296a0897d3';
  $pane = new stdClass();
  $pane->pid = 'new-086ed61e-720d-4535-b529-349ed54275cb';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'project_map-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '086ed61e-720d-4535-b529-349ed54275cb';
  $display->content['new-086ed61e-720d-4535-b529-349ed54275cb'] = $pane;
  $display->panels['right'][1] = 'new-086ed61e-720d-4535-b529-349ed54275cb';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['search_for_projects_map'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'search_funding_opportunities';
  $page->task = 'page';
  $page->admin_title = 'Search Funding Opportunities';
  $page->admin_description = '';
  $page->path = 'funding-opportunities';
  $page->access = array();
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Funding opportunities',
    'name' => 'menu-projects',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_search_funding_opportunities__panel_context_6162cd1d-e6bb-4632-b310-5fc1d71d976b';
  $handler->task = 'page';
  $handler->subtask = 'search_funding_opportunities';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
  );
  $display = new panels_display();
  $display->layout = 'nab_searchpages';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'header' => NULL,
      'top_left' => NULL,
      'top_middle' => NULL,
      'top_right' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom_left' => NULL,
      'bottom_middle' => NULL,
      'bottom_right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Funding Opportunities';
  $display->uuid = '674e872c-f59a-483a-bae5-cedb1d45660a';
  $display->storage_type = '';
  $display->storage_id = '';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-f1df0f53-2174-4c4c-bede-900d67ab17bd';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'views-17cd039ee05bfc303accec0314262709';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'inherit_path' => 1,
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'f1df0f53-2174-4c4c-bede-900d67ab17bd';
  $display->content['new-f1df0f53-2174-4c4c-bede-900d67ab17bd'] = $pane;
  $display->panels['left'][0] = 'new-f1df0f53-2174-4c4c-bede-900d67ab17bd';
  $pane = new stdClass();
  $pane->pid = 'new-5c118592-967c-4a83-8068-d44e5fe46102';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-crqI031cl6jxlwqt0YlxGuWNuR2EDLyw';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Filter By Related Topics:',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '5c118592-967c-4a83-8068-d44e5fe46102';
  $display->content['new-5c118592-967c-4a83-8068-d44e5fe46102'] = $pane;
  $display->panels['left'][1] = 'new-5c118592-967c-4a83-8068-d44e5fe46102';
  $pane = new stdClass();
  $pane->pid = 'new-224533d5-07c4-41ea-8bf5-b8736207f914';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-zMezFTj7rksp0qu6al0d9iKrO8l1xet2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '224533d5-07c4-41ea-8bf5-b8736207f914';
  $display->content['new-224533d5-07c4-41ea-8bf5-b8736207f914'] = $pane;
  $display->panels['left'][2] = 'new-224533d5-07c4-41ea-8bf5-b8736207f914';
  $pane = new stdClass();
  $pane->pid = 'new-fe740bf5-22ff-485b-8850-51b1ee4f8a20';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = '_fund_search-funding_opportunity_search_page';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'fe740bf5-22ff-485b-8850-51b1ee4f8a20';
  $display->content['new-fe740bf5-22ff-485b-8850-51b1ee4f8a20'] = $pane;
  $display->panels['right'][0] = 'new-fe740bf5-22ff-485b-8850-51b1ee4f8a20';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['search_funding_opportunities'] = $page;

  return $pages;

}
