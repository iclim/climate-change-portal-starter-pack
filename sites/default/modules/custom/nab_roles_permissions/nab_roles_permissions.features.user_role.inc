<?php
/**
 * @file
 * nab_roles_permissions.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function nab_roles_permissions_user_default_roles() {
  $roles = array();

  // Exported role: Content Administrator.
  $roles['Content Administrator'] = array(
    'name' => 'Content Administrator',
    'weight' => 4,
  );

  // Exported role: Project Contributor.
  $roles['Project Contributor'] = array(
    'name' => 'Project Contributor',
    'weight' => 2,
  );

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 5,
  );

  return $roles;
}
