<?php
/**
 * @file
 * nab_search.current_search.inc
 */

/**
 * Implements hook_current_search_default_items().
 */
function nab_search_current_search_default_items() {
  $export = array();

  $item = new stdClass();
  $item->disabled = FALSE; /* Edit this to true to make a default item disabled initially */
  $item->api_version = 1;
  $item->name = 'pccp_current_search';
  $item->label = 'pccp_current_search';
  $item->settings = array(
    'items' => array(
      'active_items_links' => array(
        'id' => 'active_links',
        'label' => 'active_items_links',
        'pattern' => '[facetapi_active:active-value]',
        'keys' => 1,
        'css' => 1,
        'classes' => 'foo1class',
        'nofollow' => 1,
        'arguments' => '',
        'search_param' => 'search_api_views_fulltext',
        'weight' => '-49',
      ),
    ),
    'advanced' => array(
      'empty_searches' => '3',
    ),
  );
  $export['pccp_current_search'] = $item;

  return $export;
}
