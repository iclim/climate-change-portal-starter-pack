Description:
============

This feature module enables federated search using a shared Solr index. It
leverages Search API, Search API Solr, and Sarnia for communicating with Solr.
The UI is built with Panels, Search API Views, and Facet API.

There are two separate Search API servers configured, each with a single index.
Regional Search 1 is used for querying the Solr index (reading) and Regional
Search 2 is using for indexing (writing).

We use two different Search API servers because the shared Solr index uses a
custom schema, and that schema is not compatible with Search API out of the box.
The Sarnia module provides all the functionality for querying a custom schema,
and so Regional Search 1 uses Sarnia's Search API service. The indexing part
required a custom Search API service which can be found in the included
module "shared_solr". Regional Search 2 uses that service.

Setup:
======

1. Install Features + module dependencies listed in regional_search.info

2. Enable this module

3. Add the following to settings.php with the name of the site:

    $conf['regional_search_site_name'] = 'Vanuatu';

This site name will appear in the "Source" search facet and will be visible to
all sites in the federated search network.

4. Fill in Solr server details at:
   /admin/config/search/search_api/server/regional_search_1/edit
   /admin/config/search/search_api/server/regional_search_2/edit

5. Index content at /admin/config/search/search_api/index/regional_search_2

6. Under /admin/structure/pages enable /regional-search if not already enabled

7. Check results at /regional-search

How indexing works:
===================

The service class inside modules/shared_solr/shared_solr_service.inc is where
documents are built and sent to the shared Solr index. However, that class
only sets up the properties required for Search API to operate. The rest of the
indexing code can be found in the Search API alteration plugin inside
plugins/search_api/regional_search_alter_callback.inc.

Search API alteration plugins are called during indexing to modify documents.
These plugins can also expose additional index properties which can then be
enabled at: /admin/config/search/search_api/index/regional_search_2/fields.

Adding more fields
------------------

When new fields must be added to the shared Solr index...

1. Add corresponding properties to RegionalSearchAlterCallback::propertyInfo()

2. Enable the property on
   /admin/config/search/search_api/index/regional_search_2/fields

3. Add the necessary indexing code to
   RegionalSearchAlterCallback::attachProperties()

Alternatively you could create a brand new alteration plugin and enable it under
/admin/config/search/search_api/index/regional_search_2/workflow.

Notes:
======

Code in shared_solr module is broken out into a separate module because
otherwise the regional_search feature ends up depending on itself for the
Search API service and becomes impossible to disable.