<?php
/**
 * @file
 * Services resource callbacks for "regional_search_node" resource type.
 *
 * @see regional_search_services_resources()
 */

/**
 * Service resource "retrieve" callback.
 */
function _regional_search_node_resource_retrieve($uuid)
{
    if ($cache = cache_get("regional_search_resource_{$uuid}")) {
        return $cache->data;
    }

    // Load the node from UUID.
    $nid = ($entity_ids = entity_get_id_by_uuid('node', array($uuid))) ? reset($entity_ids) : null;
    $wrapper = entity_metadata_wrapper('node', $nid);
    $node = $wrapper->value();

    if (!$node) {
        return services_error('Node not found.', 404);
    }
    if (empty($node->status) || $node->type != 'document' && $node->type != 'product' && $node->type != 'event') {
        return services_error('Invalid node.', 403);
    }

    // print '<pre>';
    // print_r($node);
    // print '</pre>';



    // This is our object that we will pouplate with data
    $resource = new stdClass();

    // ID
    $resource->id = $node->uuid;

    // Title
    if (!empty($node->title)) {
        $resource->title = $node->title;
    }
  
    //Projects have title in different field
    if ($node->type == 'product') {
        if (!empty($node->field_project_name)) {
            $resource->title = $node->field_project_name[LANGUAGE_NONE][0]['value'];
        }
    }

    // URL
    if ($url = entity_uri('node', $node)) {
        $resource->url = url($url['path'], array('absolute' => true) + $url['options']);
    } else {
        $resource->url = url("node/{$node->nid}", array('absolute' => true));
    }

    // Site name
    $resource->source_ss[] = variable_get('site_name');

    // Content type
    switch ($node->type) {
        case 'document':
            $type = 'Document'; break;
        case 'product':
            $type = 'Project'; break;
        case 'event':
            $type = 'Event'; break;
        default:
            $type = ucfirst($node->type);
    }

    $resource->content_type[] = $type;
    $resource->type_ss[] = $type;

    // Full text
    $resource->content = _regional_search_node_resource_rendered($node);

    // Description
    if (!empty($node->field_the_description)) {
        $resource->description = $node->field_the_description[LANGUAGE_NONE][0]['value'];
    }

    // Dates (Documents)
    if ($year = _regional_search_node_resource_field_value($wrapper, 'field_publication_year')) {
        $resource->document_publication_year_s = $year;
        $resource->date = $year;
    }

    // Dates (Projects)
    if ($years = _regional_search_node_resource_project_years($wrapper)) {
        $resource->project_start_year_s = reset($years);
        $resource->project_end_year_s = end($years);
        $resource->project_dates_ss = $years;
        $resource->date = reset($years);
    }

    //Author (doucment)
    if (!empty($node->field_author)) {
        $resource->author = $node->field_author[LANGUAGE_NONE][0]['value'];
    }

    //Language (document)
    if ($names = _regional_search_node_term_values($wrapper, 'field_language',$node)) {
        $resource->language_ss = $names;
    }

    //Resource Type (document)
    if ($names = _regional_search_node_term_values($wrapper, 'field_resource_type',$node)) {
        $resource->resource_type_ss = $names;
    }

    //Educational or Awareness Resource Type (document)
    if ($names = _regional_search_node_term_values($wrapper, 'field_educational_resource_type',$node)) {
        $resource->educational_resource_type_ss = $names;
    }

    //Subject Strand  (document)
    if ($names = _regional_search_node_term_values($wrapper, 'field_subject_strand',$node)) {
        $resource->subject_strand_ss = $names;
    }

    //Regional Focus (document)
    if ($names = _regional_search_node_term_values($wrapper, 'field_regional_focus',$node)) {
        $resource->regional_focus_ss = $names;
    }

    // Related Countries (document)
    if ($names = _regional_search_node_resource_reference_labels($wrapper, 'field_country')) {
        $resource->related_country_ss = $names;
    }

    // Implementing Countries (project)
    if ($names = _regional_search_node_resource_reference_labels($wrapper, 'field_implementing_countries')) {
        $resource->project_implementing_countries_ss = $names;
        $resource->related_country_ss = $names;
    }

    //Implementing Agencies
    if ($names = _regional_search_node_resource_reference_labels($wrapper, 'field_implementing_agency')) {
        $resource->project_implementing_agencies_ss = $names;
    }

    //Donor
    if ($names = _regional_search_node_resource_reference_labels($wrapper, 'field_donor_enity')) {
        $resource->donor_ss = $names;
    }

    //More Project Fields
    //Project Status
    if ($names = _regional_search_node_term_values($wrapper, 'field_project_status',$node)) {
        $resource->project_status_ss = $names;
    }

    //Project Type
    if ($names = _regional_search_node_term_values($wrapper, 'field_project_type',$node)) {
        $resource->project_type_ss = $names;
    }

    //Project Scope
    if ($names = _regional_search_node_term_values($wrapper, 'field_project_scope',$node)) {
        $resource->project_scope_ss = $names;
    }

    //Project Focus Area
    if ($names = _regional_search_node_term_values($wrapper, 'field_focus_area',$node)) {
        $resource->focus_area_ss = $names;
    }

    //Tags
    if ($names = _regional_search_node_term_values($wrapper, 'field_tags',$node)) {
        $resource->tags_ss = $names;
    }
  

  

    // Topics
    $topics = _regional_search_node_resource_topics($wrapper);
    if ($topics['list']) {
        $resource->project_topics_ss = $topics['list'];
        $resource->topics_ss = $topics['list'];
    }
    if ($topics['depth0']) {
        $resource->vocab_topics_level0 = $topics['depth0'];
    }
    if ($topics['depth1']) {
        $resource->vocab_topics_level1 = $topics['depth1'];
    }
    if ($topics['tree']) {
        $resource->vocab_topics_ss = $topics['tree'];
    }

    // Download links
    if ($links = _regional_search_node_resource_file_links($wrapper, 'field_document_files')) {
        $resource->download_links = $links;
    } elseif ($links = _regional_search_node_resource_file_links($wrapper, 'field_project_files')) {
        $resource->download_links = $links;
    }

    // Project locations
    if (isset($wrapper->latitude) && ($latitudes = $wrapper->latitude->value())) {
        $resource->project_sites_lat_ss = $latitudes;
    }
    if (isset($wrapper->longitude) && ($longitudes = $wrapper->longitude->value())) {
        $resource->project_sites_lng_ss = $longitudes;
    }



    cache_set("regional_search_resource_{$uuid}", $resource);

    return $resource;
}

/**
 * Service resource "index" callback.
 */
function _regional_search_node_resource_index()
{
    $query = db_select('node', 'n')
    ->orderBy('nid', 'desc')
    ->condition('type', array('document', 'product'), 'IN')
    ->condition('status', 1);

  // Use a generic "id" field for results.
  $query->addField('n', 'uuid', 'id');

    return $query->distinct()->execute()->fetchAll();
}

/**
 * Helper to get the full node view content.
 */
function _regional_search_node_resource_rendered($node)
{
    try {
        $render = entity_view('node', array(entity_id('node', $node) => $node), 'search_index');
        return render($render);
    } catch (Exception $e) {
        return '';
    }
}

/**
 * Helper to get a field value.
 */
function _regional_search_node_resource_field_value(EntityMetadataWrapper $wrapper, $field_name)
{
    if (isset($wrapper->field_name)) {
        return $wrapper->{$field_name}->raw();
    }
    return null;
}

/**
 * Helper to get multiple field values.
 */
function _regional_search_node_resource_field_values(EntityMetadataWrapper $wrapper, $field_name)
{
    if (isset($wrapper->field_name)) {
        return $wrapper->{$field_name}->raw();
    }
    return array();
}

/**
 * Helper to get the name of a related entity.
 */
function _regional_search_node_resource_reference_labels(EntityMetadataWrapper $wrapper, $field_name)
{
    $labels = array();
    if (isset($wrapper->{$field_name})) {
        foreach ($wrapper->{$field_name} as $field) {
            $labels[] = $field->label();
        }
    }
    return $labels;
}

/**
 * Helper to get a list of years for a project.
 */
function _regional_search_node_resource_project_years(EntityMetadataWrapper $wrapper)
{
    $years = array();
    if (isset($wrapper->field_start_date) && isset($wrapper->field_end_date)) {
        $start = $wrapper->field_start_date->raw();
        $end = $wrapper->field_end_date->raw();
        if ($start && $end) {
            $years = range(date('Y', $start), date('Y', $end));
        } elseif ($start) {
            $years[] = date('Y', $start);
        }
    }
    return array_unique($years);
}

/**
 * Helper to get list of file URLs for file field.
 */
function _regional_search_node_resource_file_links(EntityMetadataWrapper $wrapper, $field_name)
{
    $urls = array();
    if (!isset($wrapper->{$field_name})) {
        return $urls;
    }
    foreach ($wrapper->{$field_name}->raw() as $file) {
        if (!empty($file['uri'])) {
            $urls[] = file_create_url($file['uri']);
        }
    }
    return $urls;
}

/**
 * Gathers topic names (taxonomy terms) in a number of ways to accommodate
 * various topic fields in the schema.
 *
 * list:
 *  - Governance
 *  - Government, law and administration
 * tree:
 *  - 0/Government, law and administration/
 *  - 1/Government, law and administration/Governance/
 * depth0:
 *  - Government, law and administration
 * depth1:
 *  - Governance
 */
function _regional_search_node_resource_topics(EntityMetadataWrapper $wrapper)
{
    $data = array(
    'list' => array(),
    'tree' => array(),
    'depth0' => array(),
    'depth1' => array(),
  );

    if (!isset($wrapper->field_topics)) {
        return $data;
    }

  // Gather all terms for fast lookup, including depth.
  $terms = array();
    $vocab = taxonomy_vocabulary_machine_name_load('topics');
    foreach (taxonomy_get_tree($vocab->vid) as $term) {
        $terms[$term->tid] = $term;
    }

  // Gather relatives for each term ordered by depth.
  $trees = array();
    foreach ($wrapper->field_topics->raw() as $tid) {
        if (!isset($terms[$tid])) {
            continue;
        }
        foreach (taxonomy_get_parents_all($tid) as $term) {
            $depth = $terms[$term->tid]->depth;
            $trees[$tid][$depth] = $term->name;
        }
    }

  // If we didn't gather any relatives there were probably stale references.
  if (empty($trees)) {
      return $data;
  }

    foreach ($trees as $tree) {
        $all_names = $tree;
        ksort($all_names);
        foreach ($tree as $depth => $name) {
            $formatted = "{$depth}/";
            $topic_names = array_slice($all_names, 0, $depth + 1);
            foreach ($topic_names as $topic_name) {
                $formatted .= "{$topic_name}/";
            }
            $data['tree'][] = $formatted;
            $data['list'][] = $name;
            $data['depth' . $depth][] = $name;
        }
    }

    foreach ($data as $key => $list) {
        $data[$key] = array_values(array_unique($list));
        sort($data[$key]);
    }

    return $data;
}

/**
 * Helper to get list taxonomy term values for field
 */
function _regional_search_node_term_values(EntityMetadataWrapper $wrapper, $field_name,$node)
{
    

    $names = array();
    if (!isset($node->$field_name[LANGUAGE_NONE])) {
        return $names;
    }
    foreach ($node->$field_name[LANGUAGE_NONE] as $tid) {
        $term = taxonomy_term_load($tid['tid']);
        if (!isset($term->name)) {
            continue;
        }
        $names[] = $term->name;
        
    }

    // $term=taxonomy_term_load($node->field_resource_type['und'][0]['tid']);
    // die(print_r($names,true));
  
    return $names;
}


