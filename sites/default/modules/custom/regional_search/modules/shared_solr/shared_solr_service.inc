<?php
/**
 * @file
 * Contains SharedSolrService.
 */

class SharedSolrService extends SearchApiSolrService {

  /**
   * Overrides SearchApiSolrService::search().
   *
   * We don't support searching here, only indexing.
   */
  public function search(SearchApiQueryInterface $query) {
    return array(
      'results' => array(),
      'result count' => '1000+',
    );
  }

  /**
   * Overrides SearchApiSolrService::indexItems().
   */
  public function indexItems(SearchApiIndex $index, array $items) {
    $documents = array();
    $index_id = $this->getIndexId($index->machine_name);
    $fields = (isset($index->options['fields']) ? $index->options['fields'] : array());

    foreach ($items as $id => $item) {
      $doc = new SearchApiSolrDocument();

      // Add common fields.
      $doc->setField('id', $this->createId($index_id, $id));
      $doc->setField('item_id_s', $id);
      $doc->setField('index_id_s', $index_id);
      $doc->setField('hash_s', search_api_solr_site_hash());
      $doc->setField('site_s', $GLOBALS['base_url']);

      // Remove invalid fields forcefully added by Search API or others.
      unset($item['search_api_language']);

      // Add all the configured fields to the document.
      foreach ($item as $key => $field) {
        if (isset($fields[$key])) {
          $this->addIndexField($doc, $key, $field['value'], $field['type']);
        }
      }

      $documents[] = $doc;
    }

    // Let other modules alter documents before sending them to solr.
    drupal_alter('search_api_solr_documents', $documents, $index, $items);
    $this->alterSolrDocuments($documents, $index, $items);

    if (!$documents) {
      return array();
    }

    try {
      $this->connect();
      $this->solr->addDocuments($documents);
      if (!empty($index->options['index_directly'])) {
        $this->scheduleCommit();
      }
      return array_keys($items);
    }
    catch (SearchApiException $e) {
      watchdog_exception('shared_solr', $e, "%type while indexing: !message in %function (line %line of %file).");
    }

    return array();
  }

  /**
   * Overrides SearchApiSolrService::deleteItems().
   *
   * Prevents content indexed by other sites from being deleted.
   */
  public function deleteItems($ids = 'all', SearchApiIndex $index = NULL) {
    // Ignore "wildcard" calls that don't specify an index or ids - those are
    // meant to clear all data from the server.
    if (empty($ids) || empty($index)) {
      return;
    }

    $this->connect();
    if (is_array($ids)) {
      $index_id = $this->getIndexId($index->machine_name);
      $solr_ids = array();
      foreach ($ids as $id) {
        $solr_ids[] = $this->createId($index_id, $id);
      }
      $this->solr->deleteByMultipleIds($solr_ids);
    }
    else {
      $query = array();

      $index_id = $this->getIndexId($index->machine_name);
      $index_id = call_user_func(array($this->connection_class, 'phrase'), $index_id);

      // ALWAYS filter by site hash and index.
      $query[] = "index_id_s:$index_id";
      $query[] = 'hash_s:' . search_api_solr_site_hash();

      if ($ids != 'all') {
        $query[] = $query ? "($ids)" : $ids;
      }
      $this->solr->deleteByQuery($query ? implode(' AND ', $query) : '*:*');
    }
    $this->scheduleCommit();
  }
}