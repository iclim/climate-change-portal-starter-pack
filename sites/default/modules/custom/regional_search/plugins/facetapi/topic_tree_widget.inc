<?php
/**
 * @file
 * Contains TopicTreeFacetapiWidget.
 */

/**
 * Class TopicTreeFacetapiWidget
 *
 * Widget plugin that turns the a topics list into a collapsible hierarchy.
 */
class TopicTreeFacetapiWidget extends FacetapiWidgetCheckboxLinks {

  /**
   * Overrides FacetapiWidgetLinks::init().
   *
   * Adds Javascript for expand/collapse behavior. Topic hierarchy handling
   * happens in Facet API hooks.
   * @see regional_search.module
   */
  public function init() {
    parent::init();
    drupal_add_js(drupal_get_path('module', 'regional_search') . '/plugins/facetapi/topic_tree_widget.js');
  }
}