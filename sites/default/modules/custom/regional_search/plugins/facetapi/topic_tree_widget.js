(function ($) {

  /**
   * Toggle behavior for the "Topic Tree" Facet API Widget.
   */
  Drupal.behaviors.topic_tree_widget = {
    attach: function (context, settings) {
      $('.facetapi-topic-tree li', context).each(function() {
        var $root = $(this);
        var $children = $root.find('ul').hide();
        var $link = $('<i class="toggle toggle-invisible toggle-collapsed"></i>');
        $root.prepend($link);

        if ($children.length <= 0) {
          return;
        }

        // Set up the toggle.
        $link.text('+').removeClass('.toggle-invisible').click(function() {
          var $target = $(this);
          $target.toggleClass('expanded');
          $target.toggleClass('collapsed');
          $children.slideToggle('fast');
          if ($target.text() == '+') {
            $target.text('-');
          }
          else {
            $target.text('+');
          }
        });

        // Start as expanded when any items are active.
        if ($root.find('.facetapi-active').length > 0) {
          $link.click();
        }
      });
    }
  }

})(jQuery);