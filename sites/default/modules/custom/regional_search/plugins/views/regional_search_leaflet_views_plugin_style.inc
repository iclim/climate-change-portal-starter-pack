<?php
/**
 * @file
 * Contains regional_search_leaflet_views_plugin_style.
 */

/**
 * Views style plugin to display a Leaflet map.
 *
 * The views plugin that comes with leaflet_views only works with Geofields.
 * In our case, coordinates come from Solr properties, so unfortunately we need
 * this whole new plugin. Most of this was copied from leaflet_views_plugin_style
 * with the single "data_source" field converted to separate fields.
 */
class regional_search_leaflet_views_plugin_style extends leaflet_views_plugin_style {

  /**
   * Set default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['data_source_lat'] = array('default' => '');
    $options['data_source_lon'] = array('default' => '');
    return $options;
  }

  /**
   * Options form.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Get list of fields in this view & flag available geodata fields:
    $handlers = $this->display->handler->get_handlers('field');

    $fields = array();
    $fields_data = array();
    foreach ($handlers as $field_id => $handler) {
      $fields[$field_id] = $handler->ui_name();
      $fields_data[$field_id] = $handler->ui_name();
    }

    // Get available entity types for selection.
    $default_type = empty($this->options['entity_type']) ? '' : $this->options['entity_type'];
    $entity_types = array('');
    foreach (entity_get_info() as $key => $info) {
      $entity_types[$key] = $info['label'];
      if ($this->view->base_table == $info['base table']) {
        // If this entity type matches the view base table, use it.
        $default_type = $key;
      }
    }

    // Entity type selection.
    $form['entity_type'] = array(
      '#type' => 'select',
      '#title' => t('Entity Type'),
      '#description' => t('Which type of entity is this view based on?'),
      '#options' => $entity_types,
      '#default_value' => $default_type,
      '#required' => TRUE,
    );

    // Only allow users to choose the entity type if it cannot be detected.
    if (!empty($default_type)) {
      $form['entity_type']['#access'] = FALSE;
    }

    // Map preset.
    $form['data_source_lat'] = array(
      '#type' => 'select',
      '#title' => t('Data Source - Latitude'),
      '#description' => t('Which field contains latitude?'),
      '#options' => $fields_data,
      '#default_value' => $this->options['data_source_lat'],
      '#required' => TRUE,
    );
    $form['data_source_lon'] = array(
      '#type' => 'select',
      '#title' => t('Data Source - Longitude'),
      '#description' => t('Which field contains longitude?'),
      '#options' => $fields_data,
      '#default_value' => $this->options['data_source_lon'],
      '#required' => TRUE,
    );

    // Name field.
    $form['name_field'] = array(
      '#type' => 'select',
      '#title' => t('Title Field'),
      '#description' => t('Choose the field which will appear as a title on tooltips.'),
      // '#options' => $fields,
      '#options' => array_merge(array('' => ''), $fields),
      '#default_value' => $this->options['name_field'],
    );

    $desc_options = array_merge(array(
      '' => '',
    ), $fields);

    // Add an option to render the entire entity using a view mode.
    if (isset($this->entity_type)) {
      $desc_options += array(
        '#rendered_entity' => '<' . t('!entity entity', array('!entity' => $this->entity_type)) . '>',
      );
    }

    $form['description_field'] = array(
      '#type' => 'select',
      '#title' => t('Description Content'),
      '#description' => t('Choose the field or rendering method which will appear as a description on tooltips or popups.'),
      '#required' => FALSE,
      '#options' => $desc_options,
      '#default_value' => $this->options['description_field'],
    );

    // Taken from openlayers_views_style_data::options_form().
    // Create view mode options:
    if (isset($this->entity_type)) {

      // Get the labels (human readable) of the view modes:
      $view_mode_options = array();
      foreach ($this->entity_info['view modes'] as $key => $view_mode) {
        $view_mode_options[$key] = $view_mode['label'];
      }

      // Output the form:
      $form['view_mode'] = array(
        '#type' => 'select',
        '#title' => t('View mode'),
        '#description' => t('View modes are ways of displaying entities.'),
        '#options' => $view_mode_options,
        '#default_value' => !empty($this->options['view_mode']) ? $this->options['view_mode'] : 'full',
        '#states' => array(
          'visible' => array(
            ':input[name="style_options[description_field]"]' => array('value' => '#rendered_entity'),
          ),
        ),
      );
    }

    // Choose a map preset:
    $map_options = array();
    foreach (leaflet_map_get_info() as $key => $map) {
      $map_options[$key] = t('@label', array('@label' => $map['label']));
    }

    $form['map'] = array(
      '#title' => t('Map'),
      '#type' => 'select',
      '#options' => $map_options,
      '#default_value' => $this->options['map'] ? $this->options['map'] : '',
      '#required' => TRUE,
    );

    $form['height'] = array(
      '#title' => t('Map height'),
      '#type' => 'textfield',
      '#field_suffix' => t('px'),
      '#size' => 4,
      '#default_value' => $this->options['height'],
      '#required' => FALSE,
    );

    $form['hide_empty'] = array(
      '#title' => t('Hide empty'),
      '#type' => 'checkbox',
      '#description' => t('Hide the Leaflet map if there are no results to display.'),
      '#default_value' => isset($this->options['hide_empty']) ? $this->options['hide_empty'] : TRUE,
    );

    $form['zoom'] = leaflet_form_elements('zoom', $this->options);
    $form['icon'] = leaflet_form_elements('icon', $this->options, array('path' => 'style_options', 'fields' => $fields));
    $form['vector_display'] = leaflet_form_elements('vector_display', $this->options, array('path' => 'style_options'));
    $form['tokens'] = leaflet_form_elements('tokens', $this->options, array('weight' => 998, 'entity_type' => $this->entity_type));
  }

  /**
   * Validate the options form.
   */
  function options_validate(&$form, &$form_state) {
    if (!is_numeric($form_state['values']['style_options']['height']) || $form_state['values']['style_options']['height'] < 0) {
      form_error($form['height'], t('Map height needs to be a positive number'));
    }
  }

  /**
   * Renders view.
   */
  function render() {
    if (!empty($this->view->live_preview)) {
      return t('No preview available.');
    }
    $data = array();
    $map = leaflet_map_get_info($this->options['map']);
    // Is there a geofield selected?
    if ($this->options['data_source_lat'] && $this->options['data_source_lon']) {
      $name_field = empty($this->options['name_field']) ? NULL : $this->options['name_field'];
      $this->render_fields($this->view->result);
      foreach ($this->view->result as $id => $result) {
        $latitudes = $this->get_field_value($id, $this->options['data_source_lat']);
        $longitudes = $this->get_field_value($id, $this->options['data_source_lon']);

        // If one value is missing or there are an unequal number, something
        // is wrong with the way they are indexed.
        if (empty($latitudes) || empty($longitudes) || (count($latitudes) != count($longitudes))) {
          continue;
        }

        foreach ($latitudes as $delta => $latitude) {
          $longitude = $longitudes[$delta];

          // Some items outside of our control are indexed with 0,0 coordinates
          // so we need to ignore these.
          if (intval($latitude) == 0 && intval($longitude) == 0) {
            continue;
          }

          // Allow points on both sides of the dateline to be displayed
          // in the same area on the map.
          // @see regional_search.leaflet.js
          if ($longitude > 1) {
            $longitude = $longitude - 360;
          }

          $entity = FALSE;
          // Render the entity with the selected view mode:
          if (is_object($result)) {
            $description = '';
            if ($name_field) {
              $description = $this->rendered_fields[$id][$name_field];
            }
            if ($this->options['description_field']) {
              $description .= $this->rendered_fields[$id][$this->options['description_field']];
            }
          }

          // $points = leaflet_process_geofield($geofield);
          $points = array(
            array(
              'type' => 'point',
              'lat' => (float) $latitude,
              'lon' => (float) $longitude,
            ),
          );

          // Attach pop-ups if we have rendered into $description:
          if (isset($description)) {
            foreach ($points as &$point) {
              $point['popup'] = $description;
            }
          }
          // Attach also titles & entities, they might be used later on.
          if ($name_field) {
            foreach ($points as &$point) {
              if (isset($this->rendered_fields[$id][$name_field])) {
                $point['label'] = htmlspecialchars_decode(strip_tags($this->rendered_fields[$id][$name_field]));
              }
              if ($entity !== FALSE) {
                $point['entity'] = $entity;
              }
            }
          }
          if ($this->options['icon']['iconType'] == 'html') {
            foreach ($points as &$point) {
              $target_field = $this->options['icon']['html'];
              $point['rendered_html'] = isset($this->rendered_fields[$id][$target_field]) ? $this->rendered_fields[$id][$target_field] : '';
            }
          }
          // Let modules modify the points data.
          drupal_alter('leaflet_views_alter_points_data', $result, $points);
          // Merge these points into the $data array for map rendering:
          $data = array_merge($data, $points);
        }
      }
      $entity_type = isset($this->entity_type) ? $this->entity_type : '';
      leaflet_apply_map_settings($map, $data, $this->options, $entity_type);
      if (empty($data) && !empty($this->options['hide_empty'])) {
        return;
      }
      return leaflet_build_map($map, $data, $this->options['height'] . 'px');
    }
    return;
  }
}
