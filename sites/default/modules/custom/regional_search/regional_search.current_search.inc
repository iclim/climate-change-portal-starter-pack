<?php
/**
 * @file
 * regional_search.current_search.inc
 */

/**
 * Implements hook_current_search_default_items().
 */
function regional_search_current_search_default_items() {
  $export = array();

  $item = new stdClass();
  $item->disabled = FALSE; /* Edit this to true to make a default item disabled initially */
  $item->api_version = 1;
  $item->name = 'regional_search';
  $item->label = 'Regional Search';
  $item->settings = array(
    'items' => array(
      'label' => array(
        'id' => 'text',
        'label' => 'Label',
        'text' => 'You searched for:',
        'plural' => 0,
        'text_plural' => '',
        'plural_condition' => 'facetapi_results:result-count',
        'wrapper' => 1,
        'element' => 'span',
        'css' => 1,
        'classes' => 'current-search-label',
        'weight' => '-50',
      ),
      'active' => array(
        'id' => 'active',
        'label' => 'Active Items',
        'pattern' => '[facetapi_active:active-value]',
        'keys' => 0,
        'css' => 1,
        'classes' => 'current-search-list',
        'nofollow' => 1,
        'weight' => '-49',
      ),
      'reset' => array(
        'id' => 'reset_filters',
        'label' => 'Reset Link',
        'text' => 'Start over',
        'plural' => 0,
        'text_plural' => '',
        'plural_condition' => 'facetapi_results:result-count',
        'wrapper' => 1,
        'element' => 'span',
        'css' => 1,
        'classes' => 'current-search-reset',
        'weight' => '-48',
      ),
    ),
    'advanced' => array(
      'empty_searches' => '2',
    ),
  );
  $export['regional_search'] = $item;

  return $export;
}
