<?php
/**
 * @file
 * regional_search.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function regional_search_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "current_search" && $api == "current_search") {
    return array("version" => "1");
  }
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "sarnia" && $api == "sarnia") {
    return array("version" => "1");
  }
  if ($module == "services" && $api == "services") {
    return array("version" => "3");
  }
}

/**
 * Implements hook_views_api().
 */
function regional_search_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_search_api_index().
 */
function regional_search_default_search_api_index() {
  $items = array();
  $items['sarnia_regional_search_1'] = entity_import('search_api_index', '{
    "name" : "Regional Search 1",
    "machine_name" : "sarnia_regional_search_1",
    "description" : "This index is managed by the Sarnia module.",
    "server" : "regional_search_1",
    "item_type" : "sarnia_regional_search_1",
    "options" : {
      "index_directly" : 0,
      "cron_limit" : "0",
      "sorts_widget" : {
        "enabled" : false,
        "title" : "",
        "autosubmit" : false,
        "autosubmit_hide" : false
      },
      "fields" : {
        "_version_" : {
          "name" : "_version_",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "author_ss" : {
          "name" : "author_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "content_type" : {
          "name" : "content_type",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "date" : { "name" : "date", "indexed" : true, "type" : "text", "boost" : "1.0" },
        "description" : {
          "name" : "description",
          "indexed" : true,
          "type" : "text",
          "boost" : "1.0"
        },
        "document_author_ss" : {
          "name" : "document_author_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "document_bookid_s" : {
          "name" : "document_bookid_s",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "document_description_s" : {
          "name" : "document_description_s",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "document_edition_s" : {
          "name" : "document_edition_s",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "document_geographic_focus_ss" : {
          "name" : "document_geographic_focus_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "document_isbn_s" : {
          "name" : "document_isbn_s",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "document_language_ss" : {
          "name" : "document_language_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "document_literacy_ss" : {
          "name" : "document_literacy_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "document_national_framework_ss" : {
          "name" : "document_national_framework_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "document_physical_description_s" : {
          "name" : "document_physical_description_s",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "document_publication_year_s" : {
          "name" : "document_publication_year_s",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "document_target_group_ss" : {
          "name" : "document_target_group_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "donors_ss" : {
          "name" : "donors_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "download_links" : {
          "name" : "download_links",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "id" : { "name" : "id", "indexed" : true, "type" : "none", "boost" : "1.0" },
        "project_actions_ss" : {
          "name" : "project_actions_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_agency_contact_ss" : {
          "name" : "project_agency_contact_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_contact_ss" : {
          "name" : "project_contact_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_dates_ss" : {
          "name" : "project_dates_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_development_partners_ss" : {
          "name" : "project_development_partners_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_donor_ss" : {
          "name" : "project_donor_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_donors_ss" : {
          "name" : "project_donors_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_duration_s" : {
          "name" : "project_duration_s",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_end_year_s" : {
          "name" : "project_end_year_s",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_focus_goals_ss" : {
          "name" : "project_focus_goals_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_implementing_agency_ss" : {
          "name" : "project_implementing_agency_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_implementing_countries_ss" : {
          "name" : "project_implementing_countries_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_implkementing_agencies_ss" : {
          "name" : "project_implkementing_agencies_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_links_ss" : {
          "name" : "project_links_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_links_text_ss" : {
          "name" : "project_links_text_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_national_framework_ss" : {
          "name" : "project_national_framework_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_objectives_ss" : {
          "name" : "project_objectives_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_outcomes_ss" : {
          "name" : "project_outcomes_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_partner_contact_ss" : {
          "name" : "project_partner_contact_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_pifacc_ss" : {
          "name" : "project_pifacc_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_proj_management_unit_s" : {
          "name" : "project_proj_management_unit_s",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_regional_cc_terms_ss" : {
          "name" : "project_regional_cc_terms_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_scope_ss" : {
          "name" : "project_scope_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_sector_ss" : {
          "name" : "project_sector_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_short_title_s" : {
          "name" : "project_short_title_s",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_sites_country_name_ss" : {
          "name" : "project_sites_country_name_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_sites_lat_ss" : {
          "name" : "project_sites_lat_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_sites_lng_ss" : {
          "name" : "project_sites_lng_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_sites_name_ss" : {
          "name" : "project_sites_name_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_sites_ss" : {
          "name" : "project_sites_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_start_year_s" : {
          "name" : "project_start_year_s",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_status_s" : {
          "name" : "project_status_s",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_theme_ss" : {
          "name" : "project_theme_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_topics_ss" : {
          "name" : "project_topics_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_total_funding_us_s" : {
          "name" : "project_total_funding_us_s",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "project_type_ss" : {
          "name" : "project_type_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "related_country_ss" : {
          "name" : "related_country_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "source_ss" : {
          "name" : "source_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "tags_ss" : { "name" : "tags_ss", "indexed" : true, "type" : "none", "boost" : "1.0" },
        "text" : { "name" : "text", "indexed" : true, "type" : "text", "boost" : "1.0" },
        "title" : { "name" : "title", "indexed" : true, "type" : "text", "boost" : "1.0" },
        "title_sort" : {
          "name" : "title_sort",
          "indexed" : true,
          "type" : "text",
          "boost" : "1.0"
        },
        "title_suggest" : {
          "name" : "title_suggest",
          "indexed" : true,
          "type" : "text",
          "boost" : "1.0"
        },
        "topics_ss" : {
          "name" : "topics_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "type_ss" : { "name" : "type_ss", "indexed" : true, "type" : "none", "boost" : "1.0" },
        "update_date_s" : {
          "name" : "update_date_s",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "url" : { "name" : "url", "indexed" : true, "type" : "text", "boost" : "1.0" },
        "vocab_topics_level0" : {
          "name" : "vocab_topics_level0",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "vocab_topics_level1" : {
          "name" : "vocab_topics_level1",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "vocab_topics_s" : {
          "name" : "vocab_topics_s",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "vocab_topics_ss" : {
          "name" : "vocab_topics_ss",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        }
      }
    },
    "enabled" : "1",
    "read_only" : "1",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function regional_search_default_search_api_server() {
  $items = array();
  $items['regional_search_1'] = entity_import('search_api_server', '{
    "name" : "Regional Search 1 (read only)",
    "machine_name" : "regional_search_1",
    "description" : "This server is used by the Sarnia module for queries against the shared Solr index.",
    "class" : "sarnia_solr_service",
    "options" : {
      "clean_ids" : true,
      "site_hash" : true,
      "scheme" : "http",
      "host" : "solr1.sprep.org",
      "port" : "80",
      "path" : "\\/solr\\/collection1",
      "http_user" : "",
      "http_pass" : "",
      "excerpt" : 0,
      "retrieve_data" : 0,
      "highlight_data" : 0,
      "skip_schema_check" : 1,
      "solr_version" : "",
      "http_method" : "AUTO",
      "log_query" : 0,
      "log_response" : 0,
      "sarnia_request_handler" : "",
      "sarnia_default_query" : "*:*"
    },
    "enabled" : "1",
    "rdf_mapping" : []
  }');
  return $items;
}
