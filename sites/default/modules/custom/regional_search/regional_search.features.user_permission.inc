<?php
/**
 * @file
 * regional_search.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function regional_search_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access regional search endpoint'.
  $permissions['access regional search endpoint'] = array(
    'name' => 'access regional search endpoint',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'regional_search',
  );

  return $permissions;
}
