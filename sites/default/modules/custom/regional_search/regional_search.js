(function ($) {

  /**
   * Expand/collapse behavior for facets on the Regional Search page.
   */
  Drupal.behaviors.regional_search = {
    attach: function (context, settings) {
      // Collapse facets beyond the second one that have no active items.
      $('.facet-container .panel-facet:gt(1):not(:has(.facetapi-active))', context).each(function() {
        // $(this).find('a[data-toggle="collapse"]').click();
      });
    }
  }

})(jQuery)