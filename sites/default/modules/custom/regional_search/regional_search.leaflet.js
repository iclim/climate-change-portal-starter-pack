(function($) {

  /**
   * Overrides the same function from Leaflet module's leaflet.drupal.js.
   *
   * We need to do this in order to allow points east and west of the
   * dateline to appear together instead of separate sides of the world. This is
   * accomplished by adding or removing 360 degrees to some longitudes,
   * effectively putting them on a second, adjacent world map. When wrapping is
   * used for points in Leaflet, this is impossible.
   *
   * This is an exact copy of the original with "latLng.wrap()" commented out.
   *
   * @see https://www.drupal.org/node/2015885#comment-11295957
   */
  Drupal.leaflet.create_point = function(marker, lMap) {
    var latLng = new L.LatLng(marker.lat, marker.lon);
    // latLng = latLng.wrap();
    lMap.bounds.push(latLng);
    var lMarker;

    if (marker.html) {
      if (marker.html_class) {
        var icon = new L.DivIcon({html: marker.html, className: marker.html_class});
      }
      else {
        var icon = new L.DivIcon({html: marker.html});
      }
      // override applicable marker defaults
      if (marker.icon.iconSize) {
        icon.options.iconSize = new L.Point(parseInt(marker.icon.iconSize.x, 10), parseInt(marker.icon.iconSize.y, 10));
      }
      if (marker.icon.iconAnchor) {
        icon.options.iconAnchor = new L.Point(parseFloat(marker.icon.iconAnchor.x), parseFloat(marker.icon.iconAnchor.y));
      }
      lMarker = new L.Marker(latLng, {icon:icon});
    }
    else if (marker.icon) {
      var icon = new L.Icon({iconUrl: marker.icon.iconUrl});

      // override applicable marker defaults
      if (marker.icon.iconSize) {
        icon.options.iconSize = new L.Point(parseInt(marker.icon.iconSize.x, 10), parseInt(marker.icon.iconSize.y, 10));
      }
      if (marker.icon.iconAnchor) {
        icon.options.iconAnchor = new L.Point(parseFloat(marker.icon.iconAnchor.x), parseFloat(marker.icon.iconAnchor.y));
      }
      if (marker.icon.popupAnchor) {
        icon.options.popupAnchor = new L.Point(parseFloat(marker.icon.popupAnchor.x), parseFloat(marker.icon.popupAnchor.y));
      }
      if (marker.icon.shadowUrl !== undefined) {
        icon.options.shadowUrl = marker.icon.shadowUrl;
      }
      if (marker.icon.shadowSize) {
        icon.options.shadowSize = new L.Point(parseInt(marker.icon.shadowSize.x, 10), parseInt(marker.icon.shadowSize.y, 10));
      }
      if (marker.icon.shadowAnchor) {
        icon.options.shadowAnchor = new L.Point(parseInt(marker.icon.shadowAnchor.x, 10), parseInt(marker.icon.shadowAnchor.y, 10));
      }
      if (marker.icon.zIndexOffset) {
        icon.options.zIndexOffset = marker.icon.zIndexOffset;
      }
      if (marker.icon.className) {
        icon.options.className = marker.icon.className;
      }
      var options = {icon:icon};
      if (marker.zIndexOffset) {
        options.zIndexOffset = marker.zIndexOffset;
      }
      lMarker = new L.Marker(latLng, options);
    }
    else {
      lMarker = new L.Marker(latLng);
    }

    if (marker.label) {
      lMarker.options.title = marker.label;
    }

    return lMarker;
  }

})(jQuery)