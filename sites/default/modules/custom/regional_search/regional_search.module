<?php
/**
 * @file
 * Regional Search module code.
 */

include_once 'regional_search.features.inc';

/**
 * Implements hook_init().
 */
function regional_search_init() {
  // Disable page caching for endpoint requests. We do our own resource-level
  // caching instead.
  if (drupal_match_path($_GET['q'], 'api/regional-search/*')) {
    drupal_page_is_cacheable(FALSE);
  }
}

/**
 * Implements hook_services_resources().
 */
function regional_search_services_resources() {
  $resources['regional_search_node'] = array(
    'operations' => array(),
    'actions' => array(),
    'targeted_actions' => array(),
  );

  $ops = &$resources['regional_search_node']['operations'];

  $ops['retrieve'] = array(
    'help' => 'Retrieve a node',
    'callback' => '_regional_search_node_resource_retrieve',
    'access callback' => 'user_access',
    'access arguments' => array('access regional search endpoint'),
    'args' => array(
      array(
        'name' => 'uuid',
        'type' => 'string',
        'description' => 'The UUID of the node.',
        'source' => array('path' => 0),
        'optional' => FALSE,
      ),
    ),
    'file' => array(
      'type' => 'inc',
      'module' => 'regional_search',
      'name' => 'includes/regional_search_node',
    ),
  );

  $ops['index'] = array(
    'help' => 'List all node UUIDs',
    'callback' => '_regional_search_node_resource_index',
    'access callback' => 'user_access',
    'access arguments' => array('access regional search endpoint'),
    'file' => array(
      'type' => 'inc',
      'module' => 'regional_search',
      'name' => 'includes/regional_search_node',
    ),
  );

  return $resources;
}

/**
 * Implements hook_node_update().
 */
function regional_search_node_update($node) {
  if ($node->type == 'document' || $node->type == 'product') {
    cache_clear_all("regional_search_resource_{$node->uuid}", 'cache');
  }
}

/**
 * Implements hook_permission().
 */
function regional_search_permission() {
  $permissions['access regional search endpoint'] = array(
    'title' => t('Access regional search endpoint'),
    'description' => t('Allow users to retrieve data from the regional search endpoint.'),
  );
  return $permissions;
}

/**
 * Implements hook_module_implements_alter().
 */
function regional_search_module_implements_alter(&$implementations, $hook) {
  // Our hook implementation needs to run after Sarnia since it defines all its
  // facets using that hook.
  if ($hook == 'facetapi_facet_info_alter' && isset($implementations['regional_search'])) {
    $group = $implementations['regional_search'];
    unset($implementations['regional_search']);
    $implementations['regional_search'] = $group;
  }
}


/**
 * Implements hook_facetapi_widgets().
 */
function regional_search_facetapi_widgets() {
  return array(
    'topic_tree' => array(
      'handler' => array(
        'label' => t('Topic Tree'),
        'class' => 'TopicTreeFacetapiWidget',
        'query types' => array('sarnia_term'),
      ),
    ),
  );
}

/**
 * Implements hook_facetapi_facet_info_alter().
 */
function regional_search_facetapi_facet_info_alter(array &$facet_info, array $searcher_info) {
  // The topics facet needs some processing in order to render hierarchy.
  if (isset($facet_info['vocab_topics_ss'])) {
    $facet_info['vocab_topics_ss']['hierarchy callback'] = '_regional_search_topics_facet_hierarchy_callback';
    $facet_info['vocab_topics_ss']['map callback'] = '_regional_search_topics_facet_map_callback';
  }
}

/**
 * Facet API "hierarchy callback" for topics facet.
 *
 * Extracts parents from Solr facet values.
 *
 * Example:
 *   "1/Agriculture, Forestry & Fishing/Food Security/" has the parent...
 *   "0/Agriculture, Forestry & Fishing/"
 *
 * Note that Sarnia includes quotes around everything for some odd reason.
 */
function _regional_search_topics_facet_hierarchy_callback($values) {
  $parents = array();

  foreach ($values as $value) {
    $raw = rtrim(trim($value, '"'), '/');
    $parts = explode('/', $raw);

    // Change the depth.
    $parts[0] = $parts[0] - 1;

    // Remove last element.
    array_pop($parts);

    // Re-assemble a string as we expect a parent to look.
    $parent_value = '"' . implode('/', $parts) . '/' . '"';

    $parent_key = array_search($parent_value, $values);
    if ($parent_key !== FALSE) {
      $parents[$value][] = $values[$parent_key];
    }
  }

  return $parents;
}

/**
 * Facet API "map callback" for topics facet.
 */
function _regional_search_topics_facet_map_callback($values) {
  $map = array();
  foreach ($values as $value) {
    $raw = rtrim(trim($value, '"'), '/');
    $parts = explode('/', $raw);
    $map[$value] = array_pop($parts);
  }
  return $map;
}

/**
 * Implements hook_views_pre_render().
 */
function regional_search_views_pre_render(&$view) {
  if ($view->name == 'regional_search') {
    drupal_add_js(drupal_get_path('module', 'regional_search') . '/regional_search.js');
  }
}

/**
 * Implements hook_leaflet_map_info_alter().
 */
function regional_search_leaflet_map_info_alter(&$maps) {
  foreach ($maps as &$map) {
    // Set default map center for all maps (Vanautu, by default).
    $map['center'] = array(
      'lat' => variable_get('leaflet_map_center_latitude', -16.2264),
      'lon' => variable_get('leaflet_map_center_longitude', -167.6864),
      'force' => variable_get('leaflet_map_center_force', 1),
    );
  }
}

/**
 * Implements hook_leaflet_map_prebuild_alter().
 */
function regional_search_leaflet_map_prebuild_alter($settings) {
  // JS overrides for Leaflet. Needs to come after leaflet.drupal.js.
  drupal_add_js(drupal_get_path('module', 'regional_search') . '/regional_search.leaflet.js', array('weight' => 100));
}