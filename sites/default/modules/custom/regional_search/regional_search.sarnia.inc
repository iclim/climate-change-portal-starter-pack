<?php
/**
 * @file
 * regional_search.sarnia.inc
 */

/**
 * Implements hook_default_sarnia_preset().
 */
function regional_search_default_sarnia_preset() {
  $export = array();

  $sarnia_index = new stdClass();
  $sarnia_index->disabled = FALSE; /* Edit this to true to make a default sarnia_index disabled initially */
  $sarnia_index->api_version = 1;
  $sarnia_index->machine_name = 'sarnia_regional_search_1';
  $sarnia_index->label = 'Regional Search 1';
  $sarnia_index->search_api_server = 'regional_search_1';
  $sarnia_index->search_api_index = 'sarnia_regional_search_1';
  $sarnia_index->id_field = 'id';
  $export['sarnia_regional_search_1'] = $sarnia_index;

  return $export;
}
