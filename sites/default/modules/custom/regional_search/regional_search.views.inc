<?php
/**
 * @file
 * Regional Search views integration.
 */

/**
 * Implements hook_views_plugins().
 */
function regional_search_views_plugins() {
  $plugins = array(
    'module' => 'regional_search',
    'style' => array(
      'regional_search_leaflet' => array(
        'title' => t('Regional Search Leaflet Map'),
        'help' => t('Displays a View as a Leaflet map without requiring Geofield.'),
        'path' => drupal_get_path('module', 'regional_search') . '/plugins/views',
        'handler' => 'regional_search_leaflet_views_plugin_style',
        'theme' => 'leaflet-map',
        'uses fields' => TRUE,
        'uses row plugin' => FALSE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'type' => 'normal',
        'even empty' => FALSE,
      ),
    ),
  );
  return $plugins;
}
