<?php
/**
 * @file
 * regional_search.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function regional_search_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'regional_search';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'search_api_index_sarnia_regional_search_1';
  $view->human_name = 'Regional Search';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Results';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'GO';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'url' => 'url',
    'title' => 'title',
    'solr_document_1' => 'solr_document_1',
    'solr_document_3' => 'solr_document_3',
    'solr_document_2' => 'solr_document_2',
    'solr_document' => 'solr_document',
    'solr_document_6' => 'solr_document_6',
  );
  /* Header: Global: Result summary */
  $handler->display->display_options['header']['result']['id'] = 'result';
  $handler->display->display_options['header']['result']['table'] = 'views';
  $handler->display->display_options['header']['result']['field'] = 'result';
  $handler->display->display_options['header']['result']['content'] = '<p><b>@start</b> - <b>@end</b> of <b>@total</b></p>';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No results. Try to broaden your search.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  /* Field: Regional Search 1: url */
  $handler->display->display_options['fields']['url']['id'] = 'url';
  $handler->display->display_options['fields']['url']['table'] = 'search_api_index_sarnia_regional_search_1';
  $handler->display->display_options['fields']['url']['field'] = 'url';
  $handler->display->display_options['fields']['url']['label'] = '';
  $handler->display->display_options['fields']['url']['exclude'] = TRUE;
  $handler->display->display_options['fields']['url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['url']['link_to_entity'] = 0;
  /* Field: Regional Search 1: title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'search_api_index_sarnia_regional_search_1';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = '[url]';
  $handler->display->display_options['fields']['title']['alter']['absolute'] = TRUE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_entity'] = 0;
  /* Field: Regional Search 1: Data: document_publication_year_s */
  $handler->display->display_options['fields']['solr_document_1']['id'] = 'solr_document_1';
  $handler->display->display_options['fields']['solr_document_1']['table'] = 'search_api_index_sarnia_regional_search_1';
  $handler->display->display_options['fields']['solr_document_1']['field'] = 'solr_document';
  $handler->display->display_options['fields']['solr_document_1']['label'] = '';
  $handler->display->display_options['fields']['solr_document_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['solr_document_1']['element_type'] = 'span';
  $handler->display->display_options['fields']['solr_document_1']['element_class'] = 'label label-info doc-label';
  $handler->display->display_options['fields']['solr_document_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['solr_document_1']['solr_property'] = 'document_publication_year_s';
  $handler->display->display_options['fields']['solr_document_1']['formatter'] = '0';
  /* Field: Regional Search 1: Data: project_end_year_s */
  $handler->display->display_options['fields']['solr_document_3']['id'] = 'solr_document_3';
  $handler->display->display_options['fields']['solr_document_3']['table'] = 'search_api_index_sarnia_regional_search_1';
  $handler->display->display_options['fields']['solr_document_3']['field'] = 'solr_document';
  $handler->display->display_options['fields']['solr_document_3']['label'] = '';
  $handler->display->display_options['fields']['solr_document_3']['exclude'] = TRUE;
  $handler->display->display_options['fields']['solr_document_3']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['solr_document_3']['alter']['text'] = ' - [solr_document_3]';
  $handler->display->display_options['fields']['solr_document_3']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['solr_document_3']['solr_property'] = 'project_end_year_s';
  $handler->display->display_options['fields']['solr_document_3']['formatter'] = '0';
  /* Field: Regional Search 1: Data: project_start_year_s */
  $handler->display->display_options['fields']['solr_document_2']['id'] = 'solr_document_2';
  $handler->display->display_options['fields']['solr_document_2']['table'] = 'search_api_index_sarnia_regional_search_1';
  $handler->display->display_options['fields']['solr_document_2']['field'] = 'solr_document';
  $handler->display->display_options['fields']['solr_document_2']['label'] = '';
  $handler->display->display_options['fields']['solr_document_2']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['solr_document_2']['alter']['text'] = '[solr_document_2][solr_document_3]';
  $handler->display->display_options['fields']['solr_document_2']['element_type'] = 'span';
  $handler->display->display_options['fields']['solr_document_2']['element_class'] = 'label label-info doc-label';
  $handler->display->display_options['fields']['solr_document_2']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['solr_document_2']['empty'] = '[solr_document_1]';
  $handler->display->display_options['fields']['solr_document_2']['solr_property'] = 'project_start_year_s';
  $handler->display->display_options['fields']['solr_document_2']['formatter'] = '0';
  /* Field: Regional Search 1: Data: project_status_s */
  $handler->display->display_options['fields']['solr_document_6']['id'] = 'solr_document_6';
  $handler->display->display_options['fields']['solr_document_6']['table'] = 'search_api_index_sarnia_regional_search_1';
  $handler->display->display_options['fields']['solr_document_6']['field'] = 'solr_document';
  $handler->display->display_options['fields']['solr_document_6']['label'] = '';
  $handler->display->display_options['fields']['solr_document_6']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['solr_document_6']['alter']['text'] = 'Status: [solr_document_6]';
  $handler->display->display_options['fields']['solr_document_6']['element_type'] = 'span';
  $handler->display->display_options['fields']['solr_document_6']['element_class'] = 'label label-info doc-label';
  $handler->display->display_options['fields']['solr_document_6']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['solr_document_6']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['solr_document_6']['solr_property'] = 'project_status_s';
  $handler->display->display_options['fields']['solr_document_6']['formatter'] = '0';
  /* Field: Regional Search 1: Data: source_ss */
  $handler->display->display_options['fields']['solr_document']['id'] = 'solr_document';
  $handler->display->display_options['fields']['solr_document']['table'] = 'search_api_index_sarnia_regional_search_1';
  $handler->display->display_options['fields']['solr_document']['field'] = 'solr_document';
  $handler->display->display_options['fields']['solr_document']['label'] = '';
  $handler->display->display_options['fields']['solr_document']['element_type'] = 'span';
  $handler->display->display_options['fields']['solr_document']['element_class'] = 'label label-primary doc-label';
  $handler->display->display_options['fields']['solr_document']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['solr_document']['solr_property'] = 'source_ss';
  $handler->display->display_options['fields']['solr_document']['formatter'] = '0';
  $handler->display->display_options['fields']['solr_document']['is_multivalue'] = TRUE;
  $handler->display->display_options['fields']['solr_document']['multivalue']['count'] = '0';
  $handler->display->display_options['fields']['solr_document']['multivalue']['list_type'] = 'separator';
  /* Field: Regional Search 1: description */
  $handler->display->display_options['fields']['description']['id'] = 'description';
  $handler->display->display_options['fields']['description']['table'] = 'search_api_index_sarnia_regional_search_1';
  $handler->display->display_options['fields']['description']['field'] = 'description';
  $handler->display->display_options['fields']['description']['label'] = '';
  $handler->display->display_options['fields']['description']['alter']['max_length'] = '500';
  $handler->display->display_options['fields']['description']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['description']['element_type'] = 'p';
  $handler->display->display_options['fields']['description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['description']['link_to_entity'] = 0;
  /* Field: Regional Search 1: Data: project_implementing_countries_ss */
  $handler->display->display_options['fields']['solr_document_4']['id'] = 'solr_document_4';
  $handler->display->display_options['fields']['solr_document_4']['table'] = 'search_api_index_sarnia_regional_search_1';
  $handler->display->display_options['fields']['solr_document_4']['field'] = 'solr_document';
  $handler->display->display_options['fields']['solr_document_4']['label'] = 'Implementing Countries';
  $handler->display->display_options['fields']['solr_document_4']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['solr_document_4']['empty_zero'] = TRUE;
  $handler->display->display_options['fields']['solr_document_4']['solr_property'] = 'project_implementing_countries_ss';
  $handler->display->display_options['fields']['solr_document_4']['formatter'] = '0';
  $handler->display->display_options['fields']['solr_document_4']['is_multivalue'] = TRUE;
  $handler->display->display_options['fields']['solr_document_4']['multivalue']['count'] = '0';
  $handler->display->display_options['fields']['solr_document_4']['multivalue']['list_type'] = 'separator';
  /* Field: Regional Search 1: Data: download_links */
  $handler->display->display_options['fields']['solr_document_5']['id'] = 'solr_document_5';
  $handler->display->display_options['fields']['solr_document_5']['table'] = 'search_api_index_sarnia_regional_search_1';
  $handler->display->display_options['fields']['solr_document_5']['field'] = 'solr_document';
  $handler->display->display_options['fields']['solr_document_5']['label'] = 'Item Downloads';
  $handler->display->display_options['fields']['solr_document_5']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['solr_document_5']['alter']['path'] = '[solr_document_5]';
  $handler->display->display_options['fields']['solr_document_5']['alter']['external'] = TRUE;
  $handler->display->display_options['fields']['solr_document_5']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['solr_document_5']['empty_zero'] = TRUE;
  $handler->display->display_options['fields']['solr_document_5']['solr_property'] = 'download_links';
  $handler->display->display_options['fields']['solr_document_5']['formatter'] = '0';
  $handler->display->display_options['fields']['solr_document_5']['is_multivalue'] = TRUE;
  $handler->display->display_options['fields']['solr_document_5']['multivalue']['count'] = '0';
  /* Sort criterion: Search: Relevance */
  $handler->display->display_options['sorts']['search_api_relevance']['id'] = 'search_api_relevance';
  $handler->display->display_options['sorts']['search_api_relevance']['table'] = 'search_api_index_sarnia_regional_search_1';
  $handler->display->display_options['sorts']['search_api_relevance']['field'] = 'search_api_relevance';
  $handler->display->display_options['sorts']['search_api_relevance']['order'] = 'DESC';
  /* Sort criterion: Regional Search 1: Data: document_publication_year_s */
  $handler->display->display_options['sorts']['solr_document']['id'] = 'solr_document';
  $handler->display->display_options['sorts']['solr_document']['table'] = 'search_api_index_sarnia_regional_search_1';
  $handler->display->display_options['sorts']['solr_document']['field'] = 'solr_document';
  $handler->display->display_options['sorts']['solr_document']['order'] = 'DESC';
  $handler->display->display_options['sorts']['solr_document']['solr_property'] = 'document_publication_year_s';
  /* Sort criterion: Regional Search 1: Data: project_start_year_s */
  $handler->display->display_options['sorts']['solr_document_1']['id'] = 'solr_document_1';
  $handler->display->display_options['sorts']['solr_document_1']['table'] = 'search_api_index_sarnia_regional_search_1';
  $handler->display->display_options['sorts']['solr_document_1']['field'] = 'solr_document';
  $handler->display->display_options['sorts']['solr_document_1']['order'] = 'DESC';
  $handler->display->display_options['sorts']['solr_document_1']['solr_property'] = 'project_start_year_s';
  /* Filter criterion: Search: Fulltext search */
  $handler->display->display_options['filters']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['table'] = 'search_api_index_sarnia_regional_search_1';
  $handler->display->display_options['filters']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['exposed'] = TRUE;
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator_id'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['label'] = 'Keywords';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['identifier'] = 'keywords';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['search_api_views_fulltext']['fields'] = array(
    'description' => 'description',
    'title' => 'title',
    'title_suggest' => 'title_suggest',
  );

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $handler->display->display_options['exposed_block'] = TRUE;
  $handler->display->display_options['inherit_panels_path'] = '1';

  /* Display: Context */
  $handler = $view->new_display('ctools_context', 'Context', 'ctools_context_1');
  $handler->display->display_options['style_plugin'] = 'ctools_context';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['inherit_panels_path'] = '1';

  /* Display: Map Context */
  $handler = $view->new_display('ctools_context', 'Map Context', 'map_context');
  $handler->display->display_options['style_plugin'] = 'ctools_context';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Search: Fulltext search */
  $handler->display->display_options['filters']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['table'] = 'search_api_index_sarnia_regional_search_1';
  $handler->display->display_options['filters']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['exposed'] = TRUE;
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator_id'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['label'] = 'Keywords';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['identifier'] = 'keywords';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['search_api_views_fulltext']['fields'] = array(
    'description' => 'description',
    'title' => 'title',
    'title_suggest' => 'title_suggest',
  );
  /* Filter criterion: Regional Search 1: Data */
  $handler->display->display_options['filters']['solr_document']['id'] = 'solr_document';
  $handler->display->display_options['filters']['solr_document']['table'] = 'search_api_index_sarnia_regional_search_1';
  $handler->display->display_options['filters']['solr_document']['field'] = 'solr_document';
  $handler->display->display_options['filters']['solr_document']['operator'] = 'not empty';
  $handler->display->display_options['filters']['solr_document']['solr_property'] = 'project_sites_lat_ss';
  $handler->display->display_options['filters']['solr_document']['solr_property_expose'] = 0;
  /* Filter criterion: Regional Search 1: Data */
  $handler->display->display_options['filters']['solr_document_1']['id'] = 'solr_document_1';
  $handler->display->display_options['filters']['solr_document_1']['table'] = 'search_api_index_sarnia_regional_search_1';
  $handler->display->display_options['filters']['solr_document_1']['field'] = 'solr_document';
  $handler->display->display_options['filters']['solr_document_1']['operator'] = 'not empty';
  $handler->display->display_options['filters']['solr_document_1']['solr_property'] = 'project_sites_lng_ss';
  $handler->display->display_options['filters']['solr_document_1']['solr_property_expose'] = 0;

  /* Display: Map Pane */
  $handler = $view->new_display('panel_pane', 'Map Pane', 'map_pane');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'regional_search_leaflet';
  $handler->display->display_options['style_options']['entity_type'] = '0';
  $handler->display->display_options['style_options']['name_field'] = 'title';
  $handler->display->display_options['style_options']['map'] = 'esri-world_topo_map';
  $handler->display->display_options['style_options']['height'] = '550';
  $handler->display->display_options['style_options']['hide_empty'] = 0;
  $handler->display->display_options['style_options']['zoom']['initialZoom'] = '2';
  $handler->display->display_options['style_options']['zoom']['minZoom'] = '2';
  $handler->display->display_options['style_options']['zoom']['maxZoom'] = '14';
  $handler->display->display_options['style_options']['vector_display']['stroke'] = 0;
  $handler->display->display_options['style_options']['vector_display']['fill'] = 0;
  $handler->display->display_options['style_options']['vector_display']['clickable'] = 0;
  $handler->display->display_options['style_options']['data_source_lat'] = 'solr_document_1';
  $handler->display->display_options['style_options']['data_source_lon'] = 'solr_document_2';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['exposed_block'] = TRUE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Regional Search 1: url */
  $handler->display->display_options['fields']['url']['id'] = 'url';
  $handler->display->display_options['fields']['url']['table'] = 'search_api_index_sarnia_regional_search_1';
  $handler->display->display_options['fields']['url']['field'] = 'url';
  $handler->display->display_options['fields']['url']['label'] = '';
  $handler->display->display_options['fields']['url']['exclude'] = TRUE;
  $handler->display->display_options['fields']['url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['url']['link_to_entity'] = 0;
  /* Field: Regional Search 1: title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'search_api_index_sarnia_regional_search_1';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = '[url]';
  $handler->display->display_options['fields']['title']['alter']['absolute'] = TRUE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_entity'] = 0;
  /* Field: Regional Search 1: description */
  $handler->display->display_options['fields']['description']['id'] = 'description';
  $handler->display->display_options['fields']['description']['table'] = 'search_api_index_sarnia_regional_search_1';
  $handler->display->display_options['fields']['description']['field'] = 'description';
  $handler->display->display_options['fields']['description']['label'] = '';
  $handler->display->display_options['fields']['description']['alter']['max_length'] = '300';
  $handler->display->display_options['fields']['description']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['description']['link_to_entity'] = 0;
  /* Field: Regional Search 1: Data: project_sites_lat_ss */
  $handler->display->display_options['fields']['solr_document_1']['id'] = 'solr_document_1';
  $handler->display->display_options['fields']['solr_document_1']['table'] = 'search_api_index_sarnia_regional_search_1';
  $handler->display->display_options['fields']['solr_document_1']['field'] = 'solr_document';
  $handler->display->display_options['fields']['solr_document_1']['label'] = 'Latitude';
  $handler->display->display_options['fields']['solr_document_1']['solr_property'] = 'project_sites_lat_ss';
  $handler->display->display_options['fields']['solr_document_1']['formatter'] = '0';
  $handler->display->display_options['fields']['solr_document_1']['is_multivalue'] = TRUE;
  $handler->display->display_options['fields']['solr_document_1']['multivalue']['count'] = '1';
  $handler->display->display_options['fields']['solr_document_1']['multivalue']['list_type'] = 'separator';
  /* Field: Regional Search 1: Data: project_sites_lng_ss */
  $handler->display->display_options['fields']['solr_document_2']['id'] = 'solr_document_2';
  $handler->display->display_options['fields']['solr_document_2']['table'] = 'search_api_index_sarnia_regional_search_1';
  $handler->display->display_options['fields']['solr_document_2']['field'] = 'solr_document';
  $handler->display->display_options['fields']['solr_document_2']['label'] = 'Longitude';
  $handler->display->display_options['fields']['solr_document_2']['solr_property'] = 'project_sites_lng_ss';
  $handler->display->display_options['fields']['solr_document_2']['formatter'] = '0';
  $handler->display->display_options['fields']['solr_document_2']['is_multivalue'] = TRUE;
  $handler->display->display_options['fields']['solr_document_2']['multivalue']['count'] = '1';
  $handler->display->display_options['fields']['solr_document_2']['multivalue']['list_type'] = 'separator';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Search: Fulltext search */
  $handler->display->display_options['filters']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['table'] = 'search_api_index_sarnia_regional_search_1';
  $handler->display->display_options['filters']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['exposed'] = TRUE;
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator_id'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['label'] = 'Keywords';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['identifier'] = 'keywords';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['search_api_views_fulltext']['fields'] = array(
    'description' => 'description',
    'title' => 'title',
    'title_suggest' => 'title_suggest',
  );
  /* Filter criterion: Regional Search 1: Data */
  $handler->display->display_options['filters']['solr_document']['id'] = 'solr_document';
  $handler->display->display_options['filters']['solr_document']['table'] = 'search_api_index_sarnia_regional_search_1';
  $handler->display->display_options['filters']['solr_document']['field'] = 'solr_document';
  $handler->display->display_options['filters']['solr_document']['operator'] = 'not empty';
  $handler->display->display_options['filters']['solr_document']['solr_property'] = 'project_sites_lat_ss';
  $handler->display->display_options['filters']['solr_document']['solr_property_expose'] = 0;
  /* Filter criterion: Regional Search 1: Data */
  $handler->display->display_options['filters']['solr_document_1']['id'] = 'solr_document_1';
  $handler->display->display_options['filters']['solr_document_1']['table'] = 'search_api_index_sarnia_regional_search_1';
  $handler->display->display_options['filters']['solr_document_1']['field'] = 'solr_document';
  $handler->display->display_options['filters']['solr_document_1']['operator'] = 'not empty';
  $handler->display->display_options['filters']['solr_document_1']['solr_property'] = 'project_sites_lng_ss';
  $handler->display->display_options['filters']['solr_document_1']['solr_property_expose'] = 0;
  $handler->display->display_options['inherit_panels_path'] = '1';
  $export['regional_search'] = $view;

  return $export;
}
