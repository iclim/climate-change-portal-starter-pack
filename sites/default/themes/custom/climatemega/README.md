Climate Megamenu Theme
======================


Custom-built dependencies
-------------------------

### jquery ui

The drupal core version has some choices which are laborious to override,
so a custom built version is included in this theme. It is generated using
the themeroller function on thier website. Using this url you can access
the form with all the custom settings in place, which might be useful

https://jqueryui.com/themeroller/#!zThemeParams=5d000001000006000000000000003d8888d844329a8dfe02723de3e5701bdb5b7894f110570e46729513fcc94c4c8011ca60c6127df468119496e68486a4257d297e95eb28dbc172a4da51ebb851e0d26ad103d44ff46b0f6a1a41ac2532afa69b9f8259e4a5b5895e8c0ad642ed90a1eaef249050d4b314a2a4cbde974daa84b50d49e453b1698eb2b5e37395135074fa775e66d0216a6e535864c20c6172667cd36c4bacac07d7cb3427a7115d1bb4c2d9028bd065d0e3f9d7d7b5a60ff22f83f0a0c5418dd2e1625902de9bc98f6a5618fa57b84e4461c2c9b0dc3e04855aa0e9b763cb372c080769a7544308a97a831d7475660e5e2024fd0d75214b13e742869accb1e55f202af7bf8ca4d16aa6c62a6bb0d8f849e1ad0fff696f1c95888a79bd330c2fc84d60735bca2db436aa9ba881aa83b123dcba37b47c74e244166255ab13539dfcf0d89c1fbf2d1c14041ff8a62fec947681f144fe4f7af5879778334cee480b9f5fba1b091d5b8b573450e1b6e85edb260ab667f36ac55658fde635f31177ea9f409abb5c2c74ccdf9ea45ea5e9214c8a24120c529e813c45b21b3caac9ef9f362fb581da455c2f39d66ee6e3f0a48c7d76365a8161fab252c0c3e581c264fcfca7c60a

### bootstrap

A custom built bootstrap is included. Its variable choices are available in
sass format in the file

sass/_bootstrap_variables.scss


Compiling the stylesheets
-------------------------

This theme uses SASS, not SCSS.

To compile the css files, from the root of the theme folder run
(the source map helps with debugging, but should not be used in production)

>sass --watch sass:stylesheets --sourcemap=none


