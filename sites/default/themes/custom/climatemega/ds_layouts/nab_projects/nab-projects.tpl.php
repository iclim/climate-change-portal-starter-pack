<?php

/**
 * @file
 * nab 3 col template
 */
?>
<<?php print $layout_wrapper; print $layout_attributes; ?> class="nab-projects <?php print $classes;?> clearfix">

  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <div class="group-left<?php print isset($left_classes) ? $left_classes : ''; ?>">
    <div class="group-head">
      <?php print $top; ?>
    </div>

    <div class="group-top clearfix">
      <?php if($top_left) : ?>
        <div class="group-top-left">
          <?php print $top_left; ?>
        </div>
      <?php endif; ?>

      <?php if($top_middle) : ?>
        <div class="group-top-middle">
          <?php print $top_middle; ?>
        </div>
      <?php endif; ?>

      
    </div>
    <div class="group-bottom clearfix">
      <div class="group-bottom-left">
        <?php print $bottom_left; ?>
      </div>
      <div class="group-bottom-middle">
        <?php print $bottom_middle; ?>
      </div>
    </div>
  </div>

  <<?php print $right_wrapper ?> class="group-right<?php print isset($right_classes) ? $right_classes : '';?>">
    <?php print $right; ?>
  </<?php print $right_wrapper ?>>
  

</<?php print $layout_wrapper ?>>

<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
