<?php
function ds_nab_projects() {
  return array(
    'label' => t('NAB Project Layout'),
    'regions' => array(
      'top' => t('Head'),
      'top_left' => t('Top Left'),
      'top_middle' => t('Top Middle'),
      'bottom_left' => t('Bottom Left'),
      'bottom_middle' => t('Bottom Middle'),
      'right' => t('Right'),
    ),
    // Add this line if there is a default css file.
    'css' => TRUE,
    // Add this line if you're using DS 2.x for icon preview
    'image' => TRUE,
  );
}
