Mega = (function() {
  function toggle(classselector, direction) {
    var inc,
        mmenus = document.getElementsByClassName(classselector);
    if (typeof(direction) === 'undefined') {
      direction = 0;
    }
    if (typeof(mmenus) !== 'undefined') {
      for (i = 0; i < mmenus.length; i++) {
        mm = mmenus[i];
        if (
          (mm.className.indexOf('expanded') >= 0) ||
            (direction === -1)
        ) {
          mm.className = mm.className.replace(' expanded', '');
        }
        else {
          if (direction >= 0) {
            mm.className += ' expanded';
          }
        }
      }
    }
  }
  function expander(e) {
    var inc,
        direction = 0,
        targets = ['megamenu', 'megacolumn'];
    switch (e.type) {
    case 'mouseenter':
      direction = 1;
      break;
    case 'mouseleave':
      direction = -1;
      break;
    }
    for (inc = 0; inc < targets.length; inc++) {
      toggle(targets[inc], direction);
    }
  }
  function item_clicker(e) {
    var mm,
        anchors,
        inc,
        inc2,
        classes;
    e.stopPropagation();
    // activation behaviour on mega menu items
    mm = document.getElementsByClassName('megamenu');
    for (inc = 0; inc < mm.length; inc++) {
      anchors = mm[inc].getElementsByTagName('a');
      for (inc2 = 0; inc2 < anchors.length; inc2++) {
        classes = anchors[inc2].className
        if (!classes.includes('inactive')) {
          classes += ' inactive';
          anchors[inc2].className = classes;
        }
      }
    }
    classes = e.target.className
    if (classes.includes('inactive')) {
      classes = classes.replace('inactive', '');
      e.target.className = classes;
    }

  }
  function activate() {
    var inc,
        inc2,
        inc3,
        inc4,
        activations,
        anchors,
        eventmap = [
          ['megatoggle', ['click']]
        ];
    for (inc=0; inc < eventmap.length; inc++) {
      activations = document.getElementsByClassName(eventmap[inc][0]);
      for (inc2 = 0; inc2 < activations.length; inc2++) {
        for (inc3 = 0; inc3 < eventmap[inc][1].length; inc3++) {
          activations[inc2].addEventListener(
            eventmap[inc][1][inc3], expander, false
          );
        }
        anchors = activations[inc2].getElementsByTagName('a');
        for (inc4=0; inc4 < anchors.length; inc4++) {
          anchors[inc4].addEventListener(
            "click", item_clicker
          );
        }
      }
    }
  }
  return {
    activate: activate
  };
}());
Mega.activate();
