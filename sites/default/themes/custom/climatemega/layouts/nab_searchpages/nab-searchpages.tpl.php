<?php
/**
 * @file
 * Template for a 2 column panel layout.
 *
 * This template provides a two column panel display layout, with
 * each column roughly equal in width.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['left']: Content in the left column.
 *   - $content['right']: Content in the right column.
 */
?>
<div class="panel-display nab-searchpages clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <div class="panel-panel panel-header">
    <div class="inside"><?php print $content['header']; ?></div>
  </div>

  <?php if ($content['top_left'] || $content['top_middle'] || $content['top_right']): ?>
    <div class="topcols-wrapper clearfix">
      <div class="panel-panel panel-col-first">
        <div class="inside"><?php print $content['top_left']; ?></div>
      </div>

      <div class="panel-panel panel-col">
        <div class="inside"><?php print $content['top_middle']; ?></div>
      </div>

      <div class="panel-panel panel-col-last">
        <div class="inside"><?php print $content['top_right']; ?></div>
      </div>
    </div>
  <?php endif ?>

  <div class="centercols-wrapper clearfix">
    <div class="panel-panel panel-col-first">
      <button class="search-filter-button btn" data-toggle="collapse" data-target="#search-filter-collapsable">Show / Hide Filters</button>
      <div id="search-filter-collapsable" class="collapse">
       <div class="inside"><?php print $content['left']; ?></div>
      </div>
    </div>

    <div class="panel-panel panel-col-last">
      <div class="inside"><?php print $content['right']; ?></div>
    </div>
  </div>

  <?php if ($content['bottom_left'] || $content['bottom_middle'] || $content['bottom_right']): ?>
    <div class="bottomcols-wrapper">
      <div class="panel-panel panel-col-first">
        <div class="inside"><?php print $content['bottom_left']; ?></div>
      </div>

      <div class="panel-panel panel-col">
        <div class="inside"><?php print $content['bottom_middle']; ?></div>
      </div>

      <div class="panel-panel panel-col-last">
        <div class="inside"><?php print $content['bottom_right']; ?></div>
      </div>
    </div>
  <?php endif ?>
</div>
