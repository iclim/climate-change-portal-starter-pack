<?php

// Plugin definition
$plugin = array(
  'title' => t('NAB Searchpages'),
  'category' => t('NAB'),
  'icon' => 'nab_searchpages.png',
  'theme' => 'nab-searchpages',
  'css' => 'nab_searchpages.css',
  'regions' => array(
    'header' => t('Header'),
    'top_left' => t('Top Left'),
    'top_middle' => t('Top Middle'),
    'top_right' => t('Top Right'),
    'left' => t('Left side'),
    'right' => t('Right side'),
    'bottom_left' => t('Bottom Left'),
    'bottom_middle' => t('Bottom Middle'),
    'bottom_right' => t('Bottom Right'),
  ),
);
