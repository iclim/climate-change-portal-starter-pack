<div class="panel-display nab-threecol-stacked clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

  <?php if ($content['top']): ?>
    <div class="panel-panel panel-col-top col-sm-8 col-sm-offset-2">
      <div class="inside"><?php print $content['top']; ?></div>
    </div>
  <?php endif ?>
  

  <div class="centercols-wrapper">
    <div class="panel-panel panel-col-first col-xs-4 col-centered">
      <div class="inside"><?php print $content['left']; ?></div>
    </div>

    <div class="panel-panel panel-col col-xs-4 col-centered">
      <div class="inside"><?php print $content['middle']; ?></div>
    </div>

    <div class="panel-panel panel-col-last col-xs-4 col-centered">
      <div class="inside"><?php print $content['right']; ?></div>
    </div>
  </div>

  <?php if ($content['bottom']): ?>
    <div class="panel-panel panel-col-bottom col-sm-12">
      <div class="inside"><?php print $content['bottom']; ?></div>
    </div>
  <?php endif ?>

  <?php if ($content['bottom_left'] || $content['bottom_middle'] || $content['bottom_right']): ?>
    <div class="bottomcols-wrapper">
      <div class="panel-panel panel-col-first col-xs-4 col-centered">
        <div class="inside"><?php print $content['bottom_left']; ?></div>
      </div>

      <div class="panel-panel panel-col col-xs-4 col-centered">
        <div class="inside"><?php print $content['bottom_middle']; ?></div>
      </div>

      <div class="panel-panel panel-col-last col-xs-4 col-centered">
        <div class="inside"><?php print $content['bottom_right']; ?></div>
      </div>
    </div>
  <?php endif ?>
</div>
