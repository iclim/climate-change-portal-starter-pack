<?php

// Plugin definition
$plugin = array(
  'title' => t('NAB Three column double stacked'),
  'category' => t('NAB'),
  'icon' => 'nab_threecol_stacked.png',
  'theme' => 'nab_threecol_stacked',
  'css' => 'nab_threecol_stacked.css',
  'regions' => array(
    'banner_left' => t('Banner left'),
    'banner_right' => t('Banner right'),
    'top' => t('Top'),
    'left' => t('Left side'),
    'middle' => t('Middle column'),
    'right' => t('Right side'),
    'bottom' => t('Bottom'),
    'bottom_left' => t('Bottom left side'),
    'bottom_middle' => t('Bottom middle column'),
    'bottom_right' => t('Bottom right side'),
  ),
);
