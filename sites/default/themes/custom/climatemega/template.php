<?php


/**
 * @file
 * template.php
 */

/**
 * Implementation of theme_preprocess_page
 * @param type $variables
 * @param type $hook
 */
//function climatemega_preprocess(&$variables, $hook) {
//     drupal_set_message($hook);
//}
function climatemega_preprocess_page(&$variables, $hook) {  // Add theme suggestion for all content types
    // if this is a panel page, add template suggestions

  if ($panel_page = page_manager_get_current_page()) {
    // add a generic suggestion for all panel pages
    $variables['theme_hook_suggestions'][] = 'page__panel';

    // add the panel page machine name to the template suggestions
    $variables['theme_hook_suggestions'][] = 'page__' . $panel_page['name'];

    //add a body class for good measure
    $body_classes[] = 'page-panel';
  }

  if (isset($variables['node'])) {
    if ($variables['node']->type != '') {
      $variables['theme_hook_suggestions'][] = 'page__node__' . $variables['node']->type;
    }
  }


  $mega_menus = array(
    'menu-projects',
    'menu-resources',
    'menu-news-and-events',
    'menu-about-us'
  );

  foreach ($mega_menus as $id) {
    $menu = menu_load($id);
    $tree = menu_tree_all_data($id);
    
    //turn off bootstrap rendering for the children links
    foreach ($tree as &$item) {
      $item['link']['no_bootstrap'] = 1;
    }

    $tree_renderable = menu_tree_output($tree);

    //turn off bootstrap rendering for the menu wrapper too
    $tree_renderable['#no_bootstrap'] = 1;

    $variables['megamenu'][$menu['title']] = render($tree_renderable);
  }

  
  $tree = menu_tree_all_data('menu-footer-menu');
  foreach ($tree as &$item) {
    $item['link']['no_bootstrap'] = 1;
  }
  $tree_renderable = menu_tree_output($tree);
  $tree_renderable['#no_bootstrap'] = 1;
  $variables['footer_menu'] = render($tree_renderable);
}


function climatemega_facetapi_deactivate_widget($variables) {
  // $str = '<a class="btn btn-default btn-sm remove dropdown-toggle" href="?">
	// 	<span class="glyphicon glyphicon-remove"></span>
	// 	<span class="sr-only">Remove constraint</span>
	// </a>';
	// return $str;

  	return '(x)';
}

function climatemega_facetapi_accessible_markup($variables) {
  // print '<pre>';
	// print_r($variables);
	// print '</pre>';
  $vars = array('@text' => $variables['text']);
  $text = ($variables['active']) ? t('Remove @text filter', $vars) : t('Apply @text filter', $vars);
  // Add spaces before and after the text, since other content may be displayed
  // inline with this and we don't want the words to run together. However, the
  // spaces must be inside the <span> so as not to disrupt the layout for
  // sighted users.
  return '<span class="foo element-invisible"> ' . $text . ' </span>';
 //  $str = '<span class="constraint-value btn btn-sm btn-default btn-disabled">
	// 	<span class="filterValue" title="'.$variables['text'].'">'.$variables['text'].'</span>
	// </span>';
	// return $str;

}

function climatemega_facetapi_link_active($variables) {
// print '<pre>';
// 	print_r($variables);
// 	print '</pre>';
  // Sanitizes the link text if necessary.
  $sanitize = empty($variables['options']['html']);
  $link_text = ($sanitize) ? check_plain($variables['text']) : $variables['text'];

  // Theme function variables fro accessible markup.
  // @see http://drupal.org/node/1316580
  $accessible_vars = array(
    'text' => $variables['text'],
    'active' => TRUE,
  );

  // Builds link, passes through t() which gives us the ability to change the
  // position of the widget on a per-language basis.
  $replacements = array(
    '!facetapi_deactivate_widget' => theme('facetapi_deactivate_widget', $variables),
    '!facetapi_accessible_markup' => theme('facetapi_accessible_markup', $accessible_vars),
  );
  $variables['text'] = t('!facetapi_deactivate_widget !facetapi_accessible_markup', $replacements);
  $variables['options']['html'] = TRUE;

  return theme_link($variables) . $link_text;
}

function climatemega_current_search_text(array $variables) {
  // Initializes output, sanitizes text if necessary.
  $sanitize = empty($variables['options']['html']);
  $output = ($sanitize) ? check_plain($variables['text']) : $variables['text'];

  // Adds wrapper markup and CSS classes.
  if ($variables['wrapper'] && $variables['element']) {
    $attributes = array('class' => $variables['class']);
    $element = check_plain($variables['element']);
    $output = '<' . $element . drupal_attributes($attributes) . '>' . $output . '</' . $element . '>';
  }
  // print '<pre>';
  // print $output;
  // print '</pre>';
  return $output;
}

function climatemega_current_search_item_wrapper(array $variables) {
  $element = $variables['element'];
  $attributes = array(
    'class' => array(
      'current-search-item arve',
      drupal_html_class('foo1 current-search-item-' . $element['#current_search_id']),
      drupal_html_class('foo2 current-search-item-' . $element['#current_search_name']),
    ),
  );
  return '<div' . drupal_attributes($attributes) . '>' . $element['#children'] . '</div>';
}
