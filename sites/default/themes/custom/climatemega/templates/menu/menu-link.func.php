<?php


/**
 * Returns HTML for a menu link and submenu.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: Structured array data for a menu link.
 *
 * @return string
 *   The constructed HTML.
 *
 * @see theme_menu_link()
 *
 * @ingroup theme_functions
 */
function climatemega_menu_link(array $variables) {

  if (!empty($variables['element']['#original_link']['no_bootstrap'])) {
    return theme_menu_link($variables);
  }

  //Fallback to bootstrap links
  return bootstrap_menu_link($variables);
}


