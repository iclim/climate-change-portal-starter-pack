<?php

/**
 * Returns HTML for a wrapper for a menu sub-tree.
 *
 * @param array $variables
 *   An associative array containing:
 *   - tree: An HTML string containing the tree's items.
 *
 * @return string
 *   The constructed HTML.
 *
 * @see template_preprocess_menu_tree()
 * @see theme_menu_tree()
 *
 * @ingroup theme_functions
 */
function climatemega_menu_tree(&$variables) {
  
  //for the mega menu links, dont do the bootstrap things
  if (!empty($variables['#tree']['#no_bootstrap'])) {
    return theme_menu_tree($variables);
  }


  //Fallback to bootstrap links
  return bootstrap_menu_tree($variables);
}
