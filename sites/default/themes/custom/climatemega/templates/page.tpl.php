<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */

// print '<pre>';
// var_dump($page['content']['system_main']['nodes'][57]['field_flag_image']);
// print '</pre>';

//$navmenu =  menu_tree_all_data('main-menu');

if (!isset($content_column_class)):
  $content_column_class = ' class=""';
endif;
// $breadcrumb = array();
// $breadcrumb[] = l(t('Home'), '<front>');
// $breadcrumb = '<div class="breadcrumb">'. implode(' &gt; ', $breadcrumb)
// . '</div>';
drupal_add_css(
  drupal_get_path('theme', 'climatemega').'/stylesheets/roboto.css',
  array('every_page' => TRUE)
);
drupal_add_css(
  drupal_get_path('theme', 'climatemega').'/stylesheets/bootstrap-custom.css',
  array('every_page' => TRUE)
);
drupal_add_js(
  drupal_get_path('theme', 'climatemega').'/js/bootstrap.min.js',
  array('every_page' => TRUE, 'requires_jquery' => TRUE)
);
drupal_add_css(
  drupal_get_path('theme', 'climatemega').'/stylesheets/jquery-ui-1.10.4.custom/css/transparent-theme/jquery-ui-1.10.4.custom.min.css',
  array('every_page' => TRUE)
);
drupal_add_css(
  drupal_get_path('theme', 'climatemega').'/stylesheets/site.css',
  array('every_page' => TRUE)
);
drupal_add_css(
  drupal_get_path('theme', 'climatemega').'/stylesheets/lteie8.css',
  array('every_page' => TRUE,
        'browsers' => array(
            'IE' => 'lte IE 8',
            '!IE' => FALSE
        ),
        'preprocess' => FALSE
  )
);
drupal_add_js(
  drupal_get_path('theme', 'climatemega').'/js/easymega.js',
  array('every_page' => TRUE, 'requires_jquery' => TRUE, 'scope' => 'footer')
);

?>
<svg style="display: none;">
  <symbol id="menuicon" viewBox="0 0 20 20">
      <path d="m0-0v4h20v-4h-20zm0 8v4h20v-4h-20zm0 8v4h20v-4h-20z"></path>
  </symbol>
</svg>
<header id="header" role="banner" class="clearfix">
    <div id="sitehead" class="container-fluid">
        <div class="deptrow container-fluid">
            <div class="row">
                <div class="col col-xs-12">
                    <div class="contain">
                        <a href="/">Home</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="contain">
            <div class="searchform">
                <form class="form-search content-search" action="/sitesearch" method="get" id="search-block-form" accept-charset="UTF-8">
                    <div>
                        <div>
                            <div class="input-group">
                                <span class="sitesearch">
                                    <input title="Site search" placeholder="Site Search" class="form-control form-text" type="text" id="edit-search-block-form--2" name="search_api_views_fulltext" value="" size="20" maxlength="128">
                                </span>
                                <!-- <span class="input-group-btn"><button type="submit" class="btn btn-default"><i class="icon glyphicon glyphicon-search" aria-hidden="true"></i></button> </span> -->
                            </div>
                            <!-- <button class="element-invisible btn btn-primary form-submit" id="edit-submit" name="op" value="Search" type="submit">Search</button> -->
                        </div>
                    </div>
                </form>
            </div>
            <div class="menubutton">
                <button class="megatoggle" title="show menu">
                    <svg>
                        <use xlink:href="#menuicon"></use>
                    </svg>
                </button>
            </div>
        </div>
        <div class="row titlerow">
            <div class="col col-xs-12">
                <div class="contain">
                    <div class="brand">
                        <div>
                            <!-- <img src="/<?php print drupal_get_path('theme', 'climatemega') ?>/img/Coat_of_arms_of_Fiji.png" alt="Government of Fiji, coat of arms" /> -->
                        </div>
                      <a href="/">Climate Change Portal Starter Pack</a>
                    </div>
                </div>
            </div>
            <div class="megatrans megahover"></div>
        </div>
        <div class="row">
            <div class="megamenu megatoggle">
                <div class="container">
                    <nav>
                        <?php
                        foreach ($megamenu as $name => $item) :
                        ?>
                            <div class="col col-xs-6 col-sm-3 megacolumn">
                                <header>
                                    <p>
                                        <?php echo($name); ?>
                                    </p>
                                </header>
                                <?php echo $item; ?>                                
                              </div>
                        <?php endforeach ?>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="swoosh">
    </div> -->
</header>

<main id="main">
    <div id="main-container">
        <div class="container">
            <?php
            if (! drupal_is_front_page()){ ?>
                <div class="container main-content">
            <?php } ?>

                <?php if (!empty($page['sidebar_first'])): ?>
                    <aside>
                        <div class="col-sm-3" role="complementary">
                            <?php print render($page['sidebar_first']); ?>
                        </div>
                    </aside>  <!-- /#sidebar-first -->
                <?php endif; ?>

                <section>
                    <div<?php //print $content_column_class; ?>>
                        <?php if (!empty($page['highlighted'])): ?>
                            <div class="highlighted jumbotron">
                                <?php print render($page['highlighted']); ?>
                            </div>
                        <?php endif; ?>
                      <?php if (!empty($breadcrumb) && !$is_front): ?>
                        <?php print $breadcrumb; ?>
                      <?php endif; ?>
                      <a id="main-content"></a>
                        <?php print render($title_prefix); ?>
                        <?php if (!empty($title)): ?>
                            <h1 class="page-header"><?php print $title; ?>
                                <?php if (($tags = render($page['content']['field_flag_image'])) ): ?>
                                    <?php print render($page['content']['field_flag_image']); ?>
                                <?php endif; ?>
                            </h1>
                        <?php endif; ?>
                        <?php print render($title_suffix); ?>
                        <?php print $messages; ?>
                        <?php if (!empty($tabs)): ?>
                            <?php print render($tabs); ?>
                        <?php endif; ?>
                        <?php if (!empty($page['help'])): ?>
                            <?php print render($page['help']); ?>
                        <?php endif; ?>
                        <?php if (!empty($action_links)): ?>
                            <ul class="action-links"><?php print render($action_links); ?></ul>
                        <?php endif; ?>
                        <?php print render($page['content']); ?>
                    </div>
                </section>

                <?php if (!empty($page['sidebar_second'])): ?>
                    <aside>
                        <div class="col-sm-3" role="complementary">
                            <?php print render($page['sidebar_second']); ?>
                        </div>
                    </aside>  <!-- /#sidebar-second -->
                <?php endif; ?>

            <?php if (! drupal_is_front_page()){ ?></div><?php } ?>

        </div>
    </div>
</main>

<footer class="footer">
  <div class="image-wrapper">
      <div class="container">
          <div class="translucency">
              <div class="row">
                  <div class="col col-sm-4">
                      <header>
                          <p>New resources</p>
                      </header>
                      <?php echo views_embed_view('recent_resources', 'block'); ?>
                  </div>
                  <div class="col col-sm-4">
                      <header>
                          <p>New projects</p>
                      </header>
                      <?php echo views_embed_view('recent_projects', 'block'); ?>
                  </div>
                  <div class="col col-sm-4">
                      <header>
                          <p>Newest updates</p>
                      </header>
                      <?php echo views_embed_view('recent_updates', 'block'); ?>
                  </div>
              </div>
              <div class="row division">
                  <div class="col col-sm-6">
                    <header><p>Climate Change Depoartment</p></header>
                    <p>Nulla eu malesuada eros. Morbi id placerat enim, sed vulputate lectus. Donec at blandit dolor.</p>
                  </div>
                  <div class="col col-sm-2">
                      <ul>
                          <li>
                          <a href='#' title='Link to top of this page'>Up to top</a>
                          </li>
                      </ul>
                  </div>
                <div class="col col-sm-2">                      
                  <?php echo $footer_menu; ?>                      
                </div>
                  <div class="col col-sm-2">
                      <ul>
                          <li>
                              <a href='/contact-us' title='Communicate with us'>
                                  Contact us
                              </a>
                          </li>
                          <li>
                              <a href='#' title='Our Facebook page'>
                                  Find us on Facebook
                              </a>
                          </li>
                      </ul>
                  </div>
              </div>
          </div>
      </div>
  </div>
</footer>
