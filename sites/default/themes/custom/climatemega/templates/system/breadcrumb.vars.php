<?php
/**
 * @file
 * Stub file for "breadcrumb" theme hook [pre]process functions.
 */

/**
 * Pre-processes variables for the "breadcrumb" theme hook.
 *
 * See theme function for list of available variables.
 *
 * @see bootstrap_breadcrumb()
 * @see theme_breadcrumb()
 *
 * @ingroup theme_preprocess
 */
function climatemega_preprocess_breadcrumb(&$variables) {

  foreach ($variables['breadcrumb'] as &$bc) {
    $text = &$bc;
    if (is_array($bc) && isset($bc['data'])) {
      $text = &$bc['data'];
    }

    //Truncate any breadcrumb text to 75 chars
    $char_limit = 75;
    if (strstr($text, "</a>")) {
      $matches = null;
      if (preg_match("/>(.*)<\/a>$/", $text, $matches)) {
        $link_text = $matches[1];
        $truncated_link_text = truncate_utf8($link_text, $char_limit, TRUE, TRUE);
        $text = str_replace($link_text, $truncated_link_text, $text);
      }
    } else {
      $text = truncate_utf8($text, $char_limit, TRUE, TRUE);
    }
  }
}
