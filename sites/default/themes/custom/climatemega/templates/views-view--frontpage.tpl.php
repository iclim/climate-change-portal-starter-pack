<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<h1>Climate Change Portal</h1>
<?php if ($empty) { ?>
    <p>No content is promoted to front page</p>
<?php } else { ?>
    <?php echo($rows); ?>
<?php } ?>

<div class="pccp-sections" data-example-id="thumbnails-with-content">
    <div class="large-icons row row-centered">
        <div class="col-sm-12 col-centered">
            <h2 style="text-align:center;">
                Climate resources at your fingertips
            </h2>
        </div>

        <div class="col-xs-4 col-centered">
            <a href="/projects">
                <img class="img-responsive center-block front-icons"  src="<?php echo path_to_theme(); ?>/images/icon-projects.png" alt="projects" />
            </a>
            <div class="front-page-panel">
                <h3>Projects</h4>
                <a href="/projects" class="btn btn-primary">Find projects</a>
            </div>
        </div>

        <div class="col-xs-4 col-centered">
          <a href="/education">
            <img class="img-responsive center-block front-icons"  src="<?php echo path_to_theme(); ?>/images/icon-eduresources.png" alt="icon representing education" />
            </a>
            <div class="front-page-panel">
                <h3>For schools and universities</h4>
                  <a href="/education" class="btn btn-primary">Education search</a>
            </div>
        </div>

        <div class="col-xs-4 col-centered">
          <a href="/data-and-documents">
            <img class="img-responsive center-block front-icons"  src="<?php echo path_to_theme(); ?>/images/icon-documents.png" alt="icon representing a document" />
            </a>
            <div class="front-page-panel">
                <h3>Data and documents</h4>
                  <a href="/data-and-documents" class="btn btn-primary">Search resources</a>
            </div>
        </div>

    </div> <!-- end large icons -->
</div> <!-- end pccp-sections -->
